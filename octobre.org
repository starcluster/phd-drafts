#+TITLE: Compte rendu journalier de doctorat
#+AUTHOR: Pierre Wulles
#+SETUPFILE: page.org
#+INCLUDE: begin.org
#+OPTIONS: num:nil
#+LATEX_HEAD: \usepackage{braket}
<2021-12-13 lun.>

- GDR
- Fidle, lire photonic crystals, biblio divers,
-  utilisation de intel compiler.
- Rédiger synthèse.
  
Des tonnes d'exemples de code sur meep:
https://github.com/NanoComp/meep/tree/master/python/examples

<2021-12-06 lun.>

Préparation GDR, finalisation des simulations.


<2021-12-03 ven.>
- Comparaison de l'histogramme ok dans le cas matrice réelle
  symétrique, matrice hermitienne.
- Réalisation d'un gif qui montre l'évolution des bandes pour
  plusieurs valeurs de k0a

<2021-12-02 jeu.>
- Enfin obtenir un résultat correct pour les bandes avec les parties
  imaginaires et a = 1
- POur l'istogramme, juste le comparer à la distribution de Weigner.

<2021-12-01 mer.>
- tracer les bandes avec en plus les parties imaginaires (temps de
  vie) => très bizarre certaines valeurs propres sont négatives.
- Tenter de fitter les distributions de WD  
<2021-11-30 mar.>
- obtenir les  bandes dans l'espace de fourier fonctionne
- continuer le travail sur la distribution de valeurs propres

<2021-11-29 lun.>
- Travail sur les bandes pour obtenir quelques choses de correct dans
  l'espace de Fourier, on se rapproche du plot de Sergey mais les
  valeurs sur les axes restent absurdes.
- Travail sur le calcul de la distribution des espacements des valeurs
  propres dans le cas kd < pi, je suis obligé de couper les premiers
  valeurs car un pic apparait systématiquement à 0.
- Installation d'outils pour transformer emacs en IDE python: voir [[https://gist.github.com/widdowquinn/987164746810f4e8b88402628b387d39][ici]].
<2021-11-26 ven.>
- amélioration du logiciel: tracer de la statistique des deltas des
  fréquences propres disponible, ajout des fonctions log, quit.
- forcer le zéro sur la partie imaginaire de la somme pour avoir une
  matrice G hermitienne,
- calcul de l'histogramme par batch: on ne voit toujours pas
  apparaître l'espacement des valeurs propres !

<2021-11-25 jeu.>
- chercher pourquoi les plots dans l'espace de Fourier sont incorrectes
- cours FIDLE machine learning
- Séminaire Zotero: utiliser webdav !

<2021-11-24 mer.>
- seminar: granular matter: grain de matière se comportant comme un
  fluide, athermal, assez gros.
- improving GUI: colorbar is now updated live

<2021-11-23 mar.>
- adding different grid to the simulation: hex grid, hexhex, random

<2021-11-22 lun.>
- discussion avec Sergey sur comment tracer les diagrammes de bandes
  et afficher les valeurs propres pour un nuage plutot qu'un réseau
- retouches dans le code, déut de l'implémentation du calcul dans
  l'espace de Fourier.

<2021-11-19 ven.>
- passer le code en orienté objet car ça devient inmaintenanble

<2021-11-18 jeu.>
- jeudi de la sécurité
- démonstration lien entre valeurs propres et physique
- amélioration de l'interface graphique de G_hex

<2021-11-17 mer.>
- Séminaire equation Duffing
- Ajout de l'intéractivité dans les plots pour changer les valeurs de
  $d$, $\Delta_B$ et $\Delta_{AB}$
<2021-11-16 mar.>
- Implémentation du calcul de G pour un réseau hexagonal *en forme
  d'hexagone*
<2021-11-15 lun.>
- Séminaire Optical waveform
- Suite des calculs sur les sommes, question posée sur mathoverflow
- Implémentation du calcul de G pour un réseau hexagonal, on semble
  retrouver les même formes que dans le calcul du stage.

<2021-11-12 ven.>
- Séminaires information quantique
- Tentatives de calculs analytiques pour montrer que les sommes qui
  valent numériquement zéro valent zéro.

<2021-11-10 mer.>
- Enfin le problème est trouvé ! Les matrices étaient diagonaliés avec
  le mauvais algorithme, pour une matrice hermitienne ou presque
  hermitienne np.linalg.eig() échoue !
- Début de recherches sur les matrices aléatoires.

<2021-11-09 mar.>
- Même chose que la veille investigation pour trouver le problème

<2021-11-08 lun.>
- On continue dans les simulations qui ne marchent pas après être
  passé par une phase ou ça semblait marcher

<2021-11-05 ven.>
- Tentative de simulations, échec, valeurs propres avec parties
  imaginaires négatives
  
<2021-11-04 jeu.>
- Lecture de l'article de Monsarrat
- Séminaire
  
<2021-11-03 mer.>
- Premiers tests sur la matrice, pour le moment le diagramme des VP
  est confiné d'un coté du plan. Bizarre. Faire des tests pour une
  plus petite matrice et comparer au cas avec les fonctions de hankel.
- Finalisation du diapo pour le seminar
<2021-11-02 mar.>
- Suite des recherches sur le HPC
- Implémentation de la matrice G(d) par la somme
  
* Octobre
  
<2021-10-29 ven.>
- Travaille sur les optimisations AVX, SSE, peu de résultats
- Benchmark C/python
<2021-10-28 jeu.>
- Suite des calculs, ça aboutit mais une erreur se glisse quelquepart car j'obtiens un résultat qui oscille dans le néagtif
<2021-10-27 mer.>
- Mise au propre des calculs pour G_{3D}, début des calculs pour la
  diagonale.
<2021-10-26 mar.>
- Conclusion sur le calcul de $\int_{-\infty}^{+\infty}
  \frac{e^{i\sqrt{x^2+a^2}}}{a^2+x^2}\mathrm{d}x =
  \frac{\pi}{a}\int_a^{+\infty}H^{(1)}_0(t)\mathrm{d}t$
- Correction des bugs pour le calcul de $H_0$ par une série
  convergente, ça fonctionne ssi $Nd \ggg 1$ typiquement $10^{4}$, on
  obtient une figure beaucoup plus proche du résultat obtenu
  précedemment avec les fonctions de Hankel.
- Création d'une librairie de snippet pour effectuer facilement
  rapidement des calculs mathématiques ou autres
  programmation. Remplace le dossier =gsl=.
  
<2021-10-25 lun.>
- Simulations avec les versions sommés, on obtient des résultats qui
  ressemblent sans pour autant être identiques.
<2021-10-22 ven.>
- Calcul de $G_{XX}$ ou $G_{ZZ}$ par la somme et non pas par les
  fonctions de hankel.
<2021-10-21 jeu.>
- conclusion sur les figures de /Maximo et al/ qui sont maintenant
  correctes
- travail sur l'implémentation de Code C dans python
<2021-10-20 mer.>
<2021-10-19 mar.>
Hors les murs
  
<2021-10-18 lun.>
- En reprenant soigneusement le code ~ll2D.py~ je réussis à obtenir
  les bonnes figures (a priori) le problème venait d'une différence
  d'échelles et de nombres d'atoms.
- découverte de linkage C/python: améliore énormément le calcul
  d'intégrale, cf mini doc que j'ai écrite
- découverte de sympy: bien mieux que sagemath et le notebook relou,
  possibilité d'afficher des équations pas trop dégueu dans la console
  avec l'unicode
- en reprenant le calcul de GXX je pense que la démo passe par une
  dérivation de l'intégrande par rapport à $x$ et non par rapport à
  $\rho$
  
<2021-10-15 ven.>
- recherches pour diagonaliser une matrice en C
- finalisation des calculs de GZZ
<2021-10-14 jeu.>
- Discussion avec Sergey
- Suite des calculs
- Simulations numériques
<2021-10-13 mer.>
- Grosse galère sur les calculs de GZZ
- implémentation d'un calcul d'intégrale en C pour confirmer numériquement la valeur de GZZ
  
<2021-10-12 mar.>
- Calcul des inétgrales, recherche sur les fonctions de Bessel (faire
  une fiche récapitulative)
- Correction d'erreurs dans le calculs des $\lambda$ de l'artcle /Maximo et al/
- J.Dalibard: cours sur les pompes adiabatiques.
<2021-10-11 lun.>
- Discussion avec Sergey: deux méthodes envisagées pour le calcul de
  la fonction de green: méthode des images ou méthode de décomposition
  sur une base de $\psi$
- Tentative de calcul de $G_{ZZ}$ dans le cas scalaire puis dans le
  cas vectoriel, le premier cas aboutit avec l'aide de Sergey, dans le
  deuxième on coince très vite sur des intégrales incalculables.
- Reproduction des résultats de l'article de /Maximo et al/: toujours
  pas de plot dans le cas vectoriel.
  
<2021-10-08 ven.>
- Séminaire footprint de la recherche
- Cours JD [[https://www.college-de-france.fr/site/jean-dalibard/course-2018-05-09-09h30.htm][modele SSH et modes de Majorana]]
- Début du paramétrage de la page web
  
<2021-10-07 jeu.>
- Discussion avec Sergey: landscape localization, extension de notre
  fonction de green au cas 2D, résolution de l'équation de poisson,
  voir la méthode la plus simple qui fait le lien entre 3D et 2D.
- Tentative de sommation de la série du potentiel électrique. Échec de
  sommation dans le cas général, avec $x=0$ on aboutit à quelquechose.
- Correction des valeurs propres d'un disque aléatoire, on obtient
  bien quelquechose de cohérent et l'IPR est corrigé aussi.
- Le calcul de $G_{ZZ}(\rho)$ ne mène à rien pour l'instant.

<2021-10-06 mer.>
- Lecture du chapitre sur la méthode des images dans le Jackson
- Application de la méthode des images au cas d'une charge entre deux
  plaques parallèles
- Simulation numérique du champ $E$ généré par des charges entre deux
  plaques métalliques parallèles.
  
<2021-10-05 mar.>
- Conférence sur la localisation d'Anderson
- Implémentation de l'IPR dans les simulations du disc
- Calcul avec et sans near field pour l'oprateur scalaire: ne
  ressemble toujours pas à la figure de l'article.
- Cours de J. Dalibard sur [[https://www.college-de-france.fr/site/jean-dalibard/course-2018-05-02-09h30.htm][la phase de berry et la topologie d'une
  bande d'énergie.]]
- Utilisation de simpy pour la physique: [[https://www.youtube.com/watch?v=rJaXxb_piGI][youtube]]

<2021-10-04 lun.>
- Vérification d'autres calculs dans l'article et mise /au propre/
- Vérification numérique des premiers résultats de l'article: pour le
  noyau scalaire les valeurs propres ne correspondent pas pour le
  moment. Il manque une partie à droite du graphe.


<2021-10-01 ven.>
- Discussion avec Sergey sur la suite des recherches
- Mise en pause du projet des réseaux hexagonaux en 3D
- Analyse de l'article de /Maximo et al/: Spatial and temporal
  localization of light in two dimensions -> refaire les calculs pour
  comprendre la démarche.


#+INCLUDE: begin.org
