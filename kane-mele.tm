<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <section|Graphene>

  <\equation>
    <math-bf|a><rsub|1>=<matrix|<tformat|<table|<row|<cell|0>>|<row|<cell|a>>>>>,<math-bf|a><rsub|2>=<matrix|<tformat|<table|<row|<cell|-a<sqrt|3>/2>>|<row|<cell|-a/2>>>>>,<math-bf|a><rsub|3>=<matrix|<tformat|<table|<row|<cell|a<sqrt|3>/2>>|<row|<cell|-a/2>>>>>
  </equation>

  The hamiltonian is

  <\equation>
    H=<big|sum><rsub|<around*|\<langle\>|j
    k|\<rangle\>>>c<rsub|j><rsup|\<dagger\>>c<rsub|k>
  </equation>

  where <math|c<rsub|><rsup|\<dagger\>>> and <math|c> are operators of
  construction and annihilation. The notation <math|<around*|\<langle\>|j
  k|\<rangle\>>> means that the sum runs over all nearest neighbors. The
  Schrödinger equation can be written for a specific eigenfunction
  <math|\<psi\>> as

  <\equation>
    H\<psi\>=E\<psi\>
  </equation>

  This gives

  <\equation>
    <big|sum><rsub|<text|n>.<text|n>.n>\<psi\><rsub|k>=E\<psi\><rsub|j><label|nnschro>
  </equation>

  where the sums runs over the nearest neighbor of <math|j>. Since the
  lattice is infinite and periodic, we are in the conditions for applying
  Bloch's theorem and take our eigenfunctions to the reciprocal space, in
  this space the primitive cell is called Brillouin Zone and indicated by
  dashed lines on <reference|fig_graphene_bs>. In this Fourier space the
  hamiltonian becomes:

  <\equation>
    <wide|H|^><rsub|graphene><around*|(|<math-bf|k>|)>=<matrix|<tformat|<table|<row|<cell|0>|<cell|h<rsub|0><around*|(|<math-bf|k>|)>>>|<row|<cell|h<rsub|0><around*|(|<math-bf|k>|)><rsup|\<ast\>>>|<cell|0>>>>>
  </equation>

  Where:

  <\equation>
    h<rsub|0><around*|(|<math-bf|k>|)>=t<rsub|1><big|sum><rsub|i>exp<around*|(|i*<math-bf|k>\<cdot\><math-bf|a><rsub|i>|)>
  </equation>

  And <math|t<rsub|1>> is considered as unity here. We can rewrite the wave
  function as follows, for <math|A> or <math|B> sublattice:

  <\equation>
    \<psi\><rsub|j>=<around*|{|<tabular*|<tformat|<table|<row|<cell|A<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)>>>|<row|<cell|B<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)>>>>>>|\<nobracket\>>
  </equation>

  \;

  Using this ansatz in <reference|nnschro> we can write

  <\eqnarray*>
    <tformat|<table|<row|<cell|B<around*|(|<math-bf|k>|)><big|sum><rsub|k=1><rsup|3>exp<around*|(|i*<math-bf|k>\<cdot\><around*|(|<math-bf|r><rsub|j>+<math-bf|a><rsub|k>|)>|)>>|<cell|=>|<cell|E*A<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)><eq-number><label|eqA>>>|<row|<cell|A<around*|(|<math-bf|k>|)><big|sum><rsub|k=1><rsup|3>exp<around*|(|i*<math-bf|k>\<cdot\><around*|(|<math-bf|r><rsub|j>-<math-bf|a><rsub|k>|)>|)>>|<cell|=>|<cell|E*B<around*|(|<math-bf|k>|)>\<mathe\>xp<around*|(|i*<math-bf|k>\<cdot\><math-bf|r><rsub|j>|)><eq-number><label|eqB>>>>>
  </eqnarray*>

  \;

  <section|Haldane>

  <section|Kane-Mele>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-2|<tuple|2|1|../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-3|<tuple|3|1|../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-4|<tuple|3|?|../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|eqA|<tuple|8|?|../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|eqB|<tuple|9|?|../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|fig_graphene_bs|<tuple|1|?|../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|nnschro|<tuple|4|?|../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Graphene>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Haldane>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Kane-Mele>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>