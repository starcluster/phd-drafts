#+OPTIONS: toc:nil
#+OPTIONS: timestamp:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+TITLE: Template
#+REVEAL_PLUGINS: (highlight)
#+REVEAL_HIGHLIGHT_CSS: monokai

#+HTML_HEAD_EXTRA:<meta charset="utf-8">

#+HTML_HEAD_EXTRA:<title>reveal.js – The HTML Presentation Framework</title>

#+HTML_HEAD_EXTRA:<meta name="description" content="A framework for easily creating beautiful presentations using HTML">
#+HTML_HEAD_EXTRA:<meta name="author" content="Hakim El Hattab">

#+HTML_HEAD_EXTRA:<meta name="apple-mobile-web-app-capable" content="yes">
#+HTML_HEAD_EXTRA:<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

#+HTML_HEAD_EXTRA:<meta name="viewport" content="width=device-width, initial-scale=1.0">

#+HTML_HEAD_EXTRA:<link rel="stylesheet" href="mycss.css">
#+HTML_HEAD_EXTRA:<link rel="stylesheet" href="dist/reset.css">
#+HTML_HEAD_EXTRA:<link rel="stylesheet" href="dist/reveal.css">
#+HTML_HEAD_EXTRA:<link rel="stylesheet" href="dist/theme/black.css" id="theme">

#+HTML_HEAD_EXTRA:<link rel="stylesheet" href="plugin/highlight/monokai.css">
#+OPTIONS: date:nil
* H1
Une équation:
#+ATTR_REVEAL: :frag roll-in
\begin{align}
\int f = a + b
\end{align}
* H2

#+ATTR_REVEAL: :code_attribs data-line-numbers='1,2|3,4'
#+BEGIN_SRC ocaml
  let f x = x*x;;
  let rec f l = match l with     
    |[t] -> t
    |t::q -> f q;;

    List.fold_left ;;
#+END_SRC

* H2 b
#+ATTR_REVEAL: :code_attribs data-line-numbers='1|1,2'
#+BEGIN_SRC verbatim
One: Hello.
Two: Hello. if you can ...
#+END_SRC



* H3
** H3.1
** H3.2
* H4
[[file:images/cat.jpg]]



#+BEGIN_export html
		<script src="dist/reveal.js"></script>
		<script src="plugin/zoom/zoom.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/search/search.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script>

			// Also available as an ES module, see:
			// https://revealjs.com/initialization/
			Reveal.initialize({
				controls: true,
				progress: true,
				center: true,
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealZoom, RevealNotes, RevealSearch, RevealMarkdown, RevealHighlight ]
			});

		</script>
#+END_export
