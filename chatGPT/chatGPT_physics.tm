<TeXmacs|2.1>

<style|generic>

<\body>
  <doc-data|<doc-title|ChatGPT for the Physicist>|<doc-subtitle|Exploring the
  Potential of ChatGPT in Physics: From Programming to Scientific
  Writing>|<doc-author|<author-data|<author-name|Pierre
  Wulles>>>|<doc-date|<date>>>

  <section|Introduction>

  ChatGPT is a state-of-the-art language model developed by OpenAI, capable
  of generating human-like text and engaging in conversations with users.
  While its primary use case is to assist humans in generating natural
  language, there are a wide range of potential applications for this
  technology in the field of physics.

  It was first released on November 30, 2022, and subsequently updated on
  March 23, 2023, with the release of ChatGPT 2.0, based on the improved
  GPT-3.5 architecture, the last update is from May 02, 2023.

  In section 2, we will explain how ChatGPT works, including the underlying
  architecture and the training process. This will provide a foundational
  understanding of the technology and how it generates responses. While
  ChatGPT can generate impressive and human-like responses, i<strong|t is not
  a search engine, nor is it designed to verify the information it provides.>
  This section is the most technical one, other focuses on giving examples of
  uses.

  In section 3, we will focus on using ChatGPT for programming tasks.

  In section 4, we will explore how ChatGPT can be used for scientific
  writing, including generating summaries, abstracts, but also spell
  checking<text-dots>

  Finally, in section 5, we will examine the potential ethical implications
  of using ChatGPT and consider alternative approaches. We will discuss the
  importance of transparency, fairness, and accountability in using AI models
  in scientific research and consider some best practices for doing so.

  <section|Understanding ChatGPT>

  <subsection|At the beginning there was GPT>

  Explain what LLM are.

  \;

  <subsection|ChatGPT = GPT + RL + PRE-PROMPT>

  <\big-figure|<image|img/ChatGPT_Diagram.png|0.9par|||>>
    \;
  </big-figure>

  <subsection|What can ChatGPT do ?>

  <\big-figure|<image|img/smart.jpg|1par|||>>
    \;
  </big-figure>

  <\big-figure|<image|img/stock1.png|0.3par|||>
  <image|img/stock2.png|0.3par|||> <image|img/stock3.png|0.3par|||>>
    \;
  </big-figure>

  <\itemize>
    <item>1 = Completely incompetent

    <item>2 = Limited

    <item>3 = Moderately competent

    <item>4 = Competent

    <item>5 = Fully capable
  </itemize>

  \;

  <\big-table|<tabular|<tformat|<table|<row|<cell|>|<cell|1>|<cell|2>|<cell|3>|<cell|4>|<cell|5>>|<row|<cell|Translating>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|Spell
  Checking>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|Solving basic
  math/phys problem>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|Solving
  basic CS problem>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>>>>>
    \;
  </big-table>

  \;

  <section|Using chatGPT for programming>

  <subsection|Natural language programming>

  <subsection|Code generation>

  <subsection|Automated code documentation>

  <section|Using chatGPT for text revision/writing>

  <subsection|Spell checking>

  <subsection|Literature review and summarization>

  <subsection|Translation of technical texts>

  <section|Ethical considerations and alternatives>

  <subsection|Bias in chatGPT models>

  <subsection|Privacy concerns>

  <subsection| Exploring alternative language models>

  Bloom

  GPT4all

  Open assistant

  <section|Bibliography>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|3|4>>
    <associate|auto-11|<tuple|3.1|4>>
    <associate|auto-12|<tuple|3.2|4>>
    <associate|auto-13|<tuple|3.3|4>>
    <associate|auto-14|<tuple|4|4>>
    <associate|auto-15|<tuple|4.1|4>>
    <associate|auto-16|<tuple|4.2|4>>
    <associate|auto-17|<tuple|4.3|4>>
    <associate|auto-18|<tuple|5|4>>
    <associate|auto-19|<tuple|5.1|4>>
    <associate|auto-2|<tuple|2|2>>
    <associate|auto-20|<tuple|5.2|4>>
    <associate|auto-21|<tuple|5.3|4>>
    <associate|auto-22|<tuple|6|4>>
    <associate|auto-3|<tuple|2.1|2>>
    <associate|auto-4|<tuple|2.2|2>>
    <associate|auto-5|<tuple|1|2>>
    <associate|auto-6|<tuple|2.3|3>>
    <associate|auto-7|<tuple|2|3>>
    <associate|auto-8|<tuple|3|4>>
    <associate|auto-9|<tuple|1|4>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-8>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-9>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Understanding
      ChatGPT> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>At the beginning there was
      GPT <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>ChatGPT = GPT + RL +
      PRE-PROMPT <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|2.3<space|2spc>What can ChatGPT do ?
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Using
      chatGPT for programming> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Natural language programming
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|1tab>|3.2<space|2spc>Code generation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|1tab>|3.3<space|2spc>Automated code documentation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Using
      chatGPT for text revision/writing> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14><vspace|0.5fn>

      <with|par-left|<quote|1tab>|4.1<space|2spc>Spell checking
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|1tab>|4.2<space|2spc>Literature review and
      summarization <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>

      <with|par-left|<quote|1tab>|4.3<space|2spc>Translation of technical
      texts <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|5<space|2spc>Ethical
      considerations and alternatives> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-18><vspace|0.5fn>

      <with|par-left|<quote|1tab>|5.1<space|2spc>Bias in chatGPT models
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19>>

      <with|par-left|<quote|1tab>|5.2<space|2spc>Privacy concerns
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-20>>

      <with|par-left|<quote|1tab>|5.3<space|2spc> Exploring alternative
      language models <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|6<space|2spc>Bibliography>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-22><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>