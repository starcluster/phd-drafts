<TeXmacs|2.1>

<style|<tuple|beamer|british>>

<\body>
  <screens|<\hidden>
    <\with|par-mode|center>
      <\with|font-base-size|24>
        ChatGPT for the Physicist
      </with>
    </with>

    <with|par-mode|center|Exploring the Potential of ChatGPT in Physics: From
    Programming to Scientific Writing and many more>

    \;

    <\with|par-mode|center>
      <image|/home/these/chatGPT/img/image310.png|0.3par|||>
    </with>

    \;

    <\with|par-mode|center>
      Pierre Wulles

      31/05/2023
    </with>
  </hidden>|<\hidden>
    <tit|Why this talk ?>

    <\big-figure>
      \;

      <\with|par-mode|center>
        <image|img/labord_market.png|0.8par|||>
      </with>

      \ <image|img/stock2.png|0.4par|||> <image|img/stock3.png|0.4par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Large Langage Model>

    <\itemize>
      <unroll-greyed|<\shown>
        <item>A <strong|token> is a word (almost).\ 
      </shown>|<\shown>
        <item>Examples: Chicken, Fox, Dog, <text-dots>
      </shown>|<\shown>
        <item>Let <math|\<sigma\>> be a sequence of tokens:

        <\equation*>
          \<sigma\>=<around*|(|t<rsub|0>,\<ldots\>,t<rsub|n>|)>
        </equation*>
      </shown>|<\shown>
        <item>The goal of a langage model is to predict <math|t<rsub|n+1>>
        given <math|\<sigma\>>.

        <\equation*>
          \<bbb-P\><around*|(|t<rsub|n+1>=t|)>=F<around*|(|\<sigma\>|)>
        </equation*>
      </shown>|<\shown>
        <item>The chicken was eaten by a<strong| fox> \ 
      </shown>|<\shown>
        <item>The chicken was eaten by a {fox,dog,wolf}.
      </shown>|<\shown>
        <item>A large langage model (LLM) is trained on <strong|a lot> of
        data using a neuronal network (NN).
      </shown>|<\shown>
        <item>GPT-3 was trained over 570 Gb of text, in a lifetime, a human
        can read 1Gb.
      </shown>>
    </itemize>

    \;

    \;
  </hidden>|<\hidden>
    <tit|Large langage model>

    LLM according to GPT-3.5:

    <\quote-env>
      <em|A large language model (LLM) is a machine learning model trained on
      lots of text data to generate human-like language. However, it's
      important to note that the LLM doesn't understand the truth and only
      produces plausible text based on patterns it has learned. So, while it
      can sound convincing, the information it generates may not be accurate
      or factual. It's essential to verify and critically evaluate the
      generated text for accuracy.>
    </quote-env>

    \;
  </hidden>|<\hidden>
    <tit|From GPT to ChatGPT>

    \;

    <\big-figure|<image|img/ChatGPT_Diagram.png|0.9par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|From GPT to ChatGPT>

    The pre-prompt is a text that the chatbot user <strong|does not see>; it
    is used to <em|invoke> the chatbot from the Language Model (LLM)

    <\big-figure|<image|img/sydney.png|1par|||>>
      \;
    </big-figure>

    Finally:

    <strong|<\with|par-mode|center>
      ChatGPT = GPT + RL + PRE-PROMPT
    </with>>

    \;
  </hidden>|<\hidden>
    <tit|How smart is ChatGPT ?>

    <\big-figure|<image|img/smart.jpg|0.45par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Limitations>

    <\big-figure|<image|img/home.png|0.8par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Limitations>

    <\big-figure|<image|img/math.png|0.6par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Limitations>

    <\big-figure|<image|img/math2.png|0.5par|||>>
      \;
    </big-figure>

    In a general manner, ChatGPT is really bad for generating precise stuff.

    But is very good for<text-dots>
  </hidden>|<\hidden>
    <tit|Langage>

    <\big-figure>
      <image|img/better.png|0.7par|||>

      <image|img/long_version.png|0.7par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Natural langage programming>

    <\big-figure>
      <image|img/prompt_trajectory.png|0.6par|||>

      <image|img/answer_trajectory.png|0.6par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Natural langage programming>

    <\big-figure>
      <image|img/trajectory.png|0.6par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Natural langage programming>

    <\big-figure>
      <image|img/prompt_heat_equation.png|0.6par|||>

      <image|img/answer_heat_equation.png|0.6par|||>

      \;
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Natural langage programming>

    <\big-figure>
      <image|img/heat_equation.png|0.4par|||>

      <image|img/answer2_heat_equation.png|0.6par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Programming with ChatGPT>

    Also quite good at<strong| improving existing code> or <strong|generating
    documentation>.

    Some limitations:

    <\itemize>
      <item>when the code is big

      <item>if the code uses a lot of obscur library
    </itemize>
  </hidden>|<\hidden>
    <tit|For teaching>

    <\big-figure>
      <image|img/prompt_kepler.png|0.5par|||>

      <image|img/answer_kepler.png|0.5par|||>
    <|big-figure>
      \;
    </big-figure>

    \ 
  </hidden>|<\hidden>
    <tit|For teaching>

    <\big-figure>
      <image|img/prompt_latex.png|0.6par|||>

      <image|img/answer_latex.png|0.6par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|For teaching>

    <\big-figure|<image|img/latex.png|0.5par|||>>
      \;
    </big-figure>

    <strong|But student can use it to!>
  </hidden>|<\hidden>
    <tit|For learning>

    \;

    The \Pexplain me like I'm 5\Q prompt:

    <\big-figure|<image|img/entropy.png|0.8par|||>>
      \;
    </big-figure>

    But also to learn a new langage, <text-dots>
  </hidden>|<\shown>
    <tit|ChatGPT for text revision/writing>

    <\enumerate>
      <unroll|<\shown>
        <item>Spell checking
      </shown>|<\shown>
        <item>Litterature review and summarization
      </shown>|<\shown>
        <item>Translation of technical texts
      </shown>>
    </enumerate>
  </shown>|<\hidden>
    <tit|Ethical considerations, issues, <text-dots>>

    <\big-figure|<image|img/image140.png|1par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\tit>
      Bias in ChatGPT models
    </tit>

    Arbitrary decisons of OpenAI

    <\big-figure|<image|img/politics.png|1par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Privacy concerns>

    OpenAI is a private company.

    <\big-figure>
      <image|img/cout.png|1par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Alternatives>

    <\big-figure|<image|img/bloom.png|0.8par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\tit>
      Alternatives
    </tit>

    <\big-figure|<image|img/bloom_wild.png|1par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Alternatives>

    <\big-figure|<image|img/gpt4all.png|1par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Conclusion>

    <\big-figure|<image|img/conclusion.png|1par|||>>
      \;
    </big-figure>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|10|?>>
    <associate|auto-11|<tuple|11|?>>
    <associate|auto-12|<tuple|12|?>>
    <associate|auto-13|<tuple|13|1>>
    <associate|auto-14|<tuple|14|?>>
    <associate|auto-15|<tuple|15|?>>
    <associate|auto-16|<tuple|16|?>>
    <associate|auto-17|<tuple|17|?>>
    <associate|auto-18|<tuple|18|?>>
    <associate|auto-19|<tuple|19|?>>
    <associate|auto-2|<tuple|2|?>>
    <associate|auto-20|<tuple|20|?>>
    <associate|auto-21|<tuple|21|?>>
    <associate|auto-22|<tuple|22|?>>
    <associate|auto-23|<tuple|23|?>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
    <associate|auto-6|<tuple|6|?>>
    <associate|auto-7|<tuple|7|?>>
    <associate|auto-8|<tuple|8|?>>
    <associate|auto-9|<tuple|9|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        \;
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        \;
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|8>|>
        \;
      </surround>|<pageref|auto-8>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9>|>
        \;
      </surround>|<pageref|auto-9>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10>|>
        \;
      </surround>|<pageref|auto-10>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11>|>
        \;
      </surround>|<pageref|auto-11>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|12>|>
        \;
      </surround>|<pageref|auto-12>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13>|>
        \;
      </surround>|<pageref|auto-13>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|14>|>
        \;
      </surround>|<pageref|auto-14>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|15>|>
        \;
      </surround>|<pageref|auto-15>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|16>|>
        \;
      </surround>|<pageref|auto-16>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|17>|>
        \;
      </surround>|<pageref|auto-17>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|18>|>
        \;
      </surround>|<pageref|auto-18>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|19>|>
        \;
      </surround>|<pageref|auto-19>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|20>|>
        \;
      </surround>|<pageref|auto-20>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|21>|>
        \;
      </surround>|<pageref|auto-21>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|22>|>
        \;
      </surround>|<pageref|auto-22>>
    </associate>
  </collection>
</auxiliary>