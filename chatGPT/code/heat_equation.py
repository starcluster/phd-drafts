import numpy as np
import matplotlib.pyplot as plt

# Constants
L = 1.0  # Length of the room (square)
W = 1.0  # Width of the room (square)
Nx = 50  # Number of grid points along the x-axis
Ny = 50  # Number of grid points along the y-axis
alpha = 0.1  # Thermal diffusivity
T_left = 100.0  # Temperature at the left wall (heating source)
T_cold = 0.0  # Temperature outside the room (cold)
T_top = 20.0  # Temperature at the top wall (better insulation)
T_initial = 20.0  # Initial temperature in the room

# Calculate grid spacing
dx = L / (Nx - 1)
dy = W / (Ny - 1)
dt = 0.0001  # Time step size
t_final = 0.1  # Final simulation time

# Create the grid
x = np.linspace(0.0, L, Nx)
y = np.linspace(0.0, W, Ny)
X, Y = np.meshgrid(x, y)

# Initialize the temperature array
T = np.ones((Nx, Ny)) * T_initial

# Set the boundary conditions
T[:, 0] = T_top  # Top wall
T[0, :] = T_left  # Left wall
T[-1, :] = T_cold  # Right wall
T[:, -1] = T_cold  # Bottom wall

# Solve the heat equation using finite differences
t = 0.0
while t < t_final:
    Tn = T.copy()
    T[1:-1, 1:-1] = (
        Tn[1:-1, 1:-1]
        + alpha * dt * (
            (Tn[2:, 1:-1] - 2 * Tn[1:-1, 1:-1] + Tn[:-2, 1:-1]) / dx**2
            + (Tn[1:-1, 2:] - 2 * Tn[1:-1, 1:-1] + Tn[1:-1, :-2]) / dy**2
        )
    )
    t += dt

# Plot the temperature distribution
plt.contourf(X, Y, T, cmap='hot')
plt.colorbar()
plt.xlabel('x')
plt.ylabel('y')
plt.title('Temperature Distribution')
plt.show()
