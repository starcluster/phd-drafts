import math
import matplotlib.pyplot as plt

def calculate_trajectory(m, v0, theta):
    g = 9.8  # acceleration due to gravity (m/s^2)
    theta_rad = math.radians(theta)  # convert angle to radians

    # Calculate the time of flight
    t_flight = (2 * v0 * math.sin(theta_rad)) / g

    # Calculate the horizontal and vertical components of velocity
    v0x = v0 * math.cos(theta_rad)
    v0y = v0 * math.sin(theta_rad)

    # Calculate the time intervals
    t_intervals = [i / 100 for i in range(int(t_flight * 100) + 1)]

    # Calculate the x and y coordinates at each time interval
    x_coordinates = [v0x * t for t in t_intervals]
    y_coordinates = [(v0y * t) - (0.5 * g * t ** 2) for t in t_intervals]

    return x_coordinates, y_coordinates

def plot_trajectory(x_coordinates, y_coordinates):
    plt.plot(x_coordinates, y_coordinates)
    plt.xlabel('Horizontal Distance (m)')
    plt.ylabel('Vertical Distance (m)')
    plt.title('Projectile Trajectory')
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
    import sys

    if len(sys.argv) != 4:
        print("Usage: python trajectory.py <mass> <velocity> <angle>")
        sys.exit(1)

    mass = float(sys.argv[1])
    velocity = float(sys.argv[2])
    angle = float(sys.argv[3])

    x_coords, y_coords = calculate_trajectory(mass, velocity, angle)
    plot_trajectory(x_coords, y_coords)
