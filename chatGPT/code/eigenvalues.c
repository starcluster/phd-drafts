#include <stdio.h>
#include <stdlib.h>
#include <lapacke.h>

#define MATRIX_SIZE 3

void readMatrixFromCSV(const char* filename, double matrix[MATRIX_SIZE][MATRIX_SIZE]) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error opening file %s\n", filename);
        exit(1);
    }

    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < MATRIX_SIZE; j++) {
            fscanf(file, "%lf,", &matrix[i][j]);
        }
    }

    fclose(file);
}

void printMatrix(double matrix[MATRIX_SIZE][MATRIX_SIZE]) {
    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < MATRIX_SIZE; j++) {
            printf("%lf\t", matrix[i][j]);
        }
        printf("\n");
    }
}

void calculateEigenvalues(double matrix[MATRIX_SIZE][MATRIX_SIZE]) {
    double eigenvalues[MATRIX_SIZE];
    double workspace[3 * MATRIX_SIZE - 1];
    int info;

    LAPACK_dsyev(LAPACK_ROW_MAJOR, 'N', 'L', MATRIX_SIZE, &matrix[0][0], MATRIX_SIZE, eigenvalues, workspace, 3 * MATRIX_SIZE - 1, &info);

    if (info > 0) {
        printf("Failed to calculate eigenvalues.\n");
        exit(1);
    }

    printf("Eigenvalues:\n");
    for (int i = 0; i < MATRIX_SIZE; i++) {
        printf("%lf\n", eigenvalues[i]);
    }
}

int main() {
    double matrix[MATRIX_SIZE][MATRIX_SIZE];

    readMatrixFromCSV("matrix.csv", matrix);

    printf("Input matrix:\n");
    printMatrix(matrix);

    calculateEigenvalues(matrix);

    return 0;
}
