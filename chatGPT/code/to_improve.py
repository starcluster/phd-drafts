import numpy as np

# Create a sample NumPy array
arr = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

# Get the shape of the array
rows, cols = arr.shape

# Perform an operation using double loops
for i in range(rows):
    for j in range(cols):
        # Perform an operation on each element of the array
        arr[i, j] += 1

# Print the modified array
print(arr)
