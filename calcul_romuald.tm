<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <\equation*>
    H=<big|sum><rsub|n<rsub|1>n<rsub|2>><big|sum><rsub|n<rsub|1><rprime|'>n<rsub|2><rprime|'>><around*|(|<big|sum><rsub|k<rsub|x>>c<rsub|k<rsub|x>,n<rsub|2>><rsup|<rsub|\<dagger\>>>\<mathe\><rsup|-i*k<rsub|x>n<rsub|1>R>|)><around*|(|<big|sum><rsub|k<rsub|x><rprime|'>>c<rsub|k<rsub|x><rprime|'>,n<rsub|2><rprime|'>><rsup|>\<mathe\><rsup|-i*k<rsub|x><rprime|'>n<rsub|1><rprime|'>R>|)>
  </equation*>

  <\equation*>
    H=<big|sum><rsub|k<rsub|x>>c<rsub|k<rsub|x>,0><rsup|\<dagger\>>c<rsub|k<rsub|x>,1>\<mathe\><rsup|i*k<rsub|x><around*|(|\<pm\>R|)>>+c<rsub|k<rsub|x>,1><rsup|\<dagger\>>c<rsub|k<rsub|x>,0>\<mathe\><rsup|i*k<rsub|x><around*|(|\<pm\>R|)>>
  </equation*>

  <\equation*>
    <matrix|<tformat|<table|<row|<cell|H<rsub|1>>|<cell|H<rsub|2><around*|(|k<rsub|x>|)>>>|<row|<cell|H<rsub|2><rsup|\<dagger\>><around*|(|k<rsub|x>|)>>|<cell|H<rsub|1>>>>>>
  </equation*>

  <\equation*>
    H<rsub|2><around*|(|k<rsub|x>|)>=diag<around*|(|\<mathe\><rsup|i*k<rsub|x>R>,\<mathe\><rsup|-i*k<rsub|x>R>,\<ldots\>.|)>
  </equation*>

  <\equation*>
    H<around*|(|k<rsub|x>|)>=<matrix|<tformat|<table|<row|<cell|H<rsub|1>>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>>|<row|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|H<rsub|1>>>>>>
  </equation*>

  <\equation*>
    H<around*|(|k<rsub|x>|)>=<matrix|<tformat|<table|<row|<cell|H<rsub|1>>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|0>>|<row|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|H<rsub|1>>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>>|<row|<cell|0>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|H<rsub|1>>>>>>
  </equation*>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>