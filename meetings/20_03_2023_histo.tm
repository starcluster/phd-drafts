<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\big-figure>
      Refaire spdf en testant toutes les rotations et garder le meilleur
      produit scalaire.

      \;
    <|big-figure>
      \;
    </big-figure>
  </shown>|<\hidden>
    <\big-figure>
      Refaire histo en prenant en compte TOUTE la zone de brillouin
      correctement discr�tiz� et\ 

      \;
    <|big-figure>
      \;
    </big-figure>

    <\equation*>
      k<rsub|0>a=<frac|2\<pi\>f|c>a
    </equation*>

    Avec <math|f=7.23> GHz et <math|a=10> mm ?\ 
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/hist_6cell_a1.0_k0a0.3.pdf||||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    Max sur toutes les permutation circulaires.

    <\big-figure|<image|/home/these/localization-of-light-in-2d/a1_d2_b6b7.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/a1.1_d2_b6b7.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/a0.92_d2_b6b7.pdf||||>>
      \;
    </big-figure>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
    <associate|auto-6|<tuple|6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        \;
      </surround>|<pageref|auto-6>>
    </associate>
  </collection>
</auxiliary>