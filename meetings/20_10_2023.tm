<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <section*|Emergence of the phase diagram>

    <\big-figure>
      <image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_1_hex.pdf|0.38par|||><image|/home/these/chapter-2-thesis/figs/phase_diagram_spin_bott_2.pdf|0.38par|||>
      \ 

      <image|/home/these/chapter-2-thesis/figs/phase_diagram_spin_bott_4.pdf|0.38par|||><image|/home/these/chapter-2-thesis/figs/phase_diagram_spin_bott_6.pdf|0.38par|||>
    <|big-figure>
      <math|N=6,24,96,216>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter-2-thesis/figs/lattice_minim.pdf|0.5par|||><image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_19.pdf|0.5par|||>
    >
      Emergence of topology when <math|N=19> ! Can we do better ?\ 
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter-2-thesis/figs/lattice_18.pdf|0.5par|||>
    <image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_18.pdf|0.5par|||>>
      <math|N=18>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter-2-thesis/figs/lattice_17.pdf|0.5par|||>
    <image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_17.pdf|0.5par|||>>
      <math|N=17>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter-2-thesis/figs/lattice_15.pdf|0.5par|||>
    <image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_15.pdf|0.5par|||>>
      <math|N=15>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter-2-thesis/figs/lattice_13.pdf|0.5par|||>
    <image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_13.pdf|0.5par|||>>
      <math|N=13>
    </big-figure>
  </hidden>|<\hidden>
    <section*|Comparison between <math|U*V*U<rsup|-1>V<rsup|-1>> and
    <math|U*V*U<rsup|\<dagger\>>V<rsup|\<dagger\>>>>

    <\big-figure|<image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_2.pdf|0.45par|||>
    <image|/home/these/chapter-2-thesis/figs/phase_diagram_bott_dagger_2.pdf|0.451par|||>>
      Left inverse, right dagger for <math|N=24>, no significative change but
      the topological phase is a bit bigger with inverse which makes sense.
    </big-figure>
  </hidden>|<\hidden>
    <section*|Improving plot on spin transition in Kekule Texture model>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/sbi_t2_pbc.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <section*|Anderson disorder>

    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_1.5_96_0.0.pdf|0.35par|||>
      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_1.5_96_1.02.pdf|0.35par|||>\ 

      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_0.5_96_0.0.pdf|0.35par|||>
      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_0.5_96_1.02.pdf|0.35par|||>
    <|big-figure>
      Disorder added on the diagonal, expected behaviour ? localization
      everywhere.
    </big-figure>
  </hidden>|<\shown>
    <section*|Spin Chern number for TE modes>

    We have the hamiltonian:

    <\equation*>
      H<around*|(|<math-bf|k>|)>\<in\>\<cal-M\><rsub|12><around*|(|\<bbb-C\>|)>
    </equation*>

    With its associated eigenvectors:

    <\equation*>
      \<psi\><rsub|i>,1\<leqslant\>i\<leqslant\>12
    </equation*>

    Then we define for angles <math|\<varphi\>=<around*|{|<frac|k\<pi\>|3>,k=0\<ldots\>5|}>>:No
    should be <math|<around*|{|<around*|(|2i-3|)>\<pi\>/6|}>> ? Depends.

    <\equation*>
      \<eta\><rsub|i>=<rsup|t><around*|[|\<psi\><rsub|i><rsup|0>cos<around*|(|\<varphi\><rsub|0>|)>+\<psi\><rsub|i><rsup|1>sin<around*|(|\<varphi\><rsub|0>|)>,\<ldots\>|]>
    </equation*>

    From these new vectors we build the projector <math|P>. The new
    hamiltonian becomes:

    <\equation*>
      <wide|H|~><around*|(|<math-bf|k>|)>=P\<sigma\>P\<in\>\<cal-M\><rsub|6><around*|(|\<bbb-C\>|)>
    </equation*>

    But problem: eigenvalues of <math|P> are not <math|0> or <math|1>.
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|7|?>>
    <associate|auto-11|<tuple|8|?>>
    <associate|auto-12|<tuple|8|?>>
    <associate|auto-13|<tuple|9|?>>
    <associate|auto-14|<tuple|9|1>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|1>>
    <associate|auto-4|<tuple|3|?>>
    <associate|auto-5|<tuple|4|?>>
    <associate|auto-6|<tuple|5|?>>
    <associate|auto-7|<tuple|6|?>>
    <associate|auto-8|<tuple|6|?>>
    <associate|auto-9|<tuple|7|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=6,24,96,216>>
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Emergence of topology when <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=19>>
        ! Can we do better ?\ 
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=18>>
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=17>>
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=15>>
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=13>>
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        Left inverse, right dagger for <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=24>>,
        no significative change but the topological phase is a bit bigger
        with inverse which makes sense.
      </surround>|<pageref|auto-9>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|8>|>
        \;
      </surround>|<pageref|auto-11>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9>|>
        Disorder added on the diagonal, expected behaviour ? localization
        everywhere.
      </surround>|<pageref|auto-13>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Emergence
      of the phase diagram> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Comparison
      between <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|U*V*U<rsup|-1>V<rsup|-1>>>
      and <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|U*V*U<rsup|\<dagger\>>V<rsup|\<dagger\>>>>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Improving
      plot on spin transition in Kekule Texture model>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Anderson
      disorder> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Spin
      Chern number for TE modes> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>