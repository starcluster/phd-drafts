<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <section*|Wave propagation>

    A solution can be represented by:

    <\equation*>
      u<around*|(|\<omega\>|)>=<big|sum><rsub|n>A<rsub|n><around*|(|\<omega\>|)>R<rsub|n>
    </equation*>

    With:

    <\equation*>
      A<rsub|n><around*|(|\<omega\>|)>=<frac|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2|\<omega\>-\<omega\><rsub|n>+i\<Gamma\><rsub|n>/2><around*|\<langle\>|L<rsub|n><around*|\||v<around*|(|\<omega\>|)>|\<nobracket\>>|\<rangle\>>
    </equation*>

    Thus to obtain the solution as a function of time, we do the reverse
    Fourier transform:

    <\equation*>
      A<rsub|n><around*|(|t|)>=\<frak-F\><rsup|-1><around*|(|<frac|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2|\<omega\>-\<omega\><rsub|n>+i\<Gamma\><rsub|n>/2><around*|\<langle\>|L<rsub|n><around*|\||v<around*|(|\<omega\>|)>|\<nobracket\>>|\<rangle\>>|)><around*|(|t|)>
    </equation*>

    The source is considered to be:

    <\equation*>
      v<rsub|n><around*|(|\<omega\>|)>=G<around*|(|<math-bf|r><rsub|n>|)><matrix|<tformat|<table|<row|<cell|s<rsub|x>>>|<row|<cell|s<rsub|y>>>>>>f<around*|(|\<omega\>|)>
    </equation*>

    With:

    <\equation*>
      G<around*|(|<math-bf|r><rsub|n>|)>=<around*|(|P<around*|(|<frac|\<omega\>|c>*r<rsub|n>|)><with|font|Bbb|1><rsub|2>+Q<around*|(|<frac|\<omega\>|c>r<rsub|n>|)><frac|<math-bf|r><rsub|n>\<otimes\><math-bf|r><rsub|n>|r<rsub|n><rsup|2>>|)><frac|\<mathe\><rsup|i*k*r<rsub|n>>|r<rsub|n>>
    </equation*>

    Where <math|<math-bf|r><rsub|n>> is the vector between the site <math|n>
    and the source.\ 
  </hidden>|<\shown>
    We will start by considering that the source is exciting only along
    <math|y>:

    <\equation*>
      v<rsub|n><around*|(|\<omega\>|)>=G<around*|(|<math-bf|r><rsub|n>|)><matrix|<tformat|<table|<row|<cell|0>>|<row|<cell|s<rsub|y>>>>>>f<around*|(|\<omega\>|)>
    </equation*>

    We obtain:

    <\eqnarray*>
      <tformat|<table|<row|<cell|A<rsub|n><around*|(|t|)>>|<cell|=>|<cell|e<rsup|i\<omega\><rsub|n>t-t\<Gamma\><rsub|n>/2>s<rsub|y><around*|(|\<omega\><rsub|n>-\<omega\><rsub|0>+i<frac|\<Gamma\><rsub|0>-\<Gamma\><rsub|n>|2>|)><big|sum><rsub|n=1><rsup|N>L<rsub|2n><frac|x<rsub|n>y<rsub|n>|r<rsub|n>>Q<around*|(|<frac|\<omega\><rsub|n>-i\<Gamma\><rsub|n>/2|c>r<rsub|n>|)>>>|<row|<cell|>|<cell|+>|<cell|L<rsub|2n+1><around*|(|P<around*|(|<frac|\<omega\><rsub|n>-i\<Gamma\><rsub|n>/2|c>r<rsub|n>|)>+Q<around*|(|<frac|\<omega\><rsub|n>-i\<Gamma\><rsub|n>/2|c>r<rsub|n>|)><frac|y<rsub|n><rsup|2>|r<rsub|n><rsup|2>>|)>f<around*|(|\<omega\><rsub|n>-i*\<Gamma\><rsub|n>/2|)>>>>>
    </eqnarray*>

    With:

    <\equation*>
      f<around*|(|\<omega\>|)>=A*exp<around*|(|-<around*|(|<frac|\<omega\>-\<omega\><rsub|c>|\<sigma\>>|)><rsup|2>|)>
    </equation*>
  </shown>|<\hidden>
    \;

    <\wide-tabular>
      <tformat|<cwith|1|1|1|1|cell-halign|c>|<table|<row|<\cell>
        <strong|Problem>: How do we select the eigenstate on the edge ?
      </cell>>>>
    </wide-tabular>

    \;
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1|24_10_2023_wave_propagation.tm>>
    <associate|auto-2|<tuple|1|?|24_10_2023_wave_propagation.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Wave
      propagation> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>