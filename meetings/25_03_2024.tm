<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\hidden>
    <tit|Kekule texture>

    <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/imap_0.02_no_disorder.pdf|0.4par|||>
    <image|/home/pierre/chapter-5-thesis/figs/imap_-0.025_disorder=0.3.pdf|0.4par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Kekule texture proj on spdf>

    <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/proj_maps/391.png|0.5par|||>>
      ??
    </big-figure>
  </hidden>|<\hidden>
    <tit|Proj on spdf for 6 cell model>

    <\big-figure|<image|/home/pierre/chapter-6-thesis/figs/proj_maps/1215.png|0.5par|||>>
      ??
    </big-figure>
  </hidden>|<\hidden>
    <tit|TAI in kekule texture>

    <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/bott_map_no_pbc.pdf|0.4par|||>
    <image|/home/pierre/chapter-5-thesis/figs/bott_map_no_pbc_disorder_0.1.pdf|0.4par|||>>
      ?? disorder makes the bott index very chaotic.
    </big-figure>
  </hidden>|<\shown>
    <tit|Jury>

    <\itemize>
      <item>C�cile Repellin (examinatrice)

      <item>Fr�d�ric Faure (examinateur)

      <item>Henning Schomerus (rapporteur)

      <item>David Carpentier (rapporteur)

      <item>Adolfo Grushin (examinateur)

      <item>Atomes froids (pr�sident de jury = Professeur ou DR)
    </itemize>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        ??
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        ??
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        ?? disorder makes the bott index very chaotic.
      </surround>|<pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>