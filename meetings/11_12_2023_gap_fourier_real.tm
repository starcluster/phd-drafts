<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\big-figure|<image|/home/these/chapter-5-thesis/figs/wgap_t21.2_fourier.pdf|0.8par|||>>
      \;
    </big-figure>
  </shown>|<\hidden>
    <\big-figure|<image|/home/these/chapter-5-thesis/figs/wgap_t21.2_N216.pdf|0.8par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter-5-thesis/figs/bott_map_no_pbc.pdf|0.8par|||>>
      \;
    </big-figure>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>