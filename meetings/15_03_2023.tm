<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/visu_psi/re/910.png|0.45par|||><image|/home/these/localization-of-light-in-2d/visu_psi/re/1090.png|0.45par|||>>
      \;
    </big-figure>

    Entre bande 6 et 7 changement de comportement, interversion <math|p> et
    <math|d>.
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>