<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\shown>
    <\wide-tabular>
      <tformat|<cwith|1|1|2|2|cell-valign|c>|<table|<row|<\cell>
        Including plates with <math|\<varepsilon\>=1000>\ 

        <image|/home/pierre/chapter-5-thesis/figs/kekule_mpb_gradient_0.3_1.0.pdf|0.5par|||>
      </cell>|<\cell>
        <math|<frac|d\<omega\>|d*k<around*|[|a<rsub|0>|]>>=0.05
        c=0.05*\<times\>3\<times\>10\<#2078\>> m/s

        Size of a cell <math|a<rsub|0>=3a>

        <math|k> is in unit of <math|1/a<rsub|0>>

        <math|\<omega\>> is in unit of <math|c/a>

        thus

        <math|<frac|d\<omega\>|d*k<around*|[|a|]>>=0.05*\<times\>3/3\<times\>10<rsup|1>>
        cm/ns

        <math|<frac|d\<omega\>|d*k>=0.16> cell/ns
      </cell>>>>
    </wide-tabular>

    \;
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|8:5>
    <associate|page-width|auto>
  </collection>
</initial>