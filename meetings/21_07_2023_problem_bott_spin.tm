<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/Bott_transition_N_sites=384_t2=1.49_norm_after.pdf|0.8par|||>>
      Initially, <math|N=384>
    </big-figure>
  </hidden>|<\shown>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/Bott_transition_N_sites=600_t2=1.49_norm_before.pdf|0.8par|||>>
      More sites: <math|N=600> but transition does not occur for
      <math|t<rsub|2>=t<rsub|1>>
    </big-figure>
  </shown>|<\hidden>
    Different normalisation,

    First way:

    <\equation*>
      <around*|(|s,p<rsub|x>,p<rsub|y>,d<rsub|x*y>,d<rsub|x<rsup|2>-y<rsup|2>>,f|)>\<rightarrow\>p<rsup|+>=<frac|1|<sqrt|2>><around*|(|\<ldots\>|)>\<rightarrow\>Normalization
    </equation*>

    probably not correct so second way

    <\equation*>
      <around*|(|s,p<rsub|x>,p<rsub|y>,d<rsub|x*y>,d<rsub|x<rsup|2>-y<rsup|2>>,f|)>\<rightarrow\>Normalization\<rightarrow\>p<rsup|+>=<frac|1|<sqrt|2>><around*|(|\<ldots\>|)>
    </equation*>

    transition does not occur for <math|t<rsub|2>=t<rsub|1>><text-dots>

    \;
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/Bott_transition_N_sites=600_t2=1.49_spdf_norm_after.pdf|0.8par|||>>
      Now <math|N=600> but normalization after ???
    </big-figure>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Initially, <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=384>>
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        More sites: <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=600>>
        but transition does not occur for
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>=t<rsub|1>>>
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Now <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|N=600>>
        but normalization after ???
      </surround>|<pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>