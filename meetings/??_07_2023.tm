<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\itemize>
      <item><with|color|dark green|Valeur de <math|r<rsub|c>> \<checkmark\>>

      <item>D�sordre sur tous les sites

      <item>Enqueter sur les valeurs propres non z�ro dans <math|P\<sigma\>P>

      <item>Moyenner les cartes avec diff�rents types de d�sordre

      <item><with|color|dark green|Finir de corriger l'article>
      <with|color|dark green|<with|font-base-size|12|\<checkmark\>>>

      <item>Comparaison formule Milloni/Ma formule

      <item>Calculer <math|H<rsup|+>> et <math|H<rsup|->> mais avec la zone
      de Brillouin du tight-binding

      <item>�crire le chapitre 2:\ 

      <\itemize>
        <item><with|color|dark green|Tight-binding graphene (voir long
        text)><with|color|dark green|\<checkmark\>>

        <item>Haldane (nombre de chern)

        <item>Notre mod�le de TB

        <item>nano rubban (sur un cylindre infini et p�riodique)
      </itemize>

      <item>Lire le livre de F. Faure/Article de A. Grushin

      <item><with|color|dark green|R�inscription> <with|color|dark green|
      \<checkmark\>>
    </itemize>
  </shown>|<\hidden>
    Apr�s la r�union du 21/07/2023:

    <\itemize>
      <item>Ecrire intro th�se 10 pages mise en contexte: se baser sur des
      articles de revues

      <item>Calculer 10 20 points pour <math|1\<less\>t<rsub|2>\<less\>1.2>
      pour voir si la transition est due � un effet de taille finie

      <item>Essayer un calcul avec un r�seau p�riodique

      <item>AJOUTER LES AXES SUR LES FIGURES CHAPTER2

      <item>Chern=3 peut �tre du � une mauvaise zone de Brillouin,comparer au
      calcul analytique
    </itemize>
  </hidden>|<\hidden>
    <tit|Value of <math|r<rsub|c>>>

    We introduce disorder using:

    <\equation*>
      t<rsub|2><rprime|'>=t<rsub|2>\<mathe\><rsup|-<frac|r-a|r<rsub|c>>>
    </equation*>

    From the paper of Le Bellec et al:

    <\equation*>
      r<rsub|c>=2.85\<times\>10<rsup|-3>
    </equation*>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>