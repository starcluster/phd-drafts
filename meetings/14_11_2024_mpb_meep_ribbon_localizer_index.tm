<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\shown>
    Following the meeting of the 08/11 with Adolfo:

    <\itemize>
      <item>Haldane = A, Kane-Mele = AII

      <item>Localizer index for AII is not sgn but related to Pfiaffian

      <item><with|color|dark green|<with|color|green|DONE>>: compute the
      pfiaffan for the Kane-Mele model and the Kekule model (TB)

      <item><with|color|green|DONE>: compute the pfiaffan for the photonic
      haldane and the photonic QSHE
    </itemize>

    Following the meeting of the 08/11 with Sergey:

    <\itemize>
      <item>Real need to model the experiment with finite-element method to
      explain the absence of counter propagative mode in the experiment

      <item><with|color|red|TODO>: Band diagram for ribbon: three cases: no
      interface, topo-trivial, topo-air, check setup

      <item><with|color|red|TODO>: Meep simulation for a Gaussian pulse with
      <math|f> and <math|f<rsub|width>>: check BD and setup

      <item><with|color|red|TODO>: TAI in Kane-Mele model
    </itemize>
  </shown>|<\hidden>
    But also some urgent point to treat:

    <\itemize>
      <item><with|color|green|DONE>: Release a stable version for pybott

      <item><with|color|green|DONE>: Release doc

      <item><with|color|red|TODO>: Add examples as complete tar gz

      <item><with|color|red|TODO>: Finish the poster

      <item><with|color|red|TODO>: Write a first draft of the paper

      <item><with|color|red|TODO>: Include corrections to the thesis

      <item><with|color|red|TODO>: Complete my part in lolitop paper
    </itemize>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/haldane_li_pd_0.05.pdf|0.6par|||>>
      Phase diagram made with the localizer index for the Haldane model
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/li_kanemele_1_32.pdf|0.6par|||>>
      Localizer index computed for the Kane-Mele model
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/li_kekule_0.9_96.pdf|0.6par|||>>
      Localizer index computed for the Kekule model
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/localizer/kappa_1_l3_0.pdf|0.5par|||>>
      <math|\<Delta\><rsub|\<Beta\>>=12,\<Delta\><rsub|AB>=0>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/topological_photonic_pd_10.pdf|0.55par|||>>
      Phase diagram made with the Localizer index
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/li_tp_no_btrs.pdf|0.65par|||>>
      Topological invariants in a Photonic QSHE system\ 
    </big-figure>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|16:9>
    <associate|page-width|auto>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|2|?>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
    <associate|auto-6|<tuple|6|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Phase diagram made with the localizer index for the Haldane model
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Localizer index computed for the Kane-Mele model
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Localizer index computed for the Kekule model
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<Delta\><rsub|\<Beta\>>=12,\<Delta\><rsub|AB>=0>>
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        Phase diagram made with the Localizer index
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        Topological invariants in a Photonic QSHE system\ 
      </surround>|<pageref|auto-6>>
    </associate>
  </collection>
</auxiliary>