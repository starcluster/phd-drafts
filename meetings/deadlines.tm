<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <tit|Deadlines>

    \;

    <\wide-tabular>
      <tformat|<cwith|1|8|1|1|cell-halign|c>|<cwith|1|8|2|2|cell-halign|c>|<cwith|9|9|1|-1|cell-halign|c>|<table|<row|<\cell>
        Chapitre 1
      </cell>|<\cell>
        22/12/2023
      </cell>>|<row|<\cell>
        Chapitre 3
      </cell>|<\cell>
        02/02
      </cell>>|<row|<\cell>
        Chapitre 4
      </cell>|<\cell>
        01/03
      </cell>>|<row|<\cell>
        Chapitre 5 et version 1 du manuscrit
      </cell>|<\cell>
        07/04
      </cell>>|<row|<\cell>
        Relecture par Sergey
      </cell>|<\cell>
        03/05
      </cell>>|<row|<\cell>
        Corrections par Pierre
      </cell>|<\cell>
        07/06
      </cell>>|<row|<\cell>
        Relecture Finale par Pierre et Sergey
      </cell>|<\cell>
        21/06
      </cell>>|<row|<\cell>
        Préparation soutenance
      </cell>|<\cell>
        19/08 au 02/09\ 
      </cell>>|<row|<\cell>
        Soutenance
      </cell>|<\cell>
        ??/09
      </cell>>>>
    </wide-tabular>

    <\big-figure*|<image|/home/wulles/phd-drafts/deadline-meme.jpg|0.4par|||>>
      \;
    </big-figure*>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <\tuple|normal>
        \;
      </tuple|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>