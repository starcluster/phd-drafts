<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/honeycomb_db12.pdf|0.5par|||><image|/home/these/localization-of-light-in-2d/tb.pdf|0.5par|||>>
      Lattice field for Honeycomb system with
      <math|\<Delta\><rsub|B>=12,\<Delta\><rsub|A*B>=0> on the left and tight
      binding model on th right.
    </big-figure>
  </hidden>|<\hidden>
    Le mod�le de Kane-Mele:

    <\eqnarray*>
      <tformat|<table|<row|<cell|H>|<cell|=>|<cell|-t<big|sum><rsub|<around*|\<langle\>|i*j|\<rangle\>>,\<sigma\>>c<rsub|i*\<sigma\>><rsup|\<dagger\>>c<rsub|j*\<sigma\>>+>>|<row|<cell|>|<cell|>|<cell|i\<lambda\><rsub|SO><big|sum><rsub|<around*|\<langle\>|<around*|\<langle\>|i*j|\<rangle\>>|\<rangle\>>,\<alpha\>\<beta\>>c<rsub|i*\<alpha\>><rsup|\<dagger\>>\<sigma\><rsub|\<alpha\>\<beta\>><rsup|z>c<rsub|j*\<beta\>>>>|<row|<cell|>|<cell|>|<cell|+i\<lambda\><rsub|R><big|sum><rsub|<around*|\<langle\>|<around*|\<langle\>|i*j|\<rangle\>>|\<rangle\>>,\<alpha\>\<beta\>>u<rsub|i*j>c<rsub|i*\<alpha\>><rsup|\<dagger\>><around*|(|\<sigma\>\<times\>d<rsub|i*j>|)><rsub|\<alpha\>\<beta\>><rsup|z>c<rsub|j*\<beta\>>>>|<row|<cell|>|<cell|>|<cell|-B<rsub|z><big|sum><rsub|i,\<alpha\>\<beta\>>c<rsub|i*\<alpha\>><rsup|\<dagger\>>\<sigma\><rsub|\<alpha\>\<beta\>><rsup|z>c<rsub|j*\<beta\>>>>>>
    </eqnarray*>

    Representing respectively:

    <\itemize>
      <item>nearest-neighbor (NN) hopping

      <item>intrinsic SOC

      <item> Rashba SOC

      <item>external perpendicular Zeeman field
    </itemize>
  </hidden>|<\shown>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/lambda_SO=0.05_N=100.pdf|0.6par|||>>
      The model works fine. I should try it on the spin chern code but
      problem dimension !
    </big-figure>
  </shown>|<\hidden>
    But also:

    <\itemize>
      <item>lots of minors improvements in the code for better readibility.
    </itemize>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_6.tm>>
    <associate|auto-2|<tuple|2|?|../../.TeXmacs/texts/scratch/no_name_6.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Lattice field for Honeycomb system with
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<Delta\><rsub|B>=12,\<Delta\><rsub|A*B>=0>>
        on the left and tight binding model on th right.
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        The model works fine. I should try it on the spin chern code.
      </surround>|<pageref|auto-2>>
    </associate>
  </collection>
</auxiliary>