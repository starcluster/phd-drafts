<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <tit|Band diagram>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/band_diagram_2.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Width of the gap>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/w_gap_2.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Spin chern evolution>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/spin_chern_distance=2.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    <tit|Width of the gap with different <math|k<rsub|0>a>>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/gap_map_2.pdf||||>>
      \;
    </big-figure>
  </shown>|<\hidden>
    <tit|Wannier functions>

    <\big-figure|<image|/home/these/chapter-2-thesis/figs/wannier_5.pdf|0.5par|||>
    <image|/home/these/chapter-2-thesis/figs/wannier_evolution.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|?>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>
    </associate>
  </collection>
</auxiliary>