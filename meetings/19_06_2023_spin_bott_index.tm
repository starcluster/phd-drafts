<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <tit|Tight-binding on six cell>

    <\equation*>
      H<rsub|m,n>=<mid|{><tabular|<tformat|<table|<row|<cell|i<space|1em>if<space|1em>m=n
      \ or 0 ??>>|<row|<cell|t<rsub|1><space|1em>if<space|1em>g<around*|[|m|]>
      and g<around*|[|n|]> <math-bf|nn> and same
      cell<space|1em>>>|<row|<cell|t<rsub|2><space|1em>if<space|1em>g<around*|[|m|]>
      and g<around*|[|n|]> <math-bf|nn> and <math-bf|not> same cell>>>>>
    </equation*>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/grid_10.pdf|0.5par|||><image|/home/these/localization-of-light-in-2d/DOS_t1=1_t2=1.pdf|0.5par|||>>
      \;
    </big-figure>
  </shown>|<\hidden>
    <tit|Tight-binding on six cell>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/DOS_t1=1_t2=1.5.pdf|0.5par|||><image|/home/these/localization-of-light-in-2d/DOS_t1=1_t2=0.5.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Spin Bott index>

    We introduce:

    <\equation*>
      H<rprime|'>=P\<sigma\>P
    </equation*>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/DOS_psp_p_t1=1_t2=0.5.pdf|0.5par|||>
    <image|/home/these/localization-of-light-in-2d/DOS_psp_p_t1=1_t2=1.5.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Spin Bott Index>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/spectrum_psp_p_t2=0.5.pdf|0.5par|||>
    <image|/home/these/localization-of-light-in-2d/spectrum_psp_p_t2=1.5.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Spin Bott index>

    Mettre la valeur du SBI pour omega grandissant.

    <math|\<rightarrow\>> impossible de localiser le gap � l'aide de cet
    indice d�sormais ?\ 
  </hidden>|<\hidden>
    <tit|SBI versus disorder>

    \;
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>