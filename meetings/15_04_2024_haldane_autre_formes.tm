<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\hidden>
    Discussion avec Frederic Faure:

    <\itemize>
      <item>Membre du jury

      <item>Formalisme <math|c<rsub|i><rsup|\<dagger\>>c<rsub|j>>

      <item>Autre g�om�trie pour Haldane ?\ 
    </itemize>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/chapter-2-thesis/figs/lattice_circle.pdf|0.45par|||>
    <image|/home/pierre/chapter-2-thesis/figs/dos_lattice_circle.pdf|0.45par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/chapter-2-thesis/figs/imaps/imap_-0.0_483_dark.pdf|0.5par|||>
    <image|/home/pierre/chapter-2-thesis/figs/imaps/imap_0.294_507_dark.png|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/chapter-2-thesis/figs/lattice_hole.pdf|0.45par|||>
    <image|/home/pierre/chapter-2-thesis/figs/dos_lattice_hole.pdf|0.45par|||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    <\big-figure|<image|/home/pierre/chapter-2-thesis/figs/imaps/imap_0.033_1055_dark.pdf|0.45par|||>
    <image|/home/pierre/chapter-2-thesis/figs/imaps/imap_0.157_1062_dark.pdf|0.45par|||>>
      \;
    </big-figure>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-2|<tuple|2|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-4|<tuple|4|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>