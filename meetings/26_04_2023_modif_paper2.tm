<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure>
      \ <image|/home/these/phd-drafts/BZ_tb.pdf|0.45par|||><image|/home/these/localization-of-light-in-2d/Correlation
      on band diagramt_2 = 1.pdf|0.45par|||>

      <image|/home/these/localization-of-light-in-2d/Correlation on band
      diagramt_2 = 0.5.pdf|0.45par|||> <image|/home/these/localization-of-light-in-2d/Correlation
      on band diagramt_2 = 1.5.pdf|0.45par|||>\ 
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    \;

    \;

    <\big-figure|<image|/home/these/localization-of-light-in-2d/Spectrum of
    \ P\<sigma\> P \ on Brillouin zone - \ \ t_2 = 0.5.pdf|0.5par|||>
    <image|/home/these/localization-of-light-in-2d/Spectrum of \ P\<sigma\> P
    \ on Brillouin zone - \ \ t_2 = 1.5.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    Computing analytical Chern number by developping
    <math|H<around*|(|<math-bf|k>|)>> around <math|\<Gamma\>>:

    <\equation*>
      P=<around*|[|p<rsup|+>,d<rsup|+>,p<rsup|->,d<rsup|->,s,f|]>
    </equation*>

    Then we compute:

    <\equation*>
      P<rsup|-1>H<rsup|fo><around*|(|<math-bf|k>|)>P
    </equation*>

    And we restrain to coefficient on the left upperside and we obtain:

    <\equation*>
      <matrix|<tformat|<table|<row|<cell|-t<rsub|1>+t<rsub|2>>|<cell|-t<rsub|2><around*|(|-<sqrt|3>k<rsub|x><around*|(|.25-.5i|)>+k<rsub|y><around*|(|.75+.5i|)>|)>>|<cell|0>|<cell|-0.25i*t<rsub|2><around*|(|k<rsub|x><sqrt|3>+k<rsub|y>|)>>>|<row|<cell|>|<cell|t<rsub|1>-t<rsub|2>>|<cell|-t<rsub|2><around*|(|.25<sqrt|3><around*|(|k<rsub|y>+k<rsub|x>|)>|)>>|<cell|0>>|<row|<cell|>|<cell|>|<cell|-t<rsub|1>+t<rsub|2>>|<cell|\<ldots\>.>>|<row|<cell|>|<cell|>|<cell|>|<cell|t<rsub|1>-t<rsub|2>>>>>>
    </equation*>

    which is not the form:

    <\equation*>
      <matrix|<tformat|<table|<row|<cell|H<rsub|+>>|<cell|0>>|<row|<cell|0>|<cell|H<rsub|->>>>>>
    </equation*>

    But possible to find the eigenvectors.

    <\itemize>
      <item>Trouble dropping small terms.
    </itemize>
  </shown>|<\hidden>
    <\itemize>
      <item>Nanoribbon:
    </itemize>

    <\eqnarray*>
      <tformat|<table|<row|<cell|E*A<rsub|i,n>\<mathe\><rsup|i*k<rsub|x>x<rsub|j>>>|<cell|=>|<cell|t<rsub|2><around*|(||\<nobracket\>>A<rsub|i,n>\<mathe\><rsup|i*k<rsub|x><around*|(|x<rsub|j>+R|)>>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|i,n+1>\<mathe\><rsup|i*k<rsub|x><around*|(|x<rsub|j>+R/2|)>>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|i,n+1>\<mathe\><rsup|i*k<rsub|x><around*|(|x<rsub|j>-R/2|)>>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|i,n>\<mathe\><rsup|i*k<rsub|x><around*|(|x<rsub|j>-R|)>>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|i,n-1>\<mathe\><rsup|i*k<rsub|x><around*|(|x<rsub|j>-R/2|)>>>>|<row|<cell|>|<cell|+>|<cell|A<rsub|i,n-1>\<mathe\><rsup|i*k<rsub|x><around*|(|x<rsub|j>+R/2|)>>
      <around*|\<nobracket\>||)>>>|<row|<cell|>|<cell|+>|<cell|t<rsub|1><around*|(|<big|sum><rsub|m\<neq\>i>A<rsub|m,n>\<mathe\><rsup|i*k<rsub|x>x<rsub|j>>|)>>>>>
    </eqnarray*>

    Gives by assuming <math|\<delta\>=2cos<around*|(|k<rsub|x>R|)>> and
    <math|\<gamma\>=2cos<around*|(|k<rsub|x>R/2|)>>:

    <\eqnarray*>
      <tformat|<table|<row|<cell|E*A<rsub|i,n>>|<cell|=>|<cell|t<rsub|2><around*|(|\<delta\>A<rsub|i,n>+\<gamma\>A<rsub|i,n-1>+\<gamma\>A<rsub|i,n+1>|)>>>|<row|<cell|>|<cell|+>|<cell|t<rsub|1><around*|(|<big|sum><rsub|m\<neq\>i>A<rsub|m,n>|)>>>>>
    </eqnarray*>
  </hidden>|<\hidden>
    \;
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_7.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>
    </associate>
  </collection>
</auxiliary>