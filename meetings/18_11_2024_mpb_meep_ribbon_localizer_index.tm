<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\hidden>
    Following the meeting of the 15/11 with Adolfo:

    <\itemize>
      <item><with|color|red|TODO>: Compute the Localizer index for the
      Kane-Mele model WITH spin mixing

      <item><with|color|green|DONE>: Compute the localizer index for the HBZ
      model

      <\equation*>
        H=<frac|1|2><matrix|<tformat|<table|<row|<cell|1>|<cell|1>>|<row|<cell|1>|<cell|-1>>>>><matrix|<tformat|<table|<row|<cell|h<around*|(|k|)>>|<cell|>>|<row|<cell|>|<cell|h<rsup|*\<ast\>><around*|(|k|)>>>>>><matrix|<tformat|<table|<row|<cell|1>|<cell|1>>|<row|<cell|1>|<cell|-1>>>>>
      </equation*>

      if <math|\<psi\>=<around*|(|A\<uparrow\> A\<downarrow\> B\<uparrow\>
      B\<downarrow\>|)>> then multiply every block by a <math|4\<times\>4 >
      matrix made of 2 Had matrices

      <item><with|color|red|TODO>: Add disorder in photonic kekule
    </itemize>

    Following the meeting of the 08/11 with Sergey:

    <\itemize>
      <item>Real need to model the experiment with finite-element method to
      explain the absence of counter propagative mode in the experiment

      <item><with|color|red|<with|color|green|DONE>>: Band diagram for
      ribbon: three cases: no interface, topo-trivial, topo-air, check setup

      <item><with|color|red|TODO>: Meep simulation for a Gaussian pulse with
      <math|f> and <math|f<rsub|width>>: check BD and setup

      <item><with|color|red|TODO>: TAI in Kane-Mele model
    </itemize>
  </hidden>|<\hidden>
    But also some urgent point to treat:

    <\itemize>
      <item><with|color|green|DONE>: Release a stable version for pybott

      <item><with|color|green|DONE>: Release doc

      <item><with|color|green|DONE>: Add examples as complete tar gz

      <item><with|color|green|DONE>: Finish the poster

      <item><with|color|red|TODO>: Write a first draft of the paper on pybott

      <item><with|color|green|DONE>: Include corrections to the thesis

      <\itemize>
        <item>Chapter 2: <with|color|green|DONE>

        <item>Chapter 4,5,6,7: <with|color|green|DONE>

        <item>Chapter 1,3: <with|color|red|TODO>
      </itemize>

      <item><with|color|red|TODO>: Complete my part in lolitop paper

      <item><with|color|red|TODO>: Light Simulator ?
    </itemize>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/posters/poster_gdr_2024/poster.pdf|0.3par|||>>
      Poster GDR
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/chapter-5-thesis/code/bands_strip.pdf|0.6par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/chapter-5-thesis/code/bands_strip_neutre.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    \;
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|16:9>
    <associate|page-width|auto>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|?>>
    <associate|auto-3|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Poster GDR
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>