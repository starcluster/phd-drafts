<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\hidden>
    <\with|par-mode|center>
      <image|/home/wulles/chapter-5-thesis/figs/kekule_mpb_tb_comparison_0.32_0.96.pdf|0.3par|||>
      \ <image|/home/wulles/chapter-5-thesis/figs/kekule_mpb_tb_comparison_0.33_1.0.pdf|0.3par|||><image|/home/wulles/chapter-5-thesis/figs/kekule_mpb_tb_comparison_0.34_1.03.pdf|0.3par|||>

      <image|/home/wulles/chapter-5-thesis/figs/band_surface_0.96.pdf|0.25par|||><space|1em>
      <image|/home/wulles/chapter-5-thesis/figs/band_surface_1.0.pdf|0.25par|||><space|2em><image|/home/wulles/chapter-5-thesis/figs/band_surface_1.5.pdf|0.25par|||>
    </with>
  </hidden>|<\shown>
    <image|/home/wulles/chapter-5-thesis/figs/kekule_mpb_modes_0.05_0.1.pdf|0.5par|||>
    <image|/home/wulles/chapter-5-thesis/figs/kekule_mpb_modes_0.05_1.2.pdf|0.5par|||>

    <\with|par-mode|center>
      \;
    </with>
  </shown>|<\hidden>
    <\wide-tabular>
      <tformat|<cwith|1|1|2|2|cell-valign|c>|<table|<row|<\cell>
        <image|/home/wulles/chapter-5-thesis/figs/kekule_mpb_gradient_0.33_1.0.pdf|0.5par|||>
      </cell>|<\cell>
        <math|<frac|d\<omega\>|d*k>=0.2c> around <math|\<Gamma\>>
      </cell>>>>
    </wide-tabular>
  </hidden>|<\hidden>
    <image|/home/wulles/chapter-5-thesis/figs/honeycomb_mpb_0.2.pdf|0.5par|||><image|/home/wulles/chapter-5-thesis/figs/honeycomb_mpb_3D_0.2.pdf|0.5par|||>
    </hidden>|<\hidden>
    Plan soutenance

    <\itemize>
      <item>Introduction

      <\itemize>
        <item>Topologie (Tight-binding, Chern, Bott, Berry, QHE)

        <item>Atomes froids (Pertczel, interaction via EM)
      </itemize>

      <item>Tight-binding\ 

      <\itemize>
        <item>Kekule

        <item>Lien avec les expériences Nice
      </itemize>

      <item>Atomes froids

      <\itemize>
        <item>Scipost paper

        <item>Opens a gap without mag field
      </itemize>

      <item>Conclusion and opening on TAI and Anderson Localization
    </itemize>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|16:9>
    <associate|page-width|auto>
  </collection>
</initial>