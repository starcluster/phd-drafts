<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/visu_psi/re/980.pdf|0.77par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/visu_psi/re/1020.pdf|0.77par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    Tight-binding model:

    <\big-figure|<image|/home/these/localization-of-light-in-2d/tb/810.png|0.82par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/tb/1200.png|0.82par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/test1.2.pdf|0.44par|||><image|/home/these/localization-of-light-in-2d/test0.8.pdf|0.44par|||>>
      left <math|t<rsub|2>=1.2>, right <math|t<rsub|2>=0.8>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/hist_tb.pdf|0.8par|||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    Chern number based on:

    <\equation*>
      \<sigma\>=<matrix|<tformat|<table|<row|<cell|s>|<cell|p<rsup|+>>|<cell|p<rsup|->>|<cell|d<rsup|+>>|<cell|d<rsup|->>|<cell|f>>>>><matrix|<tformat|<table|<row|<cell|0>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|1>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|>|<cell|-1>|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|1>|<cell|>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|1>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|0>>>>><matrix|<tformat|<table|<row|<cell|s>|<cell|p<rsup|+>>|<cell|p<rsup|->>|<cell|d<rsup|+>>|<cell|d<rsup|->>|<cell|f>>>>><rsup|-1>
    </equation*>

    Then we compute <math|\<sigma\>\<psi\><around*|(|k|)>> and then depending
    of the sign we can say if it's or not

    So far, <math|\<psi\>> are not eigenvectors of
    <math|\<sigma\>><text-dots>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
    <associate|auto-6|<tuple|6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        left <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>=1.2>>,
        right <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>=0.8>>
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        \;
      </surround>|<pageref|auto-6>>
    </associate>
  </collection>
</auxiliary>