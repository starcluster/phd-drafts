<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <section*|Anderson disorder>

    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_1.5_96_0.0.pdf|0.35par|||>
      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_1.5_96_1.02.pdf|0.35par|||>\ 

      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_0.5_96_0.0.pdf|0.35par|||>
      <image|/home/these/localization-of-light-in-2d/histo_kekule/histo_0.5_96_1.02.pdf|0.35par|||>
    <|big-figure>
      Disorder added on the diagonal, expected behaviour ? localization
      everywhere.
    </big-figure>
  </shown>|<\hidden>
    <section*|Spin Chern number for TE modes>

    We have the hamiltonian:

    <\equation*>
      H<around*|(|<math-bf|k>|)>\<in\>\<cal-M\><rsub|12><around*|(|\<bbb-C\>|)>
    </equation*>

    With its associated eigenvectors:

    <\equation*>
      \<psi\><rsub|i>,1\<leqslant\>i\<leqslant\>12
    </equation*>

    Then we define for angles <math|\<varphi\>=<around*|{|<frac|k\<pi\>|3>,k=0\<ldots\>5|}>>:No
    should be <math|<around*|{|<around*|(|2i-3|)>\<pi\>/6|}>> ? Depends.

    <\equation*>
      \<eta\><rsub|i>=<rsup|t><around*|[|\<psi\><rsub|i><rsup|0>cos<around*|(|\<varphi\><rsub|0>|)>+\<psi\><rsub|i><rsup|1>sin<around*|(|\<varphi\><rsub|0>|)>,\<ldots\>|]>
    </equation*>

    From these new vectors we build the projector <math|P>. The new
    hamiltonian becomes:

    <\equation*>
      <wide|H|~><around*|(|<math-bf|k>|)>=P\<sigma\>P\<in\>\<cal-M\><rsub|6><around*|(|\<bbb-C\>|)>
    </equation*>

    But problem: eigenvalues of <math|P> are not <math|0> or <math|1>.
  </hidden>|<\hidden>
    <section*|Wave propagation>

    A solution can be represented by:

    <\equation*>
      u<around*|(|\<omega\>|)>=<big|sum><rsub|n>A<rsub|n><around*|(|\<omega\>|)>R<rsub|n>
    </equation*>

    With:

    <\equation*>
      A<rsub|n><around*|(|\<omega\>|)>=<frac|1|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2><frac|<around*|\<langle\>|L<rsub|n><around*|\||v<around*|(|\<omega\>|)>|\<nobracket\>>|\<rangle\>>|\<omega\>-\<omega\><rsub|n>+i\<Gamma\><rsub|n>/2>
    </equation*>

    Thus to obtain the solution as a function of time, we do the reverse
    Fourier transform:

    <\equation*>
      A<rsub|n><around*|(|t|)>=\<frak-F\><rsup|-1><around*|(|<frac|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2|\<omega\>-\<omega\><rsub|n>+i\<Gamma\><rsub|n>/2><around*|\<langle\>|L<rsub|n><around*|\||v<around*|(|\<omega\>|)>|\<nobracket\>>|\<rangle\>>|)><around*|(|t|)>
    </equation*>

    But if we consider the source to be constant then:

    <\equation*>
      v<rsub|i*j><around*|(|\<omega\>|)>=v<rsub|i*j>=<mid|{><tabular*|<tformat|<table|<row|<cell|A<rsub|0>H<around*|(|k<rsub|0>r<rsub|i*j>|)>>|<cell|if
      i\<neq\>j>>|<row|<cell|A<rsub|0>>|<cell|elsewhere>>>>>
    </equation*>

    does not depend of <math|\<omega\>>, finally:

    <\equation*>
      A<rsub|n><around*|(|t|)>=-i<around*|\<langle\>|L<rsub|n><around*|\||v|\<nobracket\>>|\<rangle\>>exp<around*|(|-i*\<omega\><rsub|n>t-t\<Gamma\><rsub|n>/2|)><around*|(|\<omega\><rsub|n>-\<omega\><rsub|0>-i<around*|(|\<Gamma\><rsub|n>-\<Gamma\><rsub|0>|)>/2|)>
    </equation*>
  </hidden>|<\hidden>
    \;

    <\wide-tabular>
      <tformat|<cwith|1|1|1|1|cell-halign|c>|<table|<row|<\cell>
        <strong|Problem>: How do we select the eigenstate on the edge ?
      </cell>>>>
    </wide-tabular>

    \;
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|1|1>>
    <associate|auto-4|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Disorder added on the diagonal, expected behaviour ? localization
        everywhere.
      </surround>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Anderson
      disorder> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Spin
      Chern number for TE modes> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Wave
      propagation> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>