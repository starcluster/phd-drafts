<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    Erreur, pour un vecteur complexe:

    <\equation*>
      x\<cdot\>y=<big|sum>x<rsub|i><wide|y<rsub|i>|\<bar\>>
    </equation*>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/visu_psi/re/980.pdf|0.7par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/visu_psi/re/1020.pdf|0.7par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/6cell/a1.02_d2_b6b7.pdf|0.8par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\shown>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/6cell/a0.98_d2_b6b7.pdf|0.8par|||>>
      \;
    </big-figure>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|23_03_2023_histo.tm>>
    <associate|auto-2|<tuple|2|1|23_03_2023_histo.tm>>
    <associate|auto-3|<tuple|3|1|23_03_2023_histo.tm>>
    <associate|auto-4|<tuple|4|?|23_03_2023_histo.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>