<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/6cell/a1.09_d0_b6b7.png|0.45par|||><image|/home/these/localization-of-light-in-2d/6cell/a0.91_d0_b6b7.png|0.45par|||>

      <image|/home/these/localization-of-light-in-2d/6cell/a1.09_d2_b6b7.png|0.45par|||><image|/home/these/localization-of-light-in-2d/6cell/a0.91_d2_b6b7.png|0.45par|||>
    <|big-figure>
      \;
    </big-figure>
  </shown>|<\hidden>
    Dégénerescence ?

    <\big-figure|<image|/home/these/localization-of-light-in-2d/6cell/9000.png|0.75par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/phd-drafts/exp_a1.png|0.45par|||><image|/home/these/phd-drafts/exp_a1.1.png|0.45par|||>

      <image|/home/these/localization-of-light-in-2d/hist_6cell_a1.0.pdf|0.45par|||><image|/home/these/localization-of-light-in-2d/hist_6cell_a1.1.pdf|0.45par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/phd-drafts/exp_a0.92.png|0.45par|||>

      <image|/home/these/localization-of-light-in-2d/hist_6cell_a0.92.pdf|0.45par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    \;
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>