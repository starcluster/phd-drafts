<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/t2_transition.pdf|0.9par|||>>
      Now I introduce disorder ?
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter2/figs/DOS and Bott indext_1 = 1 -
    t_2 = 0 - M = 0.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/chapter2/figs/DOS and Bott indext_1 = 1 - t_2 =
      (0.08-0.13j) - M = 1.pdf|0.5par|||>
      <image|/home/these/chapter2/figs/DOS and Bott indext_1 = 1 - t_2 = 0 -
      M = 1.pdf|0.5par|||>

      \ 
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter2/figs/Kane-Mele Band structuret =
    1 - \<lambda\>_S = 0.3 - \<lambda\>_R = 0.25 - \<lambda\>_v = 1.pdf||||>>
      Chern number incoherent
    </big-figure>
  </hidden>|<\shown>
    <\big-figure|<image|/home/these/chapter2/figs/Berry Curvaturet_2=0.15
    M=0.2.pdf|0.5par|||><image|/home/these/chapter2/figs/Berry Curvature
    KM.pdf|0.5par|||>>
      Graphene left, KM right
    </big-figure>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-2|<tuple|2|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-4|<tuple|4|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-5|<tuple|5|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Now I introduce disorder ?
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        Chern number incoherent
      </surround>|<pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>