<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <tit|TAI in Haldane/Kane-Mele>

    <\big-figure|<image|/home/these/chapter-2-thesis/code/w=0.842.png|0.5par|||>>
      \;
    </big-figure>
  </shown>|<\hidden>
    <tit|Study of <math|\<sigma\><around*|(|U*V*U<rsup|-1>*V<rsup|-1><rsup|>|)>>>

    <\big-figure|<image|/home/these/chapter-2-thesis/anim/uvuv_param_topo/eigenvalues_uvuv_233.png|0.7par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Signification of Bott Index>

    Set of orthogonal localized functions.

    <\itemize>
      <item><math|H> can deform into <math|H<rsub|Trivial>> without closing
      the gap <math|\<Rightarrow\>> localized wannier function

      <item>no localized wannier function <math|\<Rightarrow\>> <math|H> is
      topological
    </itemize>

    Meaning of\ 

    <\equation*>
      U=P*exp<around*|(|<frac|2\<pi\>i|L<rsub|x>>X|)>P and
      V=P*exp<around*|(|<frac|2\<pi\>i|L<rsub|x>>Y|)>P
    </equation*>

    not clear yet.
  </hidden>|<\hidden>
    <tit|Talk>

    Blackboard talk ?

    <\big-figure|<image|/home/these/phd-drafts/talk_cheatsheet.png|0.3par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Band diagram>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/band_diagram_2.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Spin Chern evolution>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/spin_chern_distance=2.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Phase diagram>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/phase_diagram_spin_chern_deform_2.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Localized state>

    <\big-figure|<image|/home/these/chapter-6-thesis/figs/imap_3.5_inset.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Topological Photonic Crystal Fiber>

    <\big-figure|<image|/home/these/phd-drafts/sciadv.add3522-f1.jpg|0.3par|||><image|/home/these/phd-drafts/sciadv.add3522-f2.jpg|0.6par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|News>

    Ma th�se en 180s le 13 f�vrier.
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
    <associate|auto-6|<tuple|6|?>>
    <associate|auto-7|<tuple|7|?>>
    <associate|auto-8|<tuple|8|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        \;
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        \;
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|8>|>
        \;
      </surround>|<pageref|auto-8>>
    </associate>
  </collection>
</auxiliary>