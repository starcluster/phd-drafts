<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\equation*>
      Bott<around*|(|U,V|)>=<frac|1|2\<pi\>i>Tr<around*|(|log<around*|(|U*V*U<rsup|-1>V<rsup|-1>|)>|)>
    </equation*>

    <\equation*>
      Bott<around*|(|U,V|)>=<frac|1|2\<pi\>i>Im<around*|(|Tr<around*|(|log<around*|(|U*V*U<rsup|\<dagger\>>V<rsup|\<dagger\>>|)>|)>|)>
    </equation*>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/re_part_bott.pdf|0.5par|||>
    <image|/home/these/localization-of-light-in-2d/im_part_bott.pdf|0.5par|||>>
      \;
    </big-figure>

    \;
  </hidden>|<\hidden>
    Conditions is required to go from one to another:

    <\equation*>
      U*U<rsup|\<dagger\>>=I
    </equation*>

    Or:

    <\equation*>
      <around*|\<\|\|\>|U*U<rsup|\<dagger\>>-I|\<\|\|\>>=\<cal-O\><around*|(|<frac|1|L<rsub|x>L<rsub|y>>|)>
    </equation*>

    But here:

    <\big-figure|<math|<image|/home/these/localization-of-light-in-2d/vv_dagger.pdf|0.49par|||>
    <image|/home/these/localization-of-light-in-2d/uu_dagger.pdf|0.49par|||>>>
      <math|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/uv_vu.pdf||||>>
      <math|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>
    </big-figure>
  </hidden>|<\hidden>
    How to define <math|U> and <math|V> ?\ 

    Usually we define:

    <\equation*>
      P=<big|sum><rsub|\<omega\><rsub|n>\<less\>\<omega\><rsub|gap>>\|<around*|\<nobracket\>|\<psi\><rsub|n>|\<rangle\>><around*|\<langle\>|\<psi\><rsub|n>|\|>
    </equation*>

    <\equation*>
      U=P*exp<around*|(|2\<pi\>i*X/L<rsub|x>|)>P+<with|color|#a0a0a0|<around*|(|I-P|)>><space|1em>and<space|1em>V=P*exp<around*|(|2\<pi\>i*Y/L<rsub|y>|)>P+<with|color|pastel
      grey|<with|color|light grey|<with|color|#a0a0a0|<around*|(|I-P|)>>>>
    </equation*>

    but we can also define them as:

    <\equation*>
      W=<matrix|<tformat|<table|<row|<cell|\<psi\><rsub|1>>|<cell|\<psi\><rsub|2>>|<cell|\<ldots\>>|<cell|\<psi\><rsub|k>>>>>>
    </equation*>

    where <math|k> is so that: <math|\<omega\><rsub|k>=\<omega\><rsub|gap>>,
    thus:

    <\equation*>
      U=W<rsup|\<dagger\>>exp<around*|(|2\<pi\>i*X/L<rsub|x>|)>W<space|1em>and<space|1em>V=W<rsup|\<dagger\>>exp<around*|(|2\<pi\>i*Y/L<rsub|y>|)>W
    </equation*>

    and we get the exact same result but way faster and simpler <text-dots>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/uu_dagger_W.pdf|0.5par|||>
      <image|/home/these/localization-of-light-in-2d/vv_dagger_W.pdf|0.5par|||>\ 

      \;
    <|big-figure>
      <math|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/uv_vu_W.pdf|0.5par|||>>
      <math|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>
    </big-figure>
  </hidden>|<\hidden>
    Going back to six cell model and no pbc: <math|t<rsub|1>> is
    intra-cluster and <math|t<rsub|2>> is extra cluster hoping:

    <\big-figure|<image|/home/these/localization-of-light-in-2d/sbi_t2.pdf||||>>
      \;
    </big-figure>

    \;
  </hidden>|<\hidden>
    But for PBC:

    <\big-figure|<image|/home/these/localization-of-light-in-2d/sbi_t2_pbc.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    Milonni: not matching ?

    <\big-figure|<image|/home/these/localization-of-light-in-2d/re_G.pdf||||>>
      \;
    </big-figure>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-4|<tuple|4|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-5|<tuple|5|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-6|<tuple|6|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-7|<tuple|7|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-8|<tuple|8|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>>
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>>
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>>
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<Delta\><rsub|B>=-12,\<Delta\><rsub|AB>=6>>
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        \;
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        \;
      </surround>|<pageref|auto-7>>
    </associate>
  </collection>
</auxiliary>