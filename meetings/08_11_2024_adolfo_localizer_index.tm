<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\shown>
    <\big-figure|<image|/home/pierre/pybott/tests/N=200_t2=0.2j_delta=0_kappa=0.05_haldane.pdf|0.5par|||>
    <image|/home/pierre/pybott/tests/N=200_t2=0.1j_delta=0_kappa=0.05_haldane.pdf|0.5par|||>>
      Localizer index in the Haldane model
    </big-figure>
  </shown>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/N=144_t2=0.2j_delta=0_kappa=0.038_rashba=0_km.pdf|0.5par|||>
    <image|/home/pierre/pybott/tests/N=144_t2=0.2j_delta=0_kappa=0.038_rashba=0_km_zoom.pdf|0.5par|||>>
      Localizer index in the Kane-Mele Model with no Rashba coupling
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/pierre/pybott/tests/N=144_t2=0.2j_delta=0_kappa=0.0478_rashba=1_km.pdf|0.5par|||>
    <image|/home/pierre/pybott/tests/N=144_t2=0.2j_delta=0_kappa=0.0478_rashba=1_km_zoom.pdf|0.5par|||>>
      Localizer index in the Kane-Mele Model with Rashba coupling
    </big-figure>
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|16:9>
    <associate|page-width|auto>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_3.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_3.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_3.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Localizer index in the Haldane model
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Localizer index in the Kane-Mele Model with no Rashba coupling
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Localizer index in the Kane-Mele Model with Rashba coupling
      </surround>|<pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>