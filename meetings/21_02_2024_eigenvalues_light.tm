<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\hidden>
    <tit|News>

    <\itemize>
      <item>Paper on Scipost <math|\<rightarrow\>> waiting for new version:
      add non hermitian topology ?

      <item>MT180

      <item>New figs for chapter 6 + beginning of chapter 6

      <item>Should I start chapter 5 ?

      <item>Starting to do new figures for chapter 4.
    </itemize>
  </hidden>|<\hidden>
    <tit|Eigenvalues of <math|U*V*U<rsup|-1>V<rsup|-1>>>

    <\big-figure|<image|/home/pierre/chapter-4-thesis/anim/eigenvalues_uvuv_77.png|0.6par|||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    <tit|Meeting with Loic >

    <\itemize>
      <item>Study the transition for the plates reflection going from 0 to 1

      <item>Plot the eigenvalues in RE/IM plane for a cylinder

      <item>Main idea: for non hermitian topology, no bounding of the
      spectrum like in hermitian topology

      <item>What matters are the singular value: <math|H<rsup|\<dagger\>>H>\ 
    </itemize>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>