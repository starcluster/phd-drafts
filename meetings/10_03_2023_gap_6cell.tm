<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/2cell_a1_d0.pdf|0.44par|||><image|/home/these/localization-of-light-in-2d/6cell_a1_d0.pdf|0.44par|||>

      <image|/home/these/localization-of-light-in-2d/2cell_a1.0.pdf|0.44par|||><image|/home/these/localization-of-light-in-2d/6cell_a1_d2.pdf|0.44par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/2cell_a1_d0_dB8.pdf|0.44par|||><image|/home/these/localization-of-light-in-2d/6cell_a1.1_d0_dB8.pdf|0.44par|||>

      <image|/home/these/localization-of-light-in-2d/2cell_a1_d2_dB8.pdf|0.44par|||><image|/home/these/localization-of-light-in-2d/6cell_a1.1_d2.pdf|0.44par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/6cell_gap_d0.pdf|0.9par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/6cell_gap_gamma_d2.pdf|0.7par|||>>
      \;
    </big-figure>

    <\equation*>
      y<rsub|pow>\<approx\>4x<rsup|20>
    </equation*>

    <\equation*>
      y<rsub|lin>\<approx\>-400x+400
    </equation*>
  </hidden>|<\shown>
    <\equation*>
      M<rsub|i\<nocomma\>j><rsup|\<alpha\>\<nocomma\>\<beta\>><around*|(|<math-bf|k>|)>=<frac|1|\<cal-A\>><big|sum><rsub|<tabular*|<tformat|<table|<row|<cell|<math-bf|q><rsub|m>\<in\>\<frak-F\>>>>>>>g<rsup|\<alpha\>\<beta\>*><rsub|><around*|(|<math-bf|q><rsub|m>-<math-bf|k>|)>\<mathe\><rsup|i*<around*|(|<math-bf|q><rsub|m>-<math-bf|k>|)>\<cdummy\><around*|(|<math-bf|><math-bf|a><rsub|i>-<math-bf|a><rsub|j>|)>>
    </equation*>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/6cell_matrix.pdf|0.87par|||>>
      \;
    </big-figure>

    \;

    \;

    \;
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|1>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>
    </associate>
  </collection>
</auxiliary>