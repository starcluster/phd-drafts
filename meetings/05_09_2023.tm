<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure>
      <image|/home/these/kanemele_fig_3_b.pdf|0.4par|||>
      <image|/home/these/kanemele_fig_2_e.pdf|0.4par|||>\ 

      <image|/home/these/kanemele_fig_2_a.pdf|0.4par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    We assume:

    <\equation*>
      H<rsub|Kane-Mele>=<matrix|<tformat|<table|<row|<cell|H<rsub|Haldane><around*|(|t<rsub|2>|)>>|<cell|0>>|<row|<cell|0>|<cell|H<rsub|Haldane><around*|(|<wide|t<rsub|2>|\<bar\>>|)>>>>>>
    </equation*>

    To compute the spin Chern number, we take eigenvectors below the gap and
    above the gap:

    <\equation*>
      P<rsub|->=<big|sum><rsub|i\<less\>m>q<rsub|i>\<otimes\>q<rsub|i><space|1em>P<rsub|+>=<big|sum><rsub|i\<geqslant\>m>q<rsub|i>\<otimes\>q<rsub|i>
    </equation*>

    And we compte Chern number for the new \S Hamiltonian \T:

    <\equation*>
      H<rsub|\<pm\>>=P<rsub|\<pm\>>\<sigma\>P<rsub|\<pm\>>
    </equation*>

    What is <math|\<sigma\>> ?
  </hidden>|<\shown>
    For Kane-Mele in real space, we start from:

    <\equation*>
      H<rsub|haldane><around*|(|t<rsub|2>|)>=<matrix|<tformat|<table|<row|<cell|-\<delta\>\<gamma\>>|<cell|\<delta\>>|<cell|\<gamma\>>|<cell|1>|<cell|>>|<row|<cell|\<delta\>>|<cell|\<delta\>\<gamma\>>|<cell|0>|<cell|-\<gamma\>>|<cell|>>|<row|<cell|\<gamma\>>|<cell|0>|<cell|-\<delta\>\<gamma\>>|<cell|\<delta\>>|<cell|>>|<row|<cell|1>|<cell|-\<gamma\>>|<cell|\<delta\>>|<cell|\<delta\>\<gamma\>>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|\<ddots\>>>>>>
    </equation*>

    So:

    <\equation*>
      H<rsub|Kane-Mele>=<matrix|<tformat|<table|<row|<cell|H<rsub|haldane><around*|(|t<rsub|2>|)>>|<cell|>>|<row|<cell|>|<cell|H<rsub|haldane><around*|(|<wide|t<rsub|2>|\<bar\>>|)>>>>>>
    </equation*>

    For the spin Bott index, the hamiltonian will be:

    <\equation*>
      H<rprime|'>=P\<sigma\>P
    </equation*>

    What is <math|\<sigma\>> ?

    \;
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>