<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\itemize>
      <item>Plot <math|<around*|\<\|\|\>||\<\|\|\>>> as a function of the
      size

      <item>Try to compute Bott index for Haldane model with and without PBC
      with a simple lattice

      <item>Check figs for the paper
    </itemize>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter2/figs/DOS and Bott indext_1 = -1
    - t_2 = (0.08-0.13j) - M = 0.0.pdf|0.5par|||><image|/home/these/chapter2/code/haldane_fin_dos_true.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    <\big-figure|<image|/home/these/chapter2/figs/DOS and Bott indext_1 = -1
    - t_2 = 0 - M = 0.pdf||||>>
      \;
    </big-figure>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>