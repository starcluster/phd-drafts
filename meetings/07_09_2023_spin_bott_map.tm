<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/bott_map_psp_e_0.9_216.pdf|0.45par|||>
      <image|/home/these/localization-of-light-in-2d/bott_map_psp_e_1.1_216.pdf|0.45par|||>\ 

      <image|/home/these/localization-of-light-in-2d/bott_map_psp_e_0.95_96.pdf|0.45par|||>
      <image|/home/these/localization-of-light-in-2d/bott_map_psp_e_1.05_96.pdf|0.45par|||>
    <|big-figure>
      \;
    </big-figure>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>