<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\itemize>
      <item>Figure nanoribbon: changer l�gende axe x, explorer les cas
      interfaces ET p�riodiques 1.1 0.9 / 1.1 1.2 faire � chaque fois la
      figure en taille normal � gauche et un zoom � droite

      <item>Figure spin chern spin bott: tracer en ligne continue le spin
      chern puis les spin botts, des points ou des triangles discrets,
      agrandir police dans l'inset et ajouter l'axe des x, �crire
      N=<text-dots>

      <item>Figure band diagram: tracer en unit� de <math|t<rsub|2>> et
      ajouter les cas extr�mes t2 = 0 et t2 = 100 pour la th�se pas l'article
      afin de faire la comparaison avec le cas haxagon � 6 atomes et juste 2
      atomes

      <item>Faire animation du gap fourier projet� sur l'histogramme pour le
      r�seaux EMF � cellules �l�mentaires de 6 atomes

      <item>Calcul du spin bott dans le r�seau TB mais en ajoutant une
      diff�rence dans les <math|t<rsub|1>>, on devra avoir
      <math|t<rsub|1><rsup|A>> et <math|t<rsub|1><rsup|B>> pour esp�rer
      fermer le gap topologique ouvert par <math|t<rsub|2>> et peut-�tre se
      placer dans un gap topologique gap ferm� et observer
      (peut-�tre<text-dots>) un TAI
    </itemize>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>