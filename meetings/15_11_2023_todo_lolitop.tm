<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\itemize>
      <item>Commencer � faire les figures:

      <\itemize>
        <item>Diagramme de bande avec coloration inversion trivial/topo/sans
        gap

        <item>Figure nombre de spin chern et nombre de spin bott + inset
        <math|P\<sigma\>P>

        <item>Figure nanoribbon
      </itemize>

      <item>Commencer � �crire la partie th�orie:

      <\itemize>
        <item>spdf

        <item>spin chern

        <item>spin bott

        <item>Hamiltonien
      </itemize>

      <item>Inclure le terme de masse dans les calculs pour le spin chern et
      le spin bott\ 

      <item>Revoir le spin bott �a doit marcher dans le cas EM
    </itemize>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>