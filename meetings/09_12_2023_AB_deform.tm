<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\big-figure|<image|/home/these/chapter-5-thesis/figs/kekule_hist_6_1.4_0_pbc_True_0.6.pdf||||><image|/home/these/chapter-5-thesis/figs/lattice_supercell.pdf|0.3par|||>>
      <math|\<delta\><rsub|AB>> gives the difference of size between <math|A>
      hexagons and <math|B> hexagons. Black lines shows the gap of
      <math|t<rsub|2>>. Purple lines the gap of <math|\<delta\><rsub|AB>> and
      red lines the gap of the combination of both.
    </big-figure>

    Formula for the gap:

    <\equation*>
      W<rsub|gap>=2<around*|\<\|\|\>|t<rsub|2>-t<rsub|1>|\|>-\<delta\><rsub|AB><around*|\|||\<nobracket\>>
    </equation*>
  </shown>|<\hidden>
    <\big-figure>
      <image|/home/these/chapter-5-thesis/figs/kekule_hist_10_1.0_0_pbc_True_0.5.pdf|0.42par|||><image|/home/these/chapter-5-thesis/figs/kekule_hist_16_1.0_0_pbc_True_0.5.pdf|0.42par|||>\ 

      <image|/home/these/chapter-5-thesis/figs/kekule_hist_16_1.0_0_pbc_False_0.5.pdf|0.42par|||>
      <image|/home/these/chapter-5-thesis/figs/kekule_hist_16_1.6_0_pbc_False_0.5.pdf|0.42par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    Computing phase diagram (spin bott index):

    <\big-figure|<image|/home/these/chapter-5-thesis/figs/bott_map.pdf||||>>
      Asymmetry ?\ 
    </big-figure>
  </hidden>|<\hidden>
    Previous analysis in fourier space:

    For one cell we have:

    <\equation*>
      H=t<rsub|1><matrix|<tformat|<table|<row|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>>>>>+t<rsub|2><matrix|<tformat|<table|<row|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i*<math-bf|k>*\<cdot\><math-bf|a><rsub|1>>>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i*<math-bf|k>*\<cdot\><math-bf|a><rsub|2>>>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i*<math-bf|k>*\<cdot\><around*|(|<math-bf|<math|<math-bf|a><rsub|2>->a><rsub|1>|)>>>>|<row|<cell|\<mathe\><rsup|-i*<math-bf|k>*\<cdot\><math-bf|a><rsub|1>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|\<mathe\><rsup|-i*<math-bf|k>*\<cdot\><math-bf|a><rsub|2>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|-i*<math-bf|k>*\<cdot\><around*|(|<math-bf|<math|<math-bf|a><rsub|2>->a><rsub|1>|)>>>|<cell|0>|<cell|0>|<cell|0>>>>>
    </equation*>

    When the unit cell is made of two hexagons:

    <\big-figure|<image|/home/these/chapter-5-thesis/figs/schema_super_cell.pdf|0.2par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <with|font-base-size|7|<\equation*>
      <with|font-base-size|9|<matrix|<tformat|<table|<row|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i*k*\<varepsilon\><rsub|1>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i*k*\<varepsilon\><rsub|2>>>|<cell|0>>|<row|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i*k*\<varepsilon\><rsub|21>>>>|<row|<cell|t<rsub|2>\<mathe\><rsup|-i*k*\<varepsilon\><rsub|1>>>|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|-i*k*\<varepsilon\><rsub|1>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|1><rsup|A>>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i*k*\<varepsilon\><rsub|1>>>|<cell|0>|<cell|t<rsub|1><rsup|B>>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i*k*\<varepsilon\><rsub|1>>>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|-i*k*\<varepsilon\><rsub|1>>>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>>|<row|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|-i*k*\<varepsilon\><rsub|2>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|t<rsub|1><rsup|B>>>|<row|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|-i*k*\<varepsilon\><rsub|21>>>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|1><rsup|B>>|<cell|0>>>>>>
    </equation*>>

    <\big-figure|<image|/home/these/chapter-5-thesis/figs/FBZ_internet.png|0.6par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/chapter-5-thesis/figs/bd_super_cell_t2=1_\<delta\>AB=0.0.pdf|0.42par|||>
      <image|/home/these/chapter-5-thesis/figs/bd_super_cell_t2=1.2_\<delta\>AB=0.0.pdf|0.42par|||>

      \ <image|/home/these/chapter-5-thesis/figs/bd_super_cell_t2=0_\<delta\>AB=0.0.pdf|0.42par|||>
      <image|/home/these/chapter-5-thesis/figs/bd_super_cell_t2=0_\<delta\>AB=0.5.pdf|0.42par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/chapter-5-thesis/figs/bd_super_cell_t2=1_\<delta\>AB=0.5.pdf|0.4par|||><image|/home/these/chapter-5-thesis/figs/bd_super_cell_t2=1.3_\<delta\>AB=0.3.pdf|0.4par|||>

      <image|/home/these/chapter-5-thesis/figs/kekule_hist_10_1.3_0_pbc_True_0.3.pdf|0.4par|||><image|/home/these/chapter-5-thesis/figs/imap_0.pdf|0.42par|||>

      \;
    <|big-figure>
      Gap should be closed<text-dots>\ 
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/chapter-5-thesis/figs/berry_curvature_super_cell_topo.pdf|0.4par|||>
    <image|/home/these/chapter-5-thesis/figs/berry_curvature_super_cell_trivial.pdf|0.4par|||>>
      to compute chern number new BZ, topo left and trivial right.
    </big-figure>
  </hidden>|<\hidden>
    Spin Bott index for honeycomb lattice with EM interactions:

    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/spin_bott_map_s=0.95.pdf|0.4par|||><image|/home/these/localization-of-light-in-2d/spin_bott_map_s=0.98.pdf|0.4par|||>

      <image|/home/these/localization-of-light-in-2d/spin_bott_map_s=1.02.pdf|0.4par|||><image|/home/these/localization-of-light-in-2d/spin_bott_map_s=1.1.pdf|0.4par|||>
    <|big-figure>
      0.95,0.98,1.02,1.1 <math|\<Rightarrow\>> ??? only the first one makes
      sense.
    </big-figure>
  </hidden>|<\hidden>
    Nanoribbon:
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|1>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
    <associate|auto-6|<tuple|6|?>>
    <associate|auto-7|<tuple|7|?>>
    <associate|auto-8|<tuple|8|1>>
    <associate|auto-9|<tuple|9|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<delta\><rsub|AB>>>
        gives the difference of size between
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|A>>
        hexagons and <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|B>>
        hexagons. Black lines shows the gap of
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>>>.
        Purple lines the gap of <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<delta\><rsub|AB>>>
        and red lines the gap of the combination of both.
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Asymmetry ?\ 
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        \;
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        Gap should be closed.<hspace|0.25spc>.<hspace|0.25spc>.<hspace|0.25spc>
        </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|8>|>
        to compute chern number new BZ, topo left and trivial right.
      </surround>|<pageref|auto-8>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9>|>
        0.95,0.98,1.02,1.1 <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<Rightarrow\>>>
        ??? only the first one makes sense.
      </surround>|<pageref|auto-9>>
    </associate>
  </collection>
</auxiliary>