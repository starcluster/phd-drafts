<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/SPECTRUM_psp_t1=1_t2=1.5.pdf|0.5par|||>
    <image|/home/these/localization-of-light-in-2d/SPECTRUM_psp_t1=1_t2=0.5.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/between_plates/fig_1atom.pdf|0.5par|||><image|/home/these/localization-of-light-in-2d/re_G.pdf|0.5par|||>>
      \;
    </big-figure>

    <\equation*>
      2\<Delta\><rsub|\<perp\>><around*|(|k<rsub|0>L|)>=<frac|6\<pi\>A|k<rsub|0>L><around*|(|<frac|1|2>+<big|sum><rsub|n=1><rsup|<around*|\<lceil\>|k<rsub|0>L/\<pi\>|\<rceil\>>><around*|(|1-<frac|n<rsup|2>\<pi\><rsup|2>|k<rsub|0><rsup|2>L<rsup|2>>|)>cos<around*|(|-<frac|1|2>n\<pi\>|)><rsup|2>|)>
    </equation*>
  </hidden>|<\shown>
    Chern number of <math|H<rsup|+>> and <math|H<rsup|->>:

    \;
  </shown>|<\hidden>
    \;
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>
    </associate>
  </collection>
</auxiliary>