<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\eqnarray*>
      <tformat|<table|<row|<cell|E*A<rsub|1,n>>|<cell|=>|<cell|t<rsub|1><around*|(|A<rsub|2,n>\<mathe\><rsup|-i*<frac|k<rsub|x>a|2>>+A<rsub|6,n>\<mathe\><rsup|-i*<frac|k<rsub|x>a|2>>|)>+t<rsub|2>A<rsub|4,n>\<mathe\><rsup|i**k<rsub|x>a>>>|<row|<cell|E*A<rsub|2,n>>|<cell|=>|<cell|t<rsub|1><around*|(|A<rsub|1,n>\<mathe\><rsup|i*<frac|k<rsub|x>a|2>>+A<rsub|3,n>\<mathe\><rsup|-i*k<rsub|x>a>|)>+t<rsub|2>A<rsub|5,n+1>\<mathe\><rsup|i**<frac|k<rsub|x>a|2>>>>|<row|<cell|E*A<rsub|3,n>>|<cell|=>|<cell|t<rsub|1><around*|(|A<rsub|2,n>\<mathe\><rsup|i*k<rsub|x>a>+A<rsub|4,n>\<mathe\><rsup|-i*<frac|k<rsub|x>a|2>>|)>+t<rsub|2>A<rsub|6,n+1>\<mathe\><rsup|-i**<frac|k<rsub|x>a|2>>>>|<row|<cell|E*A<rsub|4,n>>|<cell|=>|<cell|t<rsub|1><around*|(|A<rsub|3,n>\<mathe\><rsup|i*<frac|k<rsub|x>a|2>>+A<rsub|5,n>\<mathe\><rsup|i*<frac|k<rsub|x>a|2>>|)>+t<rsub|2>A<rsub|1,n>\<mathe\><rsup|-i**k<rsub|x>a>>>|<row|<cell|E*A<rsub|5,n>>|<cell|=>|<cell|t<rsub|1><around*|(|A<rsub|4,n>\<mathe\><rsup|-i*<frac|k<rsub|x>a|2>>+A<rsub|6,n>\<mathe\><rsup|i*k<rsub|x>a>|)>+t<rsub|2>A<rsub|2,n-1>\<mathe\><rsup|-i**<frac|k<rsub|x>a|2>>>>|<row|<cell|E*A<rsub|6,n>>|<cell|=>|<cell|t<rsub|1><around*|(|A<rsub|5,n>\<mathe\><rsup|-i*k<rsub|x>a>+A<rsub|1,n>\<mathe\><rsup|i*<frac|k<rsub|x>a|2>>|)>+t<rsub|2>A<rsub|3,n-1>\<mathe\><rsup|i**<frac|k<rsub|x>a|2>>>>>>
    </eqnarray*>

    <\equation*>
      D=<matrix|<tformat|<table|<row|<cell|0>|<cell|t<rsub|1>\<mathe\><rsup|-i*<frac|k<rsub|x>a|2>>>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i**k<rsub|x>a>>|<cell|0>|<cell|t<rsub|1>\<mathe\><rsup|-i*<frac|k<rsub|x>a|2>>>>|<row|<cell|>|<cell|0>|<cell|t<rsub|1>\<mathe\><rsup|-i*k<rsub|x>a>>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|>|<cell|>|<cell|0>|<cell|t<rsub|1>\<mathe\><rsup|-i*<frac|k<rsub|x>a|2>>>|<cell|0>|<cell|0>>|<row|<cell|t<rsub|2>\<mathe\><rsup|-i**k<rsub|x>a>>|<cell|>|<cell|<frac|a|b><sqrt|<frac|r|x>>>|<cell|0>|<cell|t<rsub|1>\<mathe\><rsup|i*<frac|k<rsub|x>a|2>>>|<cell|0>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|0>|<cell|t<rsub|1>\<mathe\><rsup|i*k<rsub|x>a>>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|0>>>>>
    </equation*>
  </hidden>|<\hidden>
    <\equation*>
      A=<matrix|<tformat|<table|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|>>|<row|<cell|>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i**<frac|k<rsub|x>a|2>>>|<cell|0>|<cell|>>|<row|<cell|>|<cell|>|<cell|0>|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|-i**<frac|k<rsub|x>a|2>>>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|0>|<cell|0>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|0>|<cell|0>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|0>|<cell|0>|<cell|>>>>>
    </equation*>

    <\equation*>
      A<rsup|\<dagger\>>=<matrix|<tformat|<table|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|0>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|0>|<cell|0>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|-i**<frac|k<rsub|x>a|2>><rsub|>>|<cell|0>|<cell|>|<cell|>|<cell|>>|<row|<cell|0>|<cell|0>|<cell|t<rsub|2>\<mathe\><rsup|i**<frac|k<rsub|x>a|2>><rsub|>>|<cell|0>|<cell|0>|<cell|0>>>>>
    </equation*>

    \;
  </hidden>|<\hidden>
    <\equation*>
      H<rsub|nanoribbon>=<matrix|<tformat|<table|<row|<cell|D>|<cell|A>|<cell|0>|<cell|\<ldots\>>|<cell|\<ldots\>>|<cell|0>>|<row|<cell|A>|<cell|D>|<cell|A>|<cell|0>|<cell|>|<cell|>>|<row|<cell|0>|<cell|A>|<cell|D>|<cell|A>|<cell|0>|<cell|>>|<row|<cell|\<vdots\>>|<cell|0>|<cell|A>|<cell|D>|<cell|A>|<cell|0>>|<row|<cell|\<vdots\>>|<cell|>|<cell|0>|<cell|A>|<cell|\<ddots\>>|<cell|>>|<row|<cell|0>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|D>>>>>
    </equation*>

    <\big-figure|<image|/home/these/localization-of-light-in-2d/Nanoribbont_2
    = 1.5N=6.pdf|0.5par|||> <image|/home/these/localization-of-light-in-2d/Nanoribbont_2
    = 0.5N=6.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/hist_tb.pdf|0.5par|||>>
      \;
    </big-figure>

    No edge states if the lattice is infinite ?\ 
  </hidden>|<\shown>
    Issue with:

    <\equation*>
      C=<big|int><rsub|BZ><around*|(|\<partial\><rsub|k<rsub|x>><math-bf|n><around*|(|k|)>|)><rsup|\<ast\>>\<wedge\>\<partial\><rsub|k<rsub|y>><math-bf|n><around*|(|k|)>\<mathd\>k<rsub|x>\<mathd\>k<rsub|y>
    </equation*>

    I can't find the matrix:

    <\equation*>
      <matrix|<tformat|<table|<row|<cell|H<rsub|+>>|<cell|0>>|<row|<cell|0>|<cell|H<rsub|->>>>>>
    </equation*>

    \;
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>
    </associate>
  </collection>
</auxiliary>