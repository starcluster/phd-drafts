<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\shown>
    <tit|Chapter 1>

    <\big-figure|<image|/home/pierre/Images/Screenshot_2024-03-11_10-39-10.png|1par|||>>
      \;
    </big-figure>
  </shown>|<\hidden>
    <tit|Chapter 2>

    None
  </hidden>|<\hidden>
    <tit|Chapter 3>

    <\big-figure>
      <image|/home/pierre/Images/Screenshot_2024-03-11_10-41-36.png|0.7par|||>

      <image|/home/pierre/Images/Screenshot_2024-03-11_10-40-59.png|0.7par|||>

      <image|/home/pierre/Images/Screenshot_2024-03-11_10-41-15.png|0.7par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Chapter 4>

    <\big-figure>
      <image|/home/pierre/chapter-4-thesis/figs/map_disorder_0_s.pdf|0.33par|||><image|/home/pierre/chapter-4-thesis/figs/map_disorder_ipr_0.pdf|0.33par|||>

      <image|/home/pierre/chapter-4-thesis/figs/map_disorder_gamma_0.pdf|0.33par|||><image|/home/pierre/chapter-4-thesis/figs/map_disorder_dos_0.pdf|0.33par|||>
    <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Chapter 5>

    None <math|\<rightarrow\>> en attente de Nice ?\ 
  </hidden>|<\hidden>
    <tit|Chapter 6>

    <\big-figure|<image|/home/pierre/chapter-6-thesis/figs/imap_84.6.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Chapter 6>

    <\big-figure|<image|/home/pierre/chapter-6-thesis/figs/spin_chern_distance=2.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <tit|Chapter 6>

    <\big-figure|<image|/home/pierre/chapter-6-thesis/figs/phase_diagram_spin_chern_deform_2.pdf|0.5par|||><image|/home/pierre/chapter-6-thesis/figs/gap_map_2.pdf|0.5par|||>>
      \;
    </big-figure>

    + Redaction (6 pages with figures)
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-4|<tuple|4|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-5|<tuple|5|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-6|<tuple|6|?|../../.TeXmacs/texts/scratch/no_name_1.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        \;
      </surround>|<pageref|auto-6>>
    </associate>
  </collection>
</auxiliary>