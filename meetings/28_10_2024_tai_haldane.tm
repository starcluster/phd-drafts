<TeXmacs|2.1>

<style|beamer>

<\body>
  <screens|<\hidden>
    <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/sbi_t2_pbc_paper_nice.pdf||||>>
      \;
    </big-figure>
  </hidden>|<\shown>
    <\big-figure|<image|/home/pierre/pybott/tests/pd_disorder.pdf|0.5par|||>
    <image|/home/pierre/pybott/tests/pd_tai.pdf|0.5par|||>>
      10 realizations. 400 sites.
    </big-figure>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|16:9>
    <associate|page-width|auto>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_3.tm>>
    <associate|auto-2|<tuple|2|?|../../.TeXmacs/texts/scratch/no_name_3.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>