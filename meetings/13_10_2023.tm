<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <\big-figure>
      <image|/home/these/chapter-2-thesis/figs/phase_diagram_bott.pdf|0.4par|||>
      <image|/home/these/chapter-2-thesis/figs/phase_diagram_chern.pdf|0.4par|||>

      <image|/home/these/chapter-2-thesis/figs/phase_diagram_spin_bott.pdf|0.4par|||>
      <image|/home/these/chapter-2-thesis/figs/phase_diagram_spin_chern.pdf|0.4par|||>
      <|big-figure>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <image|/home/these/localization-of-light-in-2d/dos_6cell_6.pdf|0.5par|||>
    <image|/home/these/localization-of-light-in-2d/dos_6cell_22.pdf|0.5par|||>

    <\big-figure|>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/dos_6cell_22_0.8.pdf|0.5par|||>
    <image|/home/these/localization-of-light-in-2d/dos_6cell_22_1.2.pdf|0.5par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/sbi_t2_pbc.pdf|0.82par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <section*|How to add disorder ?>

    <\equation*>
      H<rsub|i*j>=<mid|{><tabular*|<tformat|<table|<row|<cell|t<rsub|1>>|<cell|if
      \ s<rsub|i>,s<rsub|j>\<in\>cell\<wedge\>NN>>|<row|<cell|t<rsub|2>\<mathe\>xp<around*|(|<frac|a-d|a>|)>>|<cell|if
      s<rsub|i>,s<rsub|j> NN>>>>>
    </equation*>

    But does this has to change who is NN or not ?\ 

    <\big-figure|<image|/home/these/localization-of-light-in-2d/distri_t_2_22_0.8_pi12.pdf|0.48par|||>
    <image|/home/these/localization-of-light-in-2d/distri_t_2_22_1.2_pi12.pdf|0.48par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/grid_0.pdf|0.4par|||>
      <image|/home/these/localization-of-light-in-2d/grid_0.262.pdf|0.4par|||>\ 

      <image|/home/these/localization-of-light-in-2d/grid_0.524.pdf|0.4par|||><image|/home/these/localization-of-light-in-2d/grid_1.047.pdf|0.4par|||>
    <|big-figure>
      <math|\<phi\>=0,\<pi\>/12,\<pi\>/6,\<pi\>/3> How to select NN ?\ 
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/dos_6cell_22_0.8_0.2617993877991494.pdf|0.48par|||>
    <image|/home/these/localization-of-light-in-2d/dos_6cell_22_1.2_0.2617993877991494.pdf|0.48par|||>>
      Adding random rotation in <math|<around*|(|-\<pi\>/12;\<pi\>/12|)>>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/dos_6cell_22_0.8_0.5235987755982988.pdf|0.48par|||>
    <image|/home/these/localization-of-light-in-2d/dos_6cell_22_1.2_0.5235987755982988.pdf|0.48par|||>>
      Adding random rotation in <math|<around*|(|-\<pi\>/6;\<pi\>/6|)>>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure>
      <image|/home/these/localization-of-light-in-2d/sbi_t2_disorder_pbc_216.pdf|0.4par|||>
      <image|/home/these/localization-of-light-in-2d/sbi_t2_disorder_pbc_384.pdf|0.4par|||>

      <image|/home/these/localization-of-light-in-2d/sbi_t2_disorder_pbc_trivial.pdf|0.4par|||>
    <|big-figure>
      Evolution of Bott index averaged over 100 independant realisations for
      <math|t<rsub|2>=1.2> (top) and <math|t<rsub|2>=0.99> (bottom)
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/grid_1.047_half.pdf|0.45par|||>
    <image|/home/these/localization-of-light-in-2d/sbi_t2_disorder_pbc_half.pdf|0.45par|||>>
      We try to apply rotation on only one site over two.
      <math|t<rsub|2>=0.99>
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/grid_0_0.5.pdf||||>>
      Random change of size of cells.
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/sbi_t21.01_N96_disorder_size_pbc.pdf|0.45par|||><image|/home/these/localization-of-light-in-2d/sbi_t20.99_N216_disorder_size_pbc.pdf|0.45par|||>
    >
      Over 5 realisations, size randomly changing.
    </big-figure>
  </hidden>|<\hidden>
    <\big-figure|<image|/home/these/localization-of-light-in-2d/sbi_t20.99_N216_halfFalsedisorder_uniform_t2_size_pbc.pdf|0.46par|||>
    <image|/home/these/localization-of-light-in-2d/sbi_t20.99_N216_halfFalsedisorder_uniform_t1_size_pbc.pdf|0.46par|||>>
      Uniform perturbation on <math|t<rsub|1>> or <math|t<rsub|2>>.
    </big-figure>
  </hidden>|<\shown>
    <section*|Conclusion>

    <\itemize>
      <item>Except for random rotation, we have non zero spin bott index when
      adding disorder to the system

      <item><strong|But> so far it seems erratic and I cannot identify any
      regime that would lead to a TAI

      <item>I suggest: try to add disorder on the bonds but keeping the same
      bonds so that each sites still have 3 NN

      <item>Understand the distribution of <math|t<rsub|2>> when introducing
      disorder with rotation
    </itemize>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-10|<tuple|9|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-11|<tuple|10|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-12|<tuple|11|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-13|<tuple|12|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-14|<tuple|13|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-15|<tuple|13|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-2|<tuple|2|1|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-3|<tuple|3|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-4|<tuple|4|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-5|<tuple|4|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-6|<tuple|5|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-7|<tuple|6|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-8|<tuple|7|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-9|<tuple|8|?|../../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<phi\>=0,\<pi\>/12,\<pi\>/6,\<pi\>/3>>
        How to select NN ?\ 
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        Adding random rotation in <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|<around*|(|-\<pi\>/12;\<pi\>/12|)>>>
      </surround>|<pageref|auto-8>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|8>|>
        Adding random rotation in <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|<around*|(|-\<pi\>/6;\<pi\>/6|)>>>
      </surround>|<pageref|auto-9>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9>|>
        Evolution of Bott index averaged over 100 independant realisations
        for <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>=1.2>>
        (top) and <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>=0.99>>
        (bottom)
      </surround>|<pageref|auto-10>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10>|>
        We try to apply rotation on only one site over two.
        <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>=0.99>>
      </surround>|<pageref|auto-11>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11>|>
        Random change of size of cells.
      </surround>|<pageref|auto-12>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|12>|>
        Over 5 realisations, size randomly changing.
      </surround>|<pageref|auto-13>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13>|>
        Uniform perturbation on <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|1>>>
        or <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|t<rsub|2>>>.
      </surround>|<pageref|auto-14>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|How
      to add disorder ?> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>