import numpy as np
import matplotlib.pyplot as plt

def serie(a,n):
    i = np.linspace(1,n,n)
    return np.sum(np.cos(np.sqrt(a**2+i**2))/(i**2+a**2))



if __name__ == "__main__":
    N = 100
    x = np.linspace(1,100,1000)
    y = [serie(x0,100) for x0 in x]
    y1 = 1.618*np.cos(x)
    
    plt.plot(x,y)
    plt.plot(x,y1)
    plt.show()
