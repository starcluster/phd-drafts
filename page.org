#+TITLE: Oh my science !
#+AUTHOR: Pierre Wulles

#+OPTIONS: toc:nil num:nil title:nil
#+OPTIONS: html-style:nil
#+HTML_HEAD: <link rel="stylesheet" href="https://unpkg.com/tachyons@4.12.0/css/tachyons.min.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="index.css" />

# #+HTML_HEAD:  <div class="hero-image">  <div class="hero-text">    Oh my science !       </div></div>

# #+HTML: <a class="f6 fw6 dib ba mb3  b--black-20 bg-blue white ph3 ph4-ns pv2 pv3-ns br2 grow no-underline" href="./blog.html">  <span class='dib pr2'>Blog</span> <code class="f7 fw4 di"></code> </a>
# #+HTML: <a class="f6 fw6 dib ba mb3  b--black-20 bg-red white ph3 ph4-ns pv2 pv3-ns br2 grow no-underline" href="./physique.html">  <span class='dib pr2'>Physique</span> <code class="f7 fw4 di"></code> </a>
# #+HTML: <a class="f6 fw6 dib ba mb3  b--black-20 bg-green white ph3 ph4-ns pv2 pv3-ns br2 grow no-underline" href="./info.html">  <span class='dib pr2'>Info</span> <code class="f7 fw4 di"></code> </a>
# #+HTML: <a class="f6 fw6 dib ba mb3  b--black-20 bg-yellow white ph3 ph4-ns pv2 pv3-ns br2 grow no-underline" href="./cours.html">  <span class='dib pr2'>Cours</span> <code class="f7 fw4 di"></code> </a>
# #+HTML: <a class="f6 fw6 dib ba mb3  b--black-20 bg-pink white ph3 ph4-ns pv2 pv3-ns br2 grow no-underline" href="./liens.html">  <span class='dib pr2'>Liens</span> <code class="f7 fw4 di"></code> </a>
