#!/bin/bash

# Liste des répertoires Git à traiter
repositories=("chapter-1-thesis" "chapter-2-thesis" "chapter-3-thesis" "chapter-4-thesis" "chapter-5-thesis" "phd-drafts" "localization-of-light-in-2d" "chapter-6-thesis" "inf-201")

MESSAGE="$1"

if [ -z "$MESSAGE" ]; then
    echo "Veuillez fournir un message pour le commit."
    exit 1
fi

for repo in "${repositories[@]}"; do
    # Naviguer au niveau du répertoire Git
    cd
    cd "$repo" || continue

    # Vérifier s'il y a des modifications
    if git status -s | grep -q .; then
        echo "Changements détectés dans $repo"
        
        # Ajouter tous les fichiers modifiés
        git add .

        # Commit avec le message spécifié
        git commit --no-verify -m "$MESSAGE"

        # Pousser les modifications vers le dépôt distant
        git push origin main # Assurez-vous d'adapter la branche si nécessaire
    else
        echo "Pas de changements dans $repo"
    fi
done
