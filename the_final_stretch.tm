<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|The final stretch>>

  <\itemize>
    <item>Chapter 2 add a part on optics

    <item>Stiefel-whitney index

    <item>Paper with Nice

    <\itemize>
      <item>Nanoribbon on kekule model

      <item>Spin bott index on model with double hexagon cell

      <item>TAI ?
    </itemize>

    <item><strong|spin bott index on EMF model with deformation>

    <\itemize>
      <item>Get identical result to sergey

      <item>Compute green function in a box
    </itemize>

    <item>chapter 3

    <\itemize>
      <item>Rewrite the intro

      <item>Add more lines of computation

      <item>Write the computation for introducing the hamiltonian (see Luca)
      appendix
    </itemize>

    <item>chapter 4

    <\itemize>
      <item>Rewrite in rust the code for bott index

      <item>Write the chapter from the two papers
    </itemize>

    <item>chapter 5

    <\itemize>
      <item>Based on the paper with Nice

      <item>Includes experimental results
    </itemize>

    <item>Adapt bott index for Tight-binding situation modeling waves

    <\itemize>
      <item>Meet Frederic Faure

      <item>Try
    </itemize>

    <item>Short paper on how to compute Bott index

    <\itemize>
      <item>Present a generic code to compute Bott index and spin bott index
    </itemize>

    <item>MEEP

    <\itemize>
      <item>For the future
    </itemize>
  </itemize>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>