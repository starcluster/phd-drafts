#+TITLE: Compte rendu journalier de doctorat 2022
#+AUTHOR: Pierre Wulles
#+SETUPFILE: page.org
#+INCLUDE: begin.org
#+OPTIONS: num:nil
#+LATEX_HEAD: \usepackage{braket}
<2022-01-31 lun.>
- Enfin le diagramme de bandes est correct !
- Clarification des conditions de convergence pour sum G
- Correction IE1 inf201

<2022-01-28 ven.>
- suite des tentatives pour calculer le diagramme de bandes
- cours TP TD inf201
<2022-01-25 mar.>
- Tentative de calcul de G_ab mais ça ne fonctionne pas, peut etre
  erreur dans le numérateur k^2 -> q^2
- prépa cours OCAML
- nouvelle tentative pour le calcul de bandes, on a toujours le
  problème de non convergence pour a->0

<2022-01-24 lun.>
- divers config pour le laptop
- synchro zotero
- nouvelle tentative de calcul des bandes, le réseau utilisé est un
  réseau hexagonal tourné, ça ne fonctionne toujours pas, cela est
  probablement lié à un problème de sites A/B

<2022-01-21 ven.>
- Formule enfin démontrée avec l'aide de mathoverflow
- Semaine peu productive au niveau de la thèse, pas mal de temps passé
  sur la prépa de cours en OCAML, difficulté d'impleméntation du
  réseau réciproque, discussion avec Sergey nécessaire dès lundi
  8h. Du temps perdu à cause de bug pc, je prévois de rafraichir
  l'install.

<2022-01-17 lun.>
- Enfin le calcul de l'integrale semble aboutir, rédaction
- je retente le calcul des bandes avec cette fois le 1j*G_diag, reste
  des trucs à adapter pour que ça fonctionne

<2022-01-14 ven.>
- Réunion Sergey
- Tentative de calul de la méchante intégrale $exp(-t^2)/(t^2-z^2)$
  
<2022-01-13 jeu.>
- Réunion inf201
- Calcul dans l'espace de Fourier, intégrale de la mort, rédaction sur texmacs.
- simulation dans l'espace de fourier avec la somme

<2022-01-12 mer.>
- Afficher les cartes qui posent problèmes, il est est clair que les
  états qui posent problème sont ceux qui respectent une symétrie liée
  à l'hexagone.
- Réunion avec Sergey: refaire le calcul de l'espace de Fourier que
  l'on trouve dans quantum optics, relancer les calculs de diagramme
  de bande mais en utilisant la somme e la méthode des images.
<2022-01-11 mar.>
- passer un moment sur ce problème de valeurs dans G_hex_poo qui colle
  pas à ce que j'obtenais avant à cause de confusion dans le k0
  ... c'est réglé
- préparé cours inf201
<2022-01-10 lun.>
- passer tout l'aprem bloqué sur ce problème de grille qui colle pas,
  résolu en fin d'aprem seulement: la grills n'avait pas les bons
  bords !
<2022-01-07 ven.>
- un problème apparait avec le code de calcul des bandes pas possible
  pour une grille, seulement un hexagone ??
- retester les simulations faites jeudi mais pour delta_B = 0, ça ne
  suffit pas on ne voit pas bien le gap il semble apparaître tout le
  temps, il faudrait augemnter le nombre d'atomes je pense.
<2022-01-06 jeu.>
- Trouver pourquoi les valeurs propres ont des parties imag négatives:
  plusieurs test effectués, rien de très concluant, mini rapport de 3
  pages envoyés à Sergey
- suite des tests avec icc, sur meditation en comperaison avec
  GSL/Cblas on reste bien au dessus mais il faudrait tester avec
  parallélisation dans le cas blas pour être juste
<2022-01-05 mer.>
- Fin du rapport
- Investigations numériques et mathématiques sur les valeurs propres à
  parties imaginaires négatives
<2022-01-04 mar.>
- Refaire les simulations pour la synthèse
- Refaire les calculs pour aboutir au calcul dans l'espace de Fourier
<2022-01-03 lun.>
- Finalisation du rapport sur les 3 premiers mois
#+INCLUDE: begin.org
