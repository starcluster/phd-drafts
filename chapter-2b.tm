<TeXmacs|2.1>

<style|generic>

<\body>
  <doc-data|<doc-title|Lattice of sub-wavelength resonators>>

  We move away from tight-binding models to explore something a bit more
  intricate: a model involving point resonators coupled by electromagnetic
  interactions. The goal of this chapter is to explain the Hamiltonian used
  in the subsequent chapter. For a more complete derivation of this
  Hamiltonian, see the thesis of A. Goetschy.\ 

  <section|One atom in the vacuum>

  In this section, I consider the case of an atom in vacuum, modeled as a
  two-level system with ground state and excited state energies denoted as
  <math|E<rsub|g>> and <math|E<rsub|e>>, respectively. The wave functions
  associated with these states are denoted as
  <math|<around*|\||g|\<rangle\>>> and <math|<around*|\||e|\<rangle\>>>. The
  resonance frequency of this atom is <math|\<omega\><rsub|0>>, such that we
  have:

  <\equation>
    E<rsub|e>-E<rsub|g>=\<hbar\>\<omega\><rsub|0>
  </equation>

  In the following, we consider the atoms to be sufficiently small compared
  to the wavelength of the light wavelength to treat them as point
  scatterers. More precisely, we do the so-called electric dipole
  approximation in which we ignore spatial variation of the fields over the
  size of the atom. the elctron and the nucleus form a point electric dipole.
  With this assumption, we can write the Hamiltonian in the
  Power-Zienau-Wooley<\footnote>
    The PZW gauge is obtained from the Coulomb gauge by doing the unitary
    transformation <math|U=\<mathe\><rsup|i*q*<math-bf|d>\<cdot\><math-bf|A>/\<hbar\>c>>
  </footnote> gauge for this atom located in <math|<math-bf|r=0>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H>|<cell|=>|<cell|\<hbar\>\<omega\><rsub|0><big|sum><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>+\<hbar\><big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>a<rsub|<math-bf|k>,\<epsilon\>>-<big|sum><rsub|\<alpha\>><math-bf|D><rsub|\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|0>|)><eq-number>>>|<row|<cell|>|<cell|=>|<cell|H<rsub|atom>+H<rsub|trans>+H<rsub|int><eq-number><label|ham-raw>>>>>
  </eqnarray*>

  Where the sum over <math|\<alpha\>> runs over all excited states. Here
  because our atom is considered as a two-level atom of spin 1, the excited
  level is three times degenerate. This three states can be associated to the
  three directions of space: <math|x>,<math|y> and <math|z>.
  <math|\<sigma\><rsub|\<alpha\>g>> and <math|\<sigma\><rsub|g\<alpha\>>> are
  the spin-operators of the atom defined by\ 

  <\equation>
    \<sigma\><rsub|\<alpha\>g>=<around*|\||\<alpha\>|\<rangle\>><around*|\<langle\>|g|\|><infix-and>\<sigma\><rsub|g\<alpha\>>=<around*|\||g|\<rangle\>><around*|\<langle\>|\<alpha\>|\|>
  </equation>

  These operators are usually called raising and lowering operators. Note
  that <math|\<sigma\><rsub|\<alpha\>g><around*|\||\<alpha\>|\<rangle\>>=0>
  and <math|\<sigma\><rsub|g\<alpha\>><around*|\||g|\<rangle\>>=0> while
  <math|\<sigma\><rsub|\<alpha\>g><around*|\||g|\<rangle\>>=<around*|\||\<alpha\>|\<rangle\>>>
  and <math|\<sigma\><rsub|g\<alpha\>><around*|\||\<alpha\>|\<rangle\>>=<around*|\||g|\<rangle\>>>.\ 

  The second term is the Hamiltonian for the transverse electromagnetic
  field, <math|a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>> is the
  creation operator and <math|a<rsub|<math-bf|k>,\<epsilon\>>> is the
  annihilation operator. They both act in the Fock space. Finally, the last
  term describes the interaction of the atom with the electric field. In the
  dipole approximation, <math|<math-bf|D><rsub|\<alpha\>>> is the dipole
  operator given by:

  <\equation>
    <math-bf|D><rsub|\<alpha\>>=\<mu\><rsub|0><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>>+<math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>|)>
  </equation>

  With <math|\<mu\><rsub|0>> the dipolar moment of the atom and the direction
  of the dipole is given by:

  <\equation>
    <math-bf|d><rsub|\<alpha\>>=<frac|<around*|\<langle\>|g|\|><math-bf|l><around*|\||\<alpha\>|\<rangle\>>|<around*|\<\|\|\>|<around*|\<langle\>|g|\|><math-bf|l><around*|\||\<alpha\>|\<rangle\>>|\<\|\|\>>>
  </equation>

  where <math|<math-bf|l>> is internal distance between the electron and the
  nucleus. The electric field can be written by summing over the reciprocal
  space vectors and polarizations:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><around*|(|<math-bf|r>|)>>|<cell|=>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>><around*|(|<sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>>\<mathe\><rsup|i<math-bf|k>\<cdummy\><math-bf|r>>-\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>\<mathe\><rsup|-i<math-bf|k>\<cdummy\><math-bf|r>>|)>=<math-bf|E><rsub|+><around*|(|<math-bf|r>|)>+<math-bf|E><rsub|-><around*|(|<math-bf|r>|)><eq-number>>>|<row|<cell|>|<cell|<below|=|<around*|(|<math-bf|r>=<math-bf|0>|)>>>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>><around*|(|<sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>>-\<epsilon\>a<rsub|<math-bf|k>,\<epsilon\>><rsup|\<dagger\>>|)>=<math-bf|E><rsub|+><around*|(|<math-bf|0>|)>+<math-bf|E><rsub|-><around*|(|<math-bf|0>|)>>>>>
  </eqnarray*>

  Where we have separated the electric field into two independent
  contributions, this to facilitate the calculations performed later. Note
  that we have <math|<math-bf|E><rsub|+><around*|(|<math-bf|r>|)><rsup|\<dagger\>>=<math-bf|E><rsub|-><around*|(|<math-bf|r>|)>>.
  Our goal will now be to solve the equation of motions for our operator
  <math|a<rsub|<math-bf|k>,\<epsilon\>>>. The Heisenberg equation is given
  by:

  <\equation>
    <wide|a|\<dot\>><rsub|<math-bf|k>,\<epsilon\>>=<frac|i|\<hbar\>><around*|[|H,a<rsub|<math-bf|k>,\<epsilon\>>|]>=-i\<omega\><rsub|<math-bf|k>>a<rsub|<math-bf|k>,\<epsilon\>>-<frac|1|\<hbar\>><big|sum><rsub|\<alpha\>><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|\<alpha\>><around*|(|t|)>
  </equation>

  The solution of this differential equation is given by:

  <\equation>
    <label|a-sol>a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>\<mathe\><rsup|-i\<omega\><rsub|<math-bf|k>>t>-<big|int><rsub|0><rsup|t><big|sum><rsub|\<alpha\>><sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>>\<epsilon\>\<cdummy\><math-bf|D><rsub|\<alpha\>><around*|(|t<rprime|'>|)>\<mathe\><rsup|i\<omega\><rsub|<math-bf|k>><around*|(|t<rprime|'>-t|)>
    >\<mathd\>t<rprime|'>
  </equation>

  \;

  Where the first term is the solution of the homogeneous differential
  equation, representing the case without a source, whereas the second term
  represents the solution in the presence of a source. In our present
  situation the source is the two-level atom. We will now apply another
  approximation, the Markovian approximation. For that we assume the
  atom-field coupling is weak enough so that we can consider the evolution of
  the ordering operators to be of the form:

  <\equation*>
    \<sigma\><rsub|\<alpha\>g><around*|(|t<rprime|'>|)>=\<sigma\><rsub|\<alpha\>g><around*|(|t|)>\<mathe\><rsup|-i\<omega\><rsub|0><around*|(|t<rprime|'>-t|)>>
  </equation*>

  <\equation*>
    \<sigma\><rsub|g\<alpha\>><around*|(|t<rprime|'>|)>=\<sigma\><rsub|g\<alpha\>><around*|(|t|)>\<mathe\><rsup|-i\<omega\><rsub|0><around*|(|t-t<rprime|'>|)>>
  </equation*>

  The intuition behind this approximation is that the field operator has no
  \Pmemory\Q of the operator at a previous time. We can now use this ansatz
  in the second term of the solution <reference|a-sol>:

  <\equation>
    a<rsub|<math-bf|k>,\<epsilon\>><around*|(|t|)>=a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>\<mathe\><rsup|-i\<omega\><rsub|<math-bf|k>>t>-\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|0><around*|(|<frac|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>>+h.c.|)>
  </equation>

  \;

  We inject this solution into the electric field and we get:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|i<big|sum><rsub|<math-bf|k>><big|sum><rsub|\<epsilon\>\<perp\><math-bf|k>>\<mathe\><rsup|i<math-bf|k>\<cdummy\><math-bf|r>><sqrt|<frac|\<hbar\>\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V>>\<epsilon\><around*|(|a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>\<mathe\><rsup|-i\<omega\><rsub|<math-bf|k>>t>|\<nobracket\>>>>|<row|<cell|>|<cell|->|<cell|\<epsilon\>*<sqrt|<frac|\<omega\><rsub|<math-bf|k>>|2\<hbar\>\<varepsilon\><rsub|0>V>><big|sum><rsub|\<alpha\>>\<mu\><rsub|0><around*|(|<frac|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|1-\<mathe\><rsup|-i\<omega\><rsub|0>t>|)>|i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>>+h.c.|)><around*|\<nobracket\>||)><eq-number>>>>>
  </eqnarray*>

  We can now compute the sum on the polarization using
  <math|<big|sum>\<epsilon\>\<otimes\>\<epsilon\>=<with|font|Bbb|1>-<math-bf|k>\<otimes\><math-bf|k>/k<rsup|2>>,
  we end up with:

  <\equation>
    <math-bf|E><rsub|+><around*|(|<math-bf|0>,t|)>=<math-bf|E><rsup|0><rsub|+>-\<mu\><rsub|0><big|sum><rsub|\<alpha\>><big|sum><rsub|<math-bf|k>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>V><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>><frac|\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|k-k<rsub|0>>+h.c.|)>
  </equation>

  Before going any further we will go in the mode continuum limit, doing the
  transformation:

  <\equation>
    <big|sum><rsub|<math-bf|k>>\<rightarrow\><frac|V|<around*|(|2\<pi\>|)><rsup|3>><big|int>\<mathd\><rsup|3><math-bf|k>
  </equation>

  And we will also stop considering the term
  <math|<math-bf|E><rsup|0><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>,
  indeed we will later average on the vaccum and this will make disappear
  this term as we have <math|a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)><around*|\||vac|\<rangle\>>=0>.

  <\equation>
    <math-bf|E><rsub|+><around*|(|<math-bf|0>,t|)>=<frac|-\<mu\><rsub|0>|<around*|(|2\<pi\>|)><rsup|3>><big|int><big|sum><rsub|\<alpha\>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>><frac|\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|k-k<rsub|0>>+h.c.|)>\<mathd\><rsup|3><math-bf|k>
  </equation>

  Then we use spherical coordinate to rewrite our integral:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|<frac|-\<mu\><rsub|0>|<around*|(|2\<pi\>|)><rsup|3>><big|int><rsub|0><rsup|\<infty\>><big|int><big|sum><rsub|\<alpha\>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>><around*|(|<with|font|Bbb|1>-<frac|<math-bf|k>\<otimes\><math-bf|k>|k<rsup|2>>|)><around*|(|<math-bf|d><rsub|\<alpha\>><frac|\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|k-k<rsub|0>>+h.c.|)>\<mathd\>\<Omega\>k<rsup|2>\<mathd\>k>>>>
  </eqnarray*>

  The integral on <math|\<mathd\>\<Omega\>> gives a factor
  <math|4\<pi\>\<times\>2/3> and we subtitute in the integral using the
  relation <math|k=\<omega\>/c> :

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|<frac|-\<mu\><rsub|0>|<around*|(|2\<pi\>|)><rsup|3>><frac|2\<times\>4\<pi\>|3><big|int><rsub|0><rsup|\<infty\>><big|sum><rsub|\<alpha\>><frac|\<omega\><rsub|<math-bf|k>>|2\<varepsilon\><rsub|0>><around*|(|<math-bf|d><rsub|\<alpha\>><frac|\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|k-k<rsub|0>>+h.c.|)>k<rsup|2>\<mathd\>k>>|<row|<cell|>|<cell|=>|<cell|<frac|-\<mu\><rsub|0>|\<pi\><rsup|2>><frac|1|3><big|int><rsub|0><rsup|\<infty\>><big|sum><rsub|\<alpha\>><frac|\<omega\>c|2\<varepsilon\><rsub|0>><around*|(|<math-bf|d><rsub|\<alpha\>><frac|\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|\<omega\>-\<omega\><rsub|0>>+h.c.|)><frac|\<omega\><rsup|2>\<mathd\>\<omega\>|c<rsup|3>>>>>>
  </eqnarray*>

  We are now ready to compute the integral over <math|\<omega\>>, the
  attentive reader can now notice that the integrand is divergent for
  <math|\<omega\>=\<omega\><rsub|0>>. I will focus for a few lines only on
  the integral to explain how I treat this divergence, I rewrite the
  integrand seprating real and imaginary part:

  <\equation*>
    <frac|<around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|\<omega\>-\<omega\><rsub|0>>=<frac|<around*|(|1-cos<around*|(||\<nobracket\>><around*|(|\<omega\>-\<omega\><rsub|0>|)>t|)><around*|\<nobracket\>||)>|\<omega\>-\<omega\><rsub|0>>-i<frac|sin<around*|(|<around*|(|\<omega\>-\<omega\><rsub|0>|)>t|)>|\<omega\>-\<omega\><rsub|0>>
  </equation*>

  he treatment of this integral is a standard result in QED, see [reference],
  where we introduce P as the Cauchy principal part and the delta function:

  <\equation*>
    <big|int><rsub|0><rsup|\<infty\>>\<omega\><rsup|3><frac|<around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|\<omega\>-\<omega\><rsub|0>>\<mathd\>\<omega\>=\<cal-P\><around*|(|<frac|\<omega\><rsup|3>|\<omega\>-\<omega\><rsub|0>>|)>+i\<pi\>\<omega\><rsub|0><rsup|3>
  </equation*>

  The Cauchy principal part is the divergent term corresponding to the
  lambshift. It is considered in the following to be part of
  <math|\<omega\><rsub|0>>, the frequency resonance of our atom. It is
  finally possible to finish this cumbersome computation:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|E><rsub|+><around*|(|<math-bf|r>=<math-bf|0>,t|)>>|<cell|=>|<cell|<frac|-\<mu\><rsub|0>|\<pi\><rsup|2>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><big|int><rsub|0><rsup|\<infty\>><frac|\<omega\><rsup|3>|2\<varepsilon\><rsub|0>><frac|<around*|(|1-\<mathe\><rsup|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>t>|)>|\<omega\>-\<omega\><rsub|0>>\<mathd\>\<omega\>+h.c.>>|<row|<cell|>|<cell|=>|<cell|<frac|-\<mu\><rsub|0>|\<pi\><rsup|2>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|i<frac|\<pi\>\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>>+\<cal-P\><around*|(|<frac|\<omega\><rsup|3>|\<omega\>-\<omega\><rsub|0>>|)>|)>\<mathd\>\<omega\>+h.c>>|<row|<cell|>|<cell|=>|<cell|<frac|-\<mu\><rsub|0>|\<pi\>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)><around*|(|i<frac|\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>>|)>+h.c>>|<row|<cell|>|<cell|=>|<cell|<frac|-i\<mu\><rsub|0>|\<pi\>><frac|\<omega\><rsub|0><rsup|3>c<rsup|2>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><around*|(|t|)>+h.c>>|<row|<cell|>|<cell|=>|<cell|<frac|-i|\<pi\>><frac|\<omega\><rsub|0><rsup|3>c<rsup|2>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>><math-bf|D><rsub|\<alpha\>><rsup|j>>>>>
  </eqnarray*>

  We will now reintroduce this expression of <math|<math-bf|E><rsub|>> into
  the interaction Hamiltonian. It is at this point that we can average over
  the vacuum, which removes the term involving
  <math|a<rsub|<math-bf|k>,\<epsilon\>><around*|(|0|)>> as I mentioned
  earlier in the text. This average also makes the term <math|H<rsub|trans>>
  (see <reference|ham-raw>) vanishes.

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|int>>|<cell|=>|<cell|<big|sum><rsub|\<alpha\>><math-bf|D><rsub|\<alpha\>>\<cdummy\><math-bf|E><around*|(|<math-bf|0>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|i|\<pi\>><frac|\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>,\<beta\>><math-bf|D><rsub|\<alpha\>>\<cdot\><math-bf|D><rsub|\<beta\>>>>|<row|<cell|>|<cell|=>|<cell|<frac|i\<mu\><rsub|0><rsup|2>|\<pi\>><frac|\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>,\<beta\>><around*|(|<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>>+<math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>|)><around*|(|<math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>+<math-bf|d><rsup|\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|i\<mu\><rsub|0><rsup|2>|\<pi\>><frac|\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>,\<beta\>><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>+<math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>+<math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><math-bf|d><rsup|\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g>+<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><math-bf|d><rsup|\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g><label|before-rwa><eq-number>>>>>
  </eqnarray*>

  I will now apply a last, but not least, approximation to finish the
  computation, the rotating wave approximation (RWA). Because we have assumed
  <math|\<sigma\><rsub|\<alpha\>g><around*|(|t<rprime|'>|)>=\<sigma\><rsub|\<alpha\>g><around*|(|t|)>\<mathe\><rsup|-i\<omega\><rsub|0><around*|(|t<rprime|'>-t|)>>>,
  in the beginning of this text. If we do the product given in
  <reference|before-rwa>, then the terms <math|\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|g\<beta\>>>
  and <math|\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|\<beta\>g>> will end up
  with frequency of <math|2\<omega\><rsub|0>> and <math|-2\<omega\><rsub|0>>,
  consequently they will oscillate too fast and can be ignored. That's the
  RWA:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|int>>|<cell|=>|<cell|<frac|i\<mu\><rsub|0><rsup|2>|\<pi\><rsup|>><frac|\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>,\<beta\>><math-bf|d><rsup|\<ast\>><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g><math-bf|d><rsub|\<beta\>>\<sigma\><rsub|g\<beta\>>+<math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|g\<alpha\>><math-bf|d><rsup|\<ast\>><rsub|\<beta\>>\<sigma\><rsub|\<beta\>g>>>>>
  </eqnarray*>

  And now I consider the dipolar moment to be purely real to simplify, a more
  precise treatment could be given for others problems:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|int>>|<cell|=>|<cell|<frac|i\<mu\><rsub|0><rsup|2>|\<pi\><rsup|>><frac|\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>,\<beta\>><math-bf|d><rsub|\<alpha\>>\<cdummy\><math-bf|d><rsub|\<beta\>><around*|(|\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<beta\>>+\<sigma\><rsub|g\<alpha\>>\<sigma\><rsub|\<beta\>g>|)>>>|<row|<cell|>|<cell|=>|<cell|2<frac|i\<mu\><rsub|0><rsup|2>|\<pi\><rsup|>><frac|\<omega\><rsub|0><rsup|3>|2\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<cdummy\><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>>>|<row|<cell|>|<cell|=>|<cell|<frac|i\<mu\><rsub|0><rsup|2>|\<pi\><rsup|>><frac|\<omega\><rsub|0><rsup|3>|\<varepsilon\><rsub|0>><frac|1|3><frac|1|c<rsup|2>><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<cdummy\><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>>>>>
  </eqnarray*>

  and since the linewidth is defined by:

  <\equation*>
    \<Gamma\><rsub|0>=<frac|\<mu\><rsub|0><rsup|2>k<rsub|0><rsup|3>|3\<pi\>\<hbar\>\<varepsilon\><rsub|0>>=<frac|\<mu\><rsub|0><rsup|2>|3\<pi\>\<hbar\>\<varepsilon\><rsub|0>><around*|(|<frac|\<omega\><rsub|0>|c>|)><rsup|3>
  </equation*>

  we get for the Hamiltonian of interaction:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|int>>|<cell|=>|<cell|i\<Gamma\><rsub|0>c\<hbar\><big|sum><rsub|\<alpha\>><math-bf|d><rsub|\<alpha\>>\<cdummy\><math-bf|d><rsub|\<alpha\>>\<sigma\><rsub|\<alpha\>g>\<sigma\><rsub|g\<alpha\>>>>|<row|<cell|>|<cell|=>|<cell|i\<Gamma\><rsub|0>c\<hbar\><with|font|Bbb|1><rsub|3>>>>>
  </eqnarray*>

  Our treatment finally gives the following effective Hamiltonian for one
  atom in the vacuum, of frequency resonance <math|\<omega\><rsub|0>> and
  decay time <math|1/\<Gamma\><rsub|0>>:<with|color|red| (probleme avec
  <math|c> ?)>

  <\equation*>
    H<rsub|eff>=\<hbar\><matrix|<tformat|<table|<row|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>|<cell|>|<cell|>>|<row|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>|<cell|>>|<row|<cell|>|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>>>>>
  </equation*>

  One last thing to consider is to add a magnetic field along the <math|z>
  direction, this will lift the degenracy of the excited state by shifting
  the energy of <math|\<mu\><rsub|B>B> where <math|\<mu\><rsub|B>> is the
  Bohr magneton and <math|B> the intensity of the magnetic field:

  <\equation*>
    H<rsub|eff>=\<hbar\><matrix|<tformat|<table|<row|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>-\<mu\><rsub|B>B>|<cell|>|<cell|>>|<row|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>+\<mu\><rsub|B>B>|<cell|>>|<row|<cell|>|<cell|>|<cell|\<omega\><rsub|0>-<frac|i\<Gamma\><rsub|0>|2>>>>>>
  </equation*>

  <section|Hamiltonian of atoms coupled by the EM field>

  We thus aim to write the Hamiltonian describing the evolution of a set of
  atoms interacting with each other via the electromagnetic field. These
  atoms are considered smaller, by at least an order of magnitude, than the
  distance between them and the wavelength of the field propagating through
  them. We label these atoms from <math|1> to <math|N> and note their
  positions <math|<math-bf|r><rsub|i>> and we consider a simple model made of
  two level, <math|g> the ground state of spin 0, and <math|e> the excited
  state of spin 1. We consider all the atoms to be indentical and of mass
  <math|m> and frequency resonance <math|\<omega\><rsub|0>>. A first
  expression of this Hamiltonian is given by [Antezza Castin]
  <math|H=H<rsub|0>+V+H<rsub|R>> where we split between a non-interacting
  term, the atom-field dipolar coupling and the energy of the transverse
  field. We will first focus on the non-interacting term, we have, in the
  Coulomb gauge:

  <\equation>
    H<rsub|0>=<big|sum><rsub|i=1><rsup|N><around*|(|<frac|<math-bf|p><rsub|i><rsup|2>|2m>+U<rsub|i><around*|(|<math-bf|r><rsub|i>|)>+<big|sum><rsub|\<alpha\>>\<hbar\>\<omega\><rsub|0><around*|\||i,e<rsub|\<alpha\>>|\<rangle\>><around*|\<langle\>|i,e<rsub|\<alpha\>>|\|>|)>
  </equation>

  \;

  represents the energy of the transverse electromagnetci field.
  <math|U<rsub|i><around*|(|<math-bf|r><rsub|i>|)>=m<rsub|i>\<omega\><rsup|2><rsub|ho><around*|(|<math-bf|r><rsub|i>-<math-bf|R><rsub|<rsub|i>>|)><rsup|2>/2>
  is a potential to indicate that the atoms are trapped in small region of
  space with <math|\<omega\><rsup|><rsub|ho>> the atomic frequency
  oscillation. We will now consider that the quantum fluctuations of atom
  positions are negligible, so we can refer to our atoms as ``fixed
  scatterers\Q. This hypothesis allows us to drop terms
  <math|U<rsub|i><around*|(|<math-bf|r><rsub|i>|)>> and
  <math|<math-bf|p><rsub|i><rsup|2>> the momentum operator The sum over
  <math|\<alpha\>> takes into account the three-fold degeneracy of <math|e>,
  the excited state, because of the three directions of space <math|x>,
  <math|y> and <math|z>.

  We will now make the approximation of the second order to account for the
  interaction of the atom with the vacuum: we add a term
  <math|-i\<Gamma\><rsub|0>/2> to take into account the decay time of the
  atom. We also consider that the atoms are placed in a magnetic field, this
  induces a Zeeman shift and lifts the three-fold degenracy. Finally the non
  interacting Hamiltonian is given by:

  <\equation>
    H<rsub|0>=\<hbar\><big|sum><rsub|i=1><rsup|N><big|sum><rsub|\<alpha\>=\<sigma\><rsub|+>,\<sigma\><rsub|->><rsup|><around*|(|\<omega\><rsub|0>+sgn<around*|(|\<alpha\><rsub|i>|)>\<mu\>B-i<frac|\<Gamma\><rsub|0>|2>|)>\<divides\>\<alpha\><rsub|i><around*|\<nobracket\>||\<rangle\>><around*|\<langle\>|\<alpha\><rsub|i>|\|>
  </equation>

  Regarding the energy of the transverse field, it is given by:

  <\equation>
    H<rsub|R>=\<varepsilon\><rsub|0><big|int><around*|(|<math-bf|E><rsub|\<perp\>><around*|(|<math-bf|r>|)>+c<rsup|2><math-bf|B><rsup|2><around*|(|<math-bf|r>|)>|)>\<mathd\><rsup|3><math-bf|r>=<big|sum><rsub|<math-bf|k>,\<lambda\>>\<hbar\>\<omega\><rsub|<math-bf|k>,\<lambda\>><around*|(|a<rsub|<math-bf|k>,\<lambda\>><rsup|\<dagger\>>a<rsub|<math-bf|k>,\<lambda\>>+<frac|1|2>|)>
  </equation>

  And the interaction term is given by the following expression:

  <\equation>
    V=<big|sum><rsub|i=1><rsup|N>\<epsilon\><rsub|\<alpha\>><rsup|Coul>+<frac|1|8\<pi\>\<varepsilon\><rsub|0>><big|sum><rsub|j\<neq\>i><rsup|N><frac|q<rsub|i>q<rsub|j>|<around*|\||<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|\|>>
  </equation>

  It should be noted that the <math|\<epsilon\><rsub|\<alpha\>><rsup|Coul>>
  diverge, but we can introduce an arbitrary cutoff frequency to give them
  finite values. All summations over <math|<math-bf|k><rsub|i>> will
  therefore be limited to <math|k<rsub|i>\<less\>k<rsub|c>>. By doing so, we
  explicitly choose to ignore the coupling terms with the relativistic modes.

  \;

  We know choose to change gauge and do the Power-Zienau-Wolley unitary
  tranform:

  <\equation>
    T=exp<around*|(|-<frac|i|\<hbar\>><big|sum><rsub|i=1><rsup|N><math-bf|D><rsub|i>\<cdot\><math-bf|A><around*|(|r<rsub|i>|)>|)>
  </equation>

  This tranform allows us to rewrite the interaction term as:

  <\equation>
    V=-<big|sum><rsub|i=1><rsup|N><math-bf|D><rsub|i>\<cdot\><math-bf|E><rsub|\<perp\>><around*|(|<math-bf|r><rsub|i>|)>+<frac|1|3\<varepsilon\><rsub|0>><big|sum><rsub|i\<neq\>j><rsup|><math-bf|D><rsub|i>\<cdot\><math-bf|D><rsub|j>\<delta\><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)><label|V-PZW>
  </equation>

  Where:

  <\equation>
    <math-bf|D><rsub|i>=<big|sum><rsub|j=1><rsup|N>q<rsub|j><around*|(|<math-bf|r><rsub|j>-<math-bf|r><rsub|i>|)>
  </equation>

  represents the operator of the atomic dipole moment. The sum runs over all
  the position of the atoms. Note that in the present situations, atoms are
  immobile and will not overlap, we can then drop the second term in
  <reference|V-PZW>. We will now rewrite the first term in function of
  <math|<math-bf|E>>, the total electric field instead of
  <math|<math-bf|E><rsub|\<perp\>>>, the transerve electric field, for this,
  we inject the expression:

  <\equation>
    <math-bf|E><around*|(|<math-bf|r>|)>=<math-bf|E><rsub|\<perp\>><around*|(|<math-bf|r>|)>-<frac|1|\<varepsilon\><rsub|0>><math-bf|P><around*|(|<math-bf|r>|)>
  </equation>

  where <math|<math-bf|P>> is the polarization operator given by:

  <\equation>
    <math-bf|P><around*|(|<math-bf|r>|)>=<big|sum><rsub|i=1><rsup|N><math-bf|D><rsub|i>\<delta\><around*|(|<math-bf|r>-<math-bf|r><rsub|i>|)>
  </equation>

  \ in <reference|V-PZW>:

  <\equation>
    V=-<big|sum><rsub|i=1><rsup|N><math-bf|D><rsub|i>\<cdot\><around*|(|<math-bf|E><rsub|><around*|(|<math-bf|r>|)>+<frac|1|\<varepsilon\><rsub|0>><math-bf|P><around*|(|<math-bf|r>|)>|)>=-<big|sum><rsub|i=1><rsup|N><math-bf|D><rsub|i>\<cdot\><math-bf|E><rsub|><around*|(|<math-bf|r>|)>+<frac|1|\<varepsilon\><rsub|0>><big|sum><rsub|i=1><rsup|N><big|sum><rsub|j=1><rsup|N><math-bf|D><rsub|i>\<cdot\><math-bf|D><rsub|j>\<delta\><around*|(|<math-bf|r>-<math-bf|r><rsub|j>|)>
  </equation>

  The last term of the equation is independant of the energy and thus does
  not play a role when describing the interaction between the field and the
  atoms. We will ignore it later. The core of the interaction is now standing
  in the term <math|<big|sum><rsub|i=1><rsup|N><math-bf|D><rsub|i>\<cdot\><math-bf|E><rsub|><around*|(|<math-bf|r>|)>>.
  We can intuitively see the total field as a sum between the free field (the
  field in the abscence of dipoles) and the field created by the dipole
  themselves, in Fourier space, this is written as:

  <\equation>
    <math-bf|E><around*|(|r,\<omega\>|)>=<math-bf|E><rsub|0><around*|(|r,\<omega\>|)>+<frac|1|\<varepsilon\><rsub|0>><big|sum><rsub|i=1><rsup|N>\<cal-G\><around*|(|<math-bf|r>-<math-bf|r><rsub|i>|)><math-bf|D><rsub|i><around*|(|\<omega\>|)>
  </equation>

  \;

  whith <math|\<cal-G\>> the propagator of the transverse EM field. Its
  expression is derivated in The following section. We can finally ignore the
  term <math|H<rsub|R>> as we do not treat free photons and we obtain our
  effective Hamiltonian:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H>|<cell|=>|<cell|\<hbar\><big|sum><rsub|i=1><rsup|N><big|sum><rsub|\<alpha\>=\<sigma\><rsub|+>,\<sigma\><rsub|->><rsup|><around*|(|\<omega\><rsub|0>+sgn<around*|(|\<alpha\><rsub|i>|)>\<mu\>B-i<frac|\<Gamma\><rsub|0>|2>|)>\<divides\>\<alpha\><rsub|i><around*|\<nobracket\>||\<rangle\>><around*|\<langle\>|\<alpha\><rsub|i>|\|><eq-number>>>|<row|<cell|>|<cell|+>|<cell|<frac|3\<pi\>\<hbar\>\<Gamma\><rsub|0>c|\<omega\><rsub|0>><big|sum><rsub|i\<neq\>j><big|sum><rsub|\<alpha\>,\<beta\>=\<sigma\><rsub|+>,\<sigma\><rsub|->>\<cal-G\><rsub|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|i>-<math-bf|r><rsub|j>|)><around*|\<nobracket\>|\<divides\>\<alpha\><rsub|i>|\<rangle\>><around*|\<langle\>|\<beta\><rsub|j>|\|>>>>>
  </eqnarray*>

  <section|Dyadic Green's function of maxwell equations>

  <subsection|Maxwell equations>

  Mettre les d�tails calculs en annexe

  The equations describing the evolution of an electromagnetic field in time
  and space are derived from the works of James C. Maxwell [reference]. These
  equations establish a relationship between the electric field
  <math|<math-bf|E><around*|(|<math-bf|r>,t|)>> [V/m] and the magnetic field
  <math|<math-bf|B><around*|(|<math-bf|r>,t|)>> [W/m<math|<rsup|2>>]. They
  are formulated in a modern form using the divergence and curl operators:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<nabla\>\<cdummy\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|<frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>><eq-number><label|max-gauss>>>|<row|<cell|\<nabla\>\<cdummy\><math-bf|B><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|0<eq-number><label|max-thomson>>>|<row|<cell|\<nabla\>\<times\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\><math-bf|B><around*|(|<math-bf|r>,t|)>|\<partial\>t><eq-number><label|max-faraday>>>|<row|<cell|\<nabla\>\<times\><math-bf|B><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)><eq-number><label|max-ampere>>>>>
  </eqnarray*>

  Where <math|\<rho\><around*|(|<math-bf|r>,t|)>> [C/m<math|<rsup|3>>] is the
  total charge density and <math|<math-bf|J><around*|(|<math-bf|r>,t|)>> is
  the current density. These last two are linked by the equation:

  <\equation>
    <frac|\<partial\>\<rho\><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<nabla\>\<cdummy\><math-bf|J><around*|(|<math-bf|r>,t|)>=0<label|continuite>
  </equation>

  and <math|\<varepsilon\><rsub|0>=8.854\<times\>10<rsup|-12>> [F/m] is the
  free-space electric permittivity while <math|\<mu\><rsub|0>=4\<pi\>\<times\>10<rsup|-7>>
  is the free-space magnetic permeability. These two constants are related by
  the equation:

  <\equation>
    \<varepsilon\><rsub|0>\<mu\><rsub|0>=<frac|1|c<rsup|2>>
  </equation>

  where <math|c=3\<times\>10<rsup|-9>> [m/s] is the speed of light in vacuum.\ 

  <subsection|Wave equation in Fourier space>

  If we take the curl of <reference|max-faraday> we get:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<nabla\>\<times\>\<nabla\>\<times\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\>\<nabla\>\<times\><math-bf|B><around*|(|<math-bf|r>,t|)>|\<partial\>t><eq-number>>>|<row|<cell|<below|\<Rightarrow\>|using
    <reference|max-ampere>>\<nabla\><around*|(|\<nabla\>\<cdummy\><math-bf|E><around*|(|<math-bf|r>,t|)>|)>-\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-<frac|\<partial\>|\<partial\>t><around*|(|\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t>+\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>|)><eq-number>>>|<row|<cell|<below|\<Rightarrow\>|using
    <reference|max-gauss>>\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>>|)>-\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|-\<varepsilon\><rsub|0>\<mu\><rsub|0><frac|\<partial\><rsup|2><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t<rsup|2>>-\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)><eq-number>>>|<row|<cell|\<Rightarrow\>\<Delta\><math-bf|E><around*|(|<math-bf|r>,t|)>-<frac|1|c<rsup|2>><frac|\<partial\><rsup|2><math-bf|E><around*|(|<math-bf|r>,t|)>|\<partial\>t<rsup|2>>>|<cell|=>|<cell|\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>+\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|\<rho\><around*|(|<math-bf|r>,t|)>|\<varepsilon\><rsub|0>>|)><eq-number>>>>>
  </eqnarray*>

  We now introduce the Fourier transform of the involved fields:

  <\equation>
    <math-bf|E><around*|(|<math-bf|r>,t|)>=<frac|1|2\<pi\>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><math-bf|E><around*|(|<math-bf|r>,\<omega\>|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>\<omega\>
  </equation>

  <\equation>
    <math-bf|B><around*|(|<math-bf|r>,t|)>=<frac|1|2\<pi\>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><math-bf|B><around*|(|<math-bf|r>,\<omega\>|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>\<omega\>
  </equation>

  We obtain:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<below|\<Rightarrow\>|using
    <reference|continuite>><around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<mu\><rsub|0><math-bf|J><around*|(|<math-bf|r>,t|)>-\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|<math-bf|J><around*|(|<math-bf|r>,t|)>|i\<omega\>\<varepsilon\><rsub|0>>|)><eq-number>>>|<row|<cell|\<Rightarrow\><around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|E><around*|(|<math-bf|r>,t|)>>|<cell|=>|<cell|\<mu\><rsub|0>i\<omega\><around*|(|<math-bf|J><around*|(|<math-bf|r>,t|)>+\<nabla\><around*|(|\<nabla\>\<cdummy\><frac|<math-bf|J><around*|(|<math-bf|r>,t|)>|*k<rsub|0><rsup|2>>|)>|)><eq-number>>>>>
  </eqnarray*>

  Where we introduced <math|k<rsub|0>=\<omega\>/c>.

  <subsection|Green propagator>

  The solution of\ 

  <\equation>
    <around*|(|\<Delta\>+k<rsub|0><rsup|2>|)><math-bf|G><around*|(|<math-bf|r>,<math-bf|r><rprime|'>|)>=-\<delta\><around*|(|<math-bf|r>-<math-bf|r><rsup|<rprime|'>>|)>
  </equation>

  is given for spherical homogeneous and isotropic medium by\ 

  <\equation>
    G<rsub|0><rsup|\<pm\>><around*|(|r|)>=<frac|\<mathe\><rsup|\<pm\>i*k<rsub|0>r>|4\<pi\>r>
  </equation>

  If we apply on this last equation the operator
  <math|<with|font|Bbb|1>+<frac|\<nabla\>\<nabla\>|k<rsub|0><rsup|2>>> we
  get:

  \;

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|\<nabla\>\<nabla\>|k<rsub|0><rsup|2>>G<rsub|0><rsup|\<pm\>><around*|(|r|)>>|<cell|=>|<cell|<around*|(|-1-<frac|2i|k<rsub|0>r>+<frac|2|k<rsub|0><rsup|2>r<rsup|2>>|)>G<rsub|0><rsup|\<pm\>><around*|(|r|)><eq-number>>>>>
  </eqnarray*>

  After a bit of algebra, we can introduce the functions:

  <\equation>
    P<around*|(|x|)>=1-<frac|1|x>+<frac|1|x<rsup|2>>
    <infix-and>Q<around*|(|x|)>=-1+<frac|3|x>-<frac|3|x<rsup|2>>
  </equation>

  and write the dyadic Green's function describing the coupling of atoms by
  electromagnetic waves as:

  <\equation>
    \<cal-G\><around*|(|<math-bf|r><rsub|1>,<math-bf|r><rsub|2>|)>=G<rsub|0><rsup|><around*|(|r|)><around*|(|P<around*|(|k<rsub|0>r|)><with|font|Bbb|1><rsub|3>+<frac|<math-bf|r>\<otimes\><math-bf|r>|r<rsup|2>>Q<around*|(|k<rsub|0>r|)>|)>
  </equation>

  Where we have set <math|<math-bf|r>=<math-bf|r><rsub|1>-<math-bf|r><rsub|2>>
  and <math|r=<around*|\||<math-bf|r>|\|>>. Note that this expression
  diverges in if <math|r<rsub|1>> becomes close of <math|r<rsub|2>>. To
  circumvent this problem, we introduce a small spherical volume of
  exclusion:

  <\equation>
    \<cal-G\><around*|(|<math-bf|r><rsub|1>,<math-bf|r><rsub|2>|)>=<frac|\<delta\><around*|(|<math-bf|r>|)>|3k<rsub|0><rsup|2>>-G<rsub|0><rsup|><around*|(|r|)><around*|(|P<around*|(|k<rsub|0>r|)><with|font|Bbb|1><rsub|3>+<frac|<math-bf|r>\<otimes\><math-bf|r>|r<rsup|2>>Q<around*|(|k<rsub|0>r|)>|)>
  </equation>

  \;

  <section|One atom in a Fabry-P�rot Cavity>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|V-PZW|<tuple|21|4>>
    <associate|a-sol|<tuple|9|2>>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|3>>
    <associate|auto-3|<tuple|3|5>>
    <associate|auto-4|<tuple|3.1|5>>
    <associate|auto-5|<tuple|3.2|5>>
    <associate|auto-6|<tuple|3.3|6>>
    <associate|auto-7|<tuple|4|6>>
    <associate|before-rwa|<tuple|14|?>>
    <associate|continuite|<tuple|32|5>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|ham-raw|<tuple|3|?>>
    <associate|max-ampere|<tuple|31|5>>
    <associate|max-faraday|<tuple|30|5>>
    <associate|max-gauss|<tuple|28|5>>
    <associate|max-thomson|<tuple|29|5>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>One
      atom in the vacuum> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Hamiltonian
      of atoms coupled by the EM field> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Dyadic
      Green's function of maxwell equations>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Maxwell equations
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|3.2<space|2spc>Wave equation in Fourier
      space <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|3.3<space|2spc>Green propagator
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>One
      atom in a Fabry-P�rot Cavity> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>