<TeXmacs|2.1>

<style|generic>

<\body>
  <\itemize>
    <item>Comment �tendre l'indice de Bott � d'autres g�om�tries ? Hexagone ?
    Triangle ?\ 

    <item>Quel est le lien entre l'indice de Stiefel Whitney et le Chern
    number/Bott index ?

    <item>
  </itemize>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>