<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Topology in tight-binding models>|<\doc-misc>
    Presentations or seminars on topology often employ the familiar
    illustration of a mug being transformed into a donut to explain this
    concept. While this image is effective in conveying how topology concerns
    itself with global properties of objects, it prompts a lurking question:
    how does this relate to physics? I aim to address this inquiry by
    introducing a topological invariant that does not necessitate translation
    invariance for computation. Additionally, I will provide an intuitive
    explanation for what transpires in this context: why is it an integer?
    Why does it become nonzero when the system is topological? And what does
    it mean for a system to be topological? This discussion will focus on a
    simple model\Vthe Haldane model (PRL 61, 1988)\Vand will later extend to
    another tight-binding model that I have recently been investigating.
  </doc-misc>>

  <section*|Intro>

  <\itemize>
    <item>Mug Donut and topology

    <item>QHE 1980 by Klaus von Klitzing, edge states, condutance exactly
    quantized, resist to disorder.

    <item>Magnetic field drops to zero but not the conductance: QAHE Haldane
    1988

    <item><strong|Why do the presence of edge states make some numbers, the
    topological invariants non-zero ?>

    <item>No Brillouin Zone, pure real space, simple math.
  </itemize>

  <section|Basic Math>

  <subsection|Complex logarithm>

  <\equation*>
    Log z=ln r+<around*|(|\<theta\>+2\<pi\>k|)>i,r\<gtr\>0,\<theta\>\<in\>\<bbb-R\>
  </equation*>

  <strong|Branch cuts> <math|\<rightarrow\>> we turn this multiple-valued
  function into a single-valued function by restrinction:

  <\equation*>
    \<theta\>+2\<pi\>k\<in\><around*|(|\<alpha\>,\<alpha\>+2\<pi\>|)>
  </equation*>

  In the following we consider consider the <math|\<bbb-R\><rsup|->> line as
  our branch cut, i.e. <math|\<alpha\>=\<pi\>>.\ 

  The restriction<strong| is not defined> on the branch cut.

  Logarithm of a matrix:

  <\equation*>
    log M=<big|oint><rsub|\<Gamma\>>Log <around*|(|z|)><around*|(|z<with|font|Bbb|1>-M|)><rsup|-1>\<mathd\>z
  </equation*>

  The eigenvalues cannot lie on the branch cut.

  <subsection|Approximation by commuting matrices>

  <math|U,V\<in\>GL<rsub|n><around*|(|\<bbb-C\>|)>>, can you approximate by
  commuting matrices <math|U<rsub|0>> and <math|V<rsub|0>> ?

  Approximate: <math|<around*|\<\|\|\>|U-U<rsub|0>|\<\|\|\>>/<around*|\<\|\|\>|U|\<\|\|\>>\<approx\>0>.

  To solve this problem, mathematicians introduced a quantity:

  <\equation*>
    Bott<around*|(|U,V|)>=<frac|1|2\<pi\>i>Tr<around*|(|log<around*|(|U*V*U<rsup|-1>V*<rsup|-1>|)>|)>\<in\>\<bbb-Z\>
  </equation*>

  Proof ?

  Write that:

  <\equation*>
    <big|prod><rsub|j=1><rsup|n>\<lambda\><rsub|j>=1=<big|prod><rsub|j=1><rsup|n><around*|\||\<lambda\><rsub|j>|\|>=<big|prod><rsub|j=1><rsup|n><around*|\||\<lambda\><rsub|j>|\|>exp<around*|(|i\<theta\><rsub|j>|)>
  </equation*>

  And then I state that for a path <math|\<gamma\>> that does not cross
  <math|\<bbb-R\><rsup|->>:

  <\equation*>
    \<forall\>s\<in\>\<gamma\>,Bott<around*|(|U<around*|(|s|)>,V<around*|(|s|)>|)>=Bott<around*|(|U<around*|(|0|)>,V<around*|(|0|)>|)>
  </equation*>

  Particularly:

  <\equation*>
    U<around*|(|0|)>=V<around*|(|0|)>=<with|font|Bbb|1>
  </equation*>

  Are we gonna cross the banch cut ??? Have a look at

  <\equation*>
    \<sigma\><around*|(|U*V*U<rsup|-1>V*<rsup|-1>|)>
  </equation*>

  <section|Haldane Model>

  SLIDES

  <\equation*>
    <block*|<tformat|<table|<row|<cell|H=t<rsub|1><big|sum><rsub|<around*|\<langle\>|i,j|\<rangle\>>>c<rsub|i><rsup|\<dagger\>>c<rsub|j>+i*t<rsub|2><big|sum><rsub|<around*|\<langle\>|<around*|\<langle\>|i,j|\<rangle\>>|\<rangle\>>>\<sigma\><rsub|i*j>c<rsub|i><rsup|\<dagger\>>c<rsub|j>+M<big|sum><rsub|i>\<varepsilon\><rsub|i>c<rsub|i><rsup|\<dagger\>>c<rsub|i>>>>>>
  </equation*>

  <section|Bott index for the physicist>

  <subsection|Wannier functions>

  Set of orthogonal localized functions.

  <\itemize>
    <item><math|H> can deform into <math|H<rsub|Trivial>> without closing the
    gap <math|\<Rightarrow\>> localized wannier function

    <item>no localized wannier function <math|\<Rightarrow\>> <math|H> is
    topological
  </itemize>

  <subsection|Bott index on a torus>

  <\equation*>
    P=<big|sum><rsub|E\<less\>E<rsub|F>><around*|\||i|\<rangle\>><around*|\<langle\>|i|\|>
  </equation*>

  <\equation*>
    U=P*exp<around*|(|<frac|2\<pi\>i|L<rsub|x>>X|)>P and
    V=P*exp<around*|(|<frac|2\<pi\>i|L<rsub|x>>Y|)>P
  </equation*>

  \;

  <subsection|Bott index on Haldane model>

  SLIDES

  <subsection|Spin Bott index>

  <section|Kekule Texture Model>

  Specifically, <math|H<rsub|1>> and <math|H<rsub|2 >>can be written as:

  <\equation*>
    H<rsub|1>=-t<rsub|1><big|sum><rsub|i,j>c<rsub|i><rsup|\<dagger\>>c<rsub|j><space|1em>H<rsub|2>=-t<rsub|2><big|sum><rsub|m,n>c<rsub|m><rsup|\<dagger\>>c<rsub|n>
  </equation*>

  with <math|i,j> (<math|m,n>) running over NN-sites inside the hexagonal
  unit (NN-sites between hexagonal unit cell) and
  \ <math|c<rsub|i><rsup|\<dagger\>>> (<math|c<rsub|i>>) is the creation
  (annihilation) operator for the <math|i>-th site in the cluster. This
  hamiltonian takes the effective form:

  <\equation*>
    H<around*|(|<math-bf|k>|)>=-t<rsub|1><matrix|<tformat|<table|<row|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>>>>>-t<rsub|2><matrix|<tformat|<table|<row|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i<math-bf|k>\<cdot\><math-bf|a><rsub|1>>>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i<math-bf|k>\<cdot\><math-bf|a><rsub|2>>>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i<math-bf|k>\<cdot\><around*|(|<math-bf|a><rsub|2>-<math-bf|a><rsub|1>|)>>>>|<row|<cell|\<mathe\><rsup|-i<math-bf|k>\<cdot\><math-bf|a><rsub|1>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|\<mathe\><rsup|-i<math-bf|k>\<cdot\><math-bf|a><rsub|2>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|-i<math-bf|k>\<cdot\><around*|(|<math-bf|a><rsub|2>-<math-bf|a><rsub|1>|)>>>|<cell|0>|<cell|0>|<cell|0>>>>>
  </equation*>

  <\big-figure|<image|/home/these/chapter-5-thesis/figs/sbi_t2_pbc.pdf|0.5par|||>>
    \;
  </big-figure>

  SLIDES

  <blur|<\par-block>
    <section|From tight-binding to point-scatterers>

    SLIDES
  </par-block>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|1>>
    <associate|auto-10|<tuple|3.4|2>>
    <associate|auto-11|<tuple|4|2>>
    <associate|auto-12|<tuple|1|3>>
    <associate|auto-13|<tuple|5|3>>
    <associate|auto-14|<tuple|5|3>>
    <associate|auto-15|<tuple|5|3>>
    <associate|auto-16|<tuple|5|3>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|1.1|1>>
    <associate|auto-4|<tuple|1.2|2>>
    <associate|auto-5|<tuple|2|2>>
    <associate|auto-6|<tuple|3|2>>
    <associate|auto-7|<tuple|3.1|2>>
    <associate|auto-8|<tuple|3.2|2>>
    <associate|auto-9|<tuple|3.3|2>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-15>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Basic
      Math> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Complex logarithm
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>Approximation by commuting
      matrices <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Haldane
      Model> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Graphene: a tight-binding
      approach <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>Breaking TRS
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|2.3<space|2spc>Kane-Mele model
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Bott
      index for the physicist> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Wannier functions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|1tab>|3.2<space|2spc>Bott index on a torus
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|1tab>|3.3<space|2spc>Bott index on Haldane model
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|1tab>|3.4<space|2spc>Spin Bott index
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Kekule
      Texture Model> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|5<space|2spc>From
      tight-binding to point-scatterers> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>