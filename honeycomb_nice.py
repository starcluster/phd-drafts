import numpy as np
import matplotlib.pyplot as plt


def hex_coords(x: float = 0, y: float = 0, a: float = 1):
    """Generates coords of an hexagon of size a centered in (x,y)"""
    coords = []
    for i in range(6):
        angle_rad = np.pi / 3 * i
        coord_x = x + a * np.cos(angle_rad)
        coord_y = y + a * np.sin(angle_rad)
        coords.append((coord_x, coord_y))
    return coords

def gen_coord(n_cluster):
    x_lat = []
    y_lat = []
    for i in range(n_cluster):
        for j in range(n_cluster):
            x_center = 3*j+3*i/2
            y_center = 3*np.sqrt(3)/2*i
            coord = hex_coords(x_center, y_center)
            for c in coord:
                x_lat.append(c[0])
                y_lat.append(c[1])

    return x_lat,y_lat


if __name__ == "__main__":
    n_cluster = 8
    x_lat, y_lat = gen_coord(n_cluster)
    plt.scatter(x_lat, y_lat, color="black")
    plt.show()
