<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\shown>
    <\equation*>
      H<around*|(|k<rsub|x>|)>=<matrix|<tformat|<table|<row|<cell|H<rsub|1>>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|H<rsub|1>>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|\<cdots\>>|<cell|>>|<row|<cell|0>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|H<rsub|1>>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|>>|<row|<cell|>|<cell|0>|<cell|2t<rsub|2>cos<around*|(|k<rsub|x>R|)>I<rsub|6>>|<cell|\<ddots\>>|<cell|>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|H<rsub|1>>>>>>
    </equation*>
  </shown>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>