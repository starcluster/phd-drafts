<TeXmacs|2.1>

<style|generic>

<\body>
  Dear Editor,

  Thank you for sending us the Referee report on our manuscript \PTopological
  photonic band gaps in honeycomb atomic arrays\Q. We thank the Referee for
  careful reading of the manuscript and for valuable comments and
  suggestions. We revised the manuscript accordingly and resubmit it with
  changes highlighted in blue. Below we present point-by-point replies to
  Referee comments and a summary of the changes to the manuscript.

  <with|color|dark cyan|Referee writes:>

  <with|color|dark cyan|\P1. When computing the Chern number in section 2.4 I
  would like the authors to discuss how the singularities around Gamma affect
  the Chern number calculation. Do they pose any problems in convergence?\Q>

  Our reply:

  We comment on this issue on p. 9 of the revised manuscript. The role of
  singularities arising at <math|<around*|\||<math-bf|k>|\|>=k<rsub|0>> can
  be made clear by integrating Berry curvature over hexagonal areas of
  increasing side <math|k<rsub|1>> up to the maximum size corresponding to
  the Brillouin zone, and examining the behavior of the result around
  <math|k<rsub|1>=k<rsub|0>>. The result of such an integration is presented
  in Figure 2 below. We also show Berry curvature as a function of
  <math|<math-bf|k>> in Figure 1:

  <\big-figure|<image|/home/pierre/chapter-3-thesis/figs/bands_1_2_dB12.pdf|0.65par|||>>
    Sum of the Berry curvatures over the two lowest frequency bands.
  </big-figure>

  <\big-figure|<image|/home/pierre/chapter-3-thesis/figs/integrating_bc_on_different_region_annot.pdf|0.6par|||>>
    Integrated Berry curvature over a hexagon of side <math|k<rsub|1>>. The
    two vertical lines represent the hexagon inscribed within the circle
    <math|<around*|\||<math-bf|k>|\|>=k<rsub|0>> and the smallest hexagon
    that contains the circle.\ 
  </big-figure>

  \;

  We observe that Berry curvature becomes different from zero for
  <math|<around*|\||<math-bf|k>|\|>> around <math|k<rsub|0>>, however without
  causing any particular problem in the integration over <math|<math-bf|k>>.
  The result of the latter grows monotonically with <math|k<rsub|1>> and
  converges to <math|C=1> when the integration area covers the full Brillouin
  zone.

  <with|color|dark cyan|Referee writes:>

  <with|color|dark cyan|\P2. In the same section, I am a little surprised
  that they don't make connection to the extensive literature of
  non-Hermitian topology. In particular the system in free space has a clear
  non-hermitian component due to the decay rate. In non-Hermitian systems one
  can also define Chern numbers. More generally, when the spectrum is
  complex, as it is the case up to section 3, one can define invariants in
  the space of eigenvalues (<math|Re<around*|(|E|)>> and
  <math|Im<around*|(|E|)>>). Here however, the authors choose to define the
  Chern number as if the decay rate is not zero. I think the paper can
  improve substantially if the connection with non-hermitian topology is
  made.\Q>

  Our reply:

  We now make the link with the recent literature on non-Hermitian topology
  on p. 8, after Eq. (22). Indeed, topological properties of non-Hermitian
  systems may be richer than those of Hermitian ones and Chern number (21) is
  not always appropriate nor sufficient to analyze them. However, in our
  system the non-Hermitian aspect is rather trivial, concerns only a small
  fraction of states (those within the light cone), and the gap between
  eigenvalues on the complex plane can be identified as a real line gap.
  Thus, according to the previous studies [21,22], Chern number (22) is the
  appropriate topological invariant to consider.\ 

  <with|color|dark cyan|Referee writes:>

  <with|color|dark cyan|\P3. Lastly, the authors say at the end that when
  <math|d\<less\>\<pi\>/k<rsub|0>> it is hard to define topology. They also
  observe that the system becomes gapless. Hence, their remark that this
  complicates the calculation of insulators seems to be not a well posed
  question, since to define invariants for insulators one needs a gap.
  <with|color|dark cyan|Perhaps they can clarify further what they
  mean.<nbsp>\T>>

  Our reply:

  We assume that the Referee means \P<math|d\<gtr\>\<pi\>/k<rsub|0>>\Q
  because this is where complications arise. We admit that our initial
  explanations for the reasons of these complications might have been
  unclear. We have now modified and appended them (see the new text on pp.
  14,15 and 16). In short, propagating modes with <math|k<rsub|z>> different
  from zero arise in the system with large <math|d>. Thus, 2D analysis in
  which the band diagram is studied as a function of
  <math|k<rsub|x>,k<rsub|y>> and Chern number is calculated by integrating
  over <math|k<rsub|x>,k<rsub|y>>, becomes insufficient. It remains to be
  seen whether a gap is still present in the system with large <math|d> and
  whether this gap is topologically nontrivial, but such an analysis is
  beyond the scope of the present work.

  We hope that the above reply and changes to the manuscript are sufficient
  to make the latter acceptable for publication.

  \;

  Sincerely yours,

  P. Wulles and S. Skipetrov
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Sum of the Berry curvatures over the two lowest frequency bands.
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Integrated Berry curvature over hexagons with side
        <with|mode|<quote|math>|k<rsub|1>>. The two vertical lines represent
        the hexagon inscribed within the circle
        <with|mode|<quote|math>|<around*|\||<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|k>>>|\|>=k<rsub|0>>
        and the smallest hexagon that contains the circle.\ 
      </surround>|<pageref|auto-2>>
    </associate>
  </collection>
</auxiliary>