<TeXmacs|2.1>

<style|generic>

<\body>
  <doc-data|<doc-title|Answer to the referee>>

  <\itemize>
    <item>When computing the Chern number in section 2.4 I would like the
    authors to discuss how the singularities around Gamma affect the Chern
    number calculation. Do they pose any problems in convergence?
  </itemize>

  The singularities around Gamma slightly affect the value of the Berry
  curvature as demonstrated by Fig. <reference|bc>, however these artefacts
  are very small and end up being negligeable when integrating over the
  entire Brillouin zone to get the Chern number.

  <\big-figure|<image|/home/pierre/chapter-3-thesis/figs/bc_12_0.pdf|0.5par|||>>
    <label|bc>The Berry curvature of the first band of our system over the
    entiere Brillouin zone. Computed here for
    <math|\<Delta\><rsub|<math-bf|B>>=12> and <math|\<Delta\><rsub|A*B>=0>.
    No plates. The glitches are observed for
    <math|<around*|\<\|\|\>|<math-bf|k>|\<\|\|\>>=k<rsub|0>>.
  </big-figure>

  <\itemize>
    <item> In the same section, I am a little surprised that they don't make
    connection to the extensive literature of non-Hermitian topology. In
    particular the system in free space has a clear non-hermitian component
    due to the decay rate. In non-Hermitian systems one can also define Chern
    numbers. More generally, when the spectrum is complex, as it is the case
    up to section 3, one can define invariants in the space of eigenvalues
    (<math|Re<around*|(|E|)>> and <math|Im<around*|(|E|)>>). Here however,
    the authors choose to define the Chern number as if the decay rate is not
    zero. I think the paper can improve substantially if the connection with
    non-hermitian topology is made.
  </itemize>

  Even if our Hamiltonian has complex eigenvalues, most of its eigenvalues
  are lying on the real axis and the gap we are having an interest at is
  situated in a region where <math|Im<around*|(|E<around*|(|<math-bf|k>|)>|)>=0>,
  when we are situated around the point <math|K> or <math|K<rprime|'>>.
  Nevertheless, we can choose to plot the eigenvalues of
  <math|H<around*|(|<math-bf|k>|)>> for all <math|<math-bf|k>> that belong to
  the Brillouin zone. This shows that in the region where
  <math|<around*|\<\|\|\>|<math-bf|k>|\<\|\|\>>\<less\>k<rsub|0>> the
  eigenvalues separate in two groups separated by a so-called \Pline-gap\Q
  [cite, not sure] see Fig. <reference|ev>. But we did not investigated
  further on this topic.

  Concerning the computation of the Chern number we believe that a non-zero
  decay rate is not a problem; indeed it has been shwon by <hlink|Shen et
  al|https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.120.146402>
  that in non-hermitian situation; one can define the Chern number either by
  using right-right eignevectors but also, left-right, right-left and
  left-left and this will not change the topology shown by the index.

  <\big-figure|<image|/home/pierre/chapter-3-thesis/figs/eigenvalues_mk_on_bz_12_0.pdf|0.5par|||><image|/home/pierre/chapter-3-thesis/figs/eigenvalues_mk_on_bz_0_12.pdf|0.502par|||>>
    <label|ev>Eigenvalues of <math|H> on the entire Brillouin Zone for (left)
    <math|\<Delta\><rsub|<math-bf|B>>=12>, <math|\<Delta\><rsub|A*B>=0> and
    (right) for <math|\<Delta\><rsub|<math-bf|B>>=0> and
    <math|\<Delta\><rsub|A*B>=12>. <math|\<Delta\><rsub|<math-bf|B>>> is also
    opening a \Pline-gap\Q within the groupe of complex eigenvalues.\ 
  </big-figure>

  <\itemize>
    <item> Lastly, the authors say at the end that when
    <math|d\<less\>\<pi\>/k<rsub|0>> it is hard to define topology. They also
    observe that the system becomes gapless. Hence, their remark that this
    complicates the calculation of insulators seems to be not a well posed
    question, since to define invariants for insulators one needs a gap.
    Perhaps they can clarify further what they mean.
  </itemize>

  What we mean is that when <math|d\<gtr\>\<pi\>/k<rsub|0>>, there are
  eigenvalues for <math|<around*|\<\|\|\>|<math-bf|k>|\<\|\|\>>\<less\>k<rsub|0>>
  that can appar in the region that was before a gap. There are sporadic
  apparition and does not constitue a clear band. One way to still define a
  gap would be to ignore thoses values in this precise region and proceed as
  before but unfortunately this does not work as we don't know to which bands
  the eigenvalues should be attributed.

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?|../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-2|<tuple|2|?|../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|bc|<tuple|1|?|../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|ev|<tuple|2|?|../.TeXmacs/texts/scratch/no_name_1.tm>>
  </collection>
</references>