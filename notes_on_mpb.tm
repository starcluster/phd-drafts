<TeXmacs|2.1>

<style|generic>

<\body>
  <doc-data|<doc-title|Solving Maxwell equations in Fourier space using MPB>>

  <section|Honeycomb dielectric photonic crystal>

  First, I try to retrieve the band diagram for the honeycomb dielectric
  photonic crystal in order to verify that I use the code in a correct
  manner.

  <subsection|Band diagram and gaps >

  This band diagram matches band diagram found online:

  <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/honeycomb_mpb_0.2.pdf|0.5par|||>>
    <math|r=0.2a> and <math|a=1>cm, <math|\<varepsilon\>=12\<varepsilon\><rsub|0>>
    for a cylinder.
  </big-figure>

  <subsection|Examining the modes>

  I can now examine the electric-field distributions for some of the bands
  which were saved at the <math|K> point. Besides looking neat, the field
  patterns will tell us about the characters of the modes and provide some
  hints regarding the origin of the band gap. Indeed we can observe between
  mode 2 and 3 a change in the type of electric field distribution. Also
  observed between 6 and 7, this corresponds to the two gaps shown
  previously. However between mode 1 and 2 we can observe orthogonality
  explaining the \Ptouching point\Q.

  <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/honeycomb_mpb_modes_0.2.pdf|0.7par|||>>
    <math|r=0.2a> and <math|a=1>cm, <math|\<varepsilon\>=12\<varepsilon\><rsub|0>>
    for a cylinder.
  </big-figure>

  <section|Kekule dielectric photonic crystal>

  Incresing the gap of the cluster seem to open a gap, however investigations
  must be lead to determine if points used in Fourier space are correct.\ 

  <subsection|Band diagram and gaps >

  <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/kekule_mpb_0.05_0.1.pdf|0.3par|||>
  <image|/home/pierre/chapter-5-thesis/figs/kekule_mpb_0.05_1.pdf|0.3par|||>
  <image|/home/pierre/chapter-5-thesis/figs/kekule_mpb_0.05_1.2.pdf|0.3par|||>>
    Different value of cluster: <math|R=0.1a>,<math|R=a> and <math|R=1.2a>.
    We also have <math|r=0.2a> and <math|a=1>cm,
    <math|\<varepsilon\>=12\<varepsilon\><rsub|0>> for a cylinder.\ 
  </big-figure>

  <subsection|Examining the modes>

  <\big-figure|<image|/home/pierre/chapter-5-thesis/figs/kekule_mpb_modes_0.05_0.1.pdf|0.3par|||>
  <image|/home/pierre/chapter-5-thesis/figs/kekule_mpb_modes_0.05_1.pdf|0.3par|||>
  <image|/home/pierre/chapter-5-thesis/figs/kekule_mpb_modes_0.05_1.2.pdf|0.3par|||>>
    Modes shown for different value of cluster: <math|R=0.1a>,<math|R=a> and
    <math|R=1.2a>. We also have <math|r=0.2a> and <math|a=1>cm,
    <math|\<varepsilon\>=12\<varepsilon\><rsub|0>> for a cylinder
  </big-figure>
</body>

<\initial>
  <\collection>
    <associate|page-bot|5mm>
    <associate|page-even|5mm>
    <associate|page-medium|paper>
    <associate|page-odd|5mm>
    <associate|page-right|5mm>
    <associate|page-top|5mm>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-10|<tuple|4|2|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-2|<tuple|1.1|1|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-3|<tuple|1|1|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-4|<tuple|1.2|1|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-5|<tuple|2|1|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-6|<tuple|2|2|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-7|<tuple|2.1|2|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-8|<tuple|3|2|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-9|<tuple|2.2|2|../.TeXmacs/texts/scratch/no_name_2.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        <with|mode|<quote|math>|r=0.2a> and <with|mode|<quote|math>|a=1>cm,
        <with|mode|<quote|math>|\<varepsilon\>=12\<varepsilon\><rsub|0>> for
        a cylinder.
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        <with|mode|<quote|math>|r=0.2a> and <with|mode|<quote|math>|a=1>cm,
        <with|mode|<quote|math>|\<varepsilon\>=12\<varepsilon\><rsub|0>> for
        a cylinder.
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Different value of cluster: <with|mode|<quote|math>|R=0.1a>,<with|mode|<quote|math>|R=a>
        and <with|mode|<quote|math>|R=1.2a>. We also have
        <with|mode|<quote|math>|r=0.2a> and <with|mode|<quote|math>|a=1>cm,
        <with|mode|<quote|math>|\<varepsilon\>=12\<varepsilon\><rsub|0>> for
        a cylinder.\ 
      </surround>|<pageref|auto-8>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        Modes shown for different value of cluster:
        <with|mode|<quote|math>|R=0.1a>,<with|mode|<quote|math>|R=a> and
        <with|mode|<quote|math>|R=1.2a>. We also have
        <with|mode|<quote|math>|r=0.2a> and <with|mode|<quote|math>|a=1>cm,
        <with|mode|<quote|math>|\<varepsilon\>=12\<varepsilon\><rsub|0>> for
        a cylinder
      </surround>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Honeycomb
      dielectric photonic crystal> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Band diagram and gaps
      \ <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>Examining the modes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Kekule
      dielectric photonic crystal> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Band diagram and gaps
      \ <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>Examining the modes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>
    </associate>
  </collection>
</auxiliary>