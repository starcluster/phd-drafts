<TeXmacs|2.1>

<style|<tuple|generic|british>>

<\body>
  <doc-data|<doc-title|The Stiefel-Whitney
  class>|<doc-author|<author-data|<author-name|Pierre Wulles>>>>

  <section|The awful truth of chern number>

  We consider a gapped Hamiltonian <math|H<around*|(|<math-bf|k>|)>> of a 2D
  system. To define the Chern number we first introduce a vector named
  \PBerry connection\Q

  <\equation>
    <math-bf|A><rsub|m><around*|(|<math-bf|k>|)>=i<around*|\<langle\>|\<psi\><rsub|m><around*|(|<math-bf|k>|)><around*|\||\<nabla\>|\<nobracket\>>\<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<rangle\>><label|berry-connection>
  </equation>

  Where the <math|<around*|\||\<psi\><rsub|j>|\<rangle\>>> are eigenfunctions
  of <math|H<around*|(|<math-bf|k>|)>>, <math|m> means the eigenfunction of
  the band <math|m>. Then we introduce the \PBerry curvature\Q, a scalar:

  <\equation>
    F<rsub|m><around*|(|<math-bf|k>|)>=\<partial\><rsub|k<rsub|x>>A<rsub|m,x><around*|(|<math-bf|k>|)>-\<partial\><rsub|k<rsub|y>>A<rsub|m,y><around*|(|<math-bf|k>|)><label|berry-curvature>
  </equation>

  From which we can finally define the Chern number:

  <\equation>
    C=<big|sum><rsub|m=1><rsup|N<rsub|occ>><big|int><rsub|\<cal-M\>><frac|\<mathd\><rsup|2>k|2\<pi\>>F<rsub|j><around*|(|<math-bf|k>|)><label|chern-number>
  </equation>

  We will now assume that the Hamiltonian of our system is real. This can
  easily happen for a tight-binding system.

  <\equation*>
    H=H<rsup|\<ast\>>
  </equation*>

  Then it comes that eigenfunctions are reals as well (and
  <math|\<lambda\>\<in\>\<bbb-R\>>):

  <\equation*>
    H<around*|\||\<psi\>|\<rangle\>>=\<lambda\><around*|\||\<psi\>|\<rangle\>>
  </equation*>

  But if the eigenfunctions are reals from <reference|berry-connection> and
  <reference|berry-curvature> we get the following:

  <\eqnarray*>
    <tformat|<table|<row|<cell|-i*F<rsub|m><around*|(|<math-bf|k>|)>>|<cell|=>|<cell|\<partial\><rsub|k<rsub|x>><around*|\<langle\>|\<psi\><rsub|m><around*|(|<math-bf|k>|)><rsub|><around*|\||\<partial\><rsub|k<rsub|y>>|\<nobracket\>>\<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<rangle\>>-\<partial\><rsub|k<rsub|y>><around*|\<langle\>|\<psi\><rsub|m><around*|(|<math-bf|k>|)><rsub|><around*|\||\<partial\><rsub|k<rsub|x>>|\<nobracket\>>\<psi\><rsub|m><around*|(|<math-bf|k>|)>|\<rangle\>>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|i=1><rsup|N>\<partial\><rsub|k<rsub|x>><around*|(|\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)><rsub|><around*|(|\<partial\><rsub|k<rsub|y>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)>|)><rsup|\<ast\>>|)>-\<partial\><rsub|k<rsub|y>><around*|(|\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)><rsub|><around*|(|\<partial\><rsub|k<rsub|x>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)>|)><rsup|\<ast\>>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|i=1><rsup|N>\<partial\><rsub|k<rsub|x>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)><rsub|><around*|(|\<partial\><rsub|k<rsub|y>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)>|)><rsup|\<ast\>>+\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)><rsub|><around*|(|\<partial\><rsub|k<rsub|x>>\<partial\><rsub|k<rsub|y>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)>|)><rsup|\<ast\>>>>|<row|<cell|>|<cell|>|<cell|-\<partial\><rsub|k<rsub|y>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)><rsub|><around*|(|\<partial\><rsub|k<rsub|x>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)>|)><rsup|\<ast\>>+\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)><rsub|><around*|(|\<partial\><rsub|k<rsub|y>>\<partial\><rsub|k<rsub|x>>\<psi\><rsub|m><rsup|i><around*|(|<math-bf|k>|)>|)><rsup|\<ast\>>>>>>
  </eqnarray*>

  From which it is clear that if <math|<around*|\||\<psi\>|\<rangle\>>=<around*|\||\<psi\>|\<rangle\>><rsup|\<ast\>>>
  then <math|F> will always be zero and so the Chern number.

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|berry-connection|<tuple|1|1>>
    <associate|berry-curvature|<tuple|2|1>>
    <associate|chern-number|<tuple|3|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>The
      awful truth of chern number> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>