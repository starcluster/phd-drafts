<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Two-level atom>>

  From Le Bellac

  <\equation*>
    H<rsup|<around*|(|0|)>><around*|\||a|\<rangle\>>=E<rsub|a><around*|\||a|\<rangle\>><space|1em>and<space|2em>H<rsup|<around*|(|0|)>><around*|\||b|\<rangle\>>=E<rsub|b><around*|\||b|\<rangle\>>
  </equation*>

  and:

  <\equation*>
    <around*|\||\<psi\><around*|(|t|)>|\<rangle\>>=\<gamma\><rsub|a><around*|(|t|)>\<mathe\><rsup|-i*E<rsub|a>t/\<hbar\>>+<big|int>\<gamma\><rsub|b><around*|(|t|)>\<mathe\><rsup|-i*E<rsub|b>t/\<hbar\>>\<cal-D\><around*|(|E<rsub|b>|)>\<mathd\>E<rsub|b>
  </equation*>

  We use Schrödinger equation with <math|H=H<rsup|<around*|(|0|)>>+W>,
  <math|W> is independant of time and <math|W<rsub|a*a>=W<rsub|b*b>=0>. We
  get:

  <\eqnarray*>
    <tformat|<table|<row|<cell|i\<hbar\><wide|\<gamma\>|\<dot\>><rsub|a><around*|(|t|)>>|<cell|=>|<cell|<big|int>\<mathe\><rsup|i\<omega\><rsub|a*b>t>W<rsub|a*b>\<gamma\><rsub|b><around*|(|t|)>\<cal-D\><around*|(|E<rsub|b>|)>\<mathd\>E<rsub|b>>>|<row|<cell|i\<hbar\><wide|\<gamma\>|\<dot\>><rsub|b><around*|(|t|)>>|<cell|=>|<cell|\<gamma\><rsub|a><around*|(|t|)>\<mathe\><rsup|-i*\<omega\><rsub|a*b>t/\<hbar\>>W<rsub|a*b><rsup|**\<ast\>>>>>>
  </eqnarray*>

  We assume:

  <\equation*>
    \<gamma\><rsub|a><around*|(|t|)>=exp<around*|(|-<frac|i\<delta\>|2>t|)><space|2em>\<delta\>=\<delta\><rsub|1>-i\<Gamma\>
  </equation*>

  For the initial condition <math|\<gamma\><rsub|b><around*|(|t=0|)>=0>:

  <\equation*>
    \<gamma\><rsub|b><around*|(|t|)>=<frac|W<rsub|a*b><rsup|\<ast\>>|\<hbar\><around*|(|\<omega\><rsub|a*b>+\<delta\>/2|)>>
  </equation*>

  For long time:

  <\equation*>
    lim<rsub|t\<rightarrow\>+\<infty\>><around*|\||\<gamma\><rsub|b><around*|(|t|)>|\|><rsup|2>=<frac|<around*|\||W<rsub|a*b><rsup|>|\|><rsup|2>|\<hbar\><rsup|2><around*|(|<around*|(|\<omega\><rsub|a*b>+\<delta\><rsub|1>/2|)><rsup|2>+\<Gamma\><rsup|2>/4|)>>
  </equation*>

  and then:

  <\equation*>
    <frac|\<hbar\>\<delta\>|2>=<big|int><frac|<around*|\||W<rsub|a*b><rsup|>|\|><rsup|2><around*|[|1-exp<around*|(|i<around*|(|\<omega\><rsub|a*b>+\<delta\>/2|)>t|)>|]>|\<hbar\><around*|(|\<omega\><rsub|a*b>+\<delta\>/2|)>>\<cal-D\><around*|(|E<rsub|b>|)>\<mathd\>E<rsub|b>
  </equation*>

  For a two-level atom, <math|\<hbar\>\<omega\><rsub|a*b>=\<hbar\><around*|(|\<omega\><rsub|0>-\<omega\>|)>>
  and we make the substitution <math|\<mathd\>E<rsub|b>=\<hbar\>\<mathd\>\<omega\>>:

  <\equation*>
    <frac|\<hbar\>\<delta\>|2>=<big|int><rsub|0><rsup|+\<infty\>><frac|<around*|\||W<rsub|a*b><rsup|>|\|><rsup|2><around*|[|1-exp<around*|(|i<around*|(|\<omega\><rsub|0>-\<omega\>+\<delta\>/2|)>t|)>|]>|\<hbar\><around*|(|\<omega\><rsub|0>-\<omega\>+\<delta\>/2|)>>\<cal-D\><around*|(|\<omega\>|)>\<mathd\>\<omega\>
  </equation*>

  and finally:

  <\equation*>
    \<delta\>=<frac|2|\<hbar\>>\<cal-P\><big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|<around*|\||W<rsub|a*b><rsup|>|\|><rsup|2>|\<omega\><rsub|0>-\<omega\>>\<cal-D\><around*|(|\<omega\>|)>\<mathd\>\<omega\>-<frac|2i\<pi\>|\<hbar\>>\<cal-D\><around*|(|\<omega\><rsub|0>|)><around*|\||W<rsub|a*b><rsup|>|\|><rsup|2>
  </equation*>

  With:

  <\equation*>
    D<around*|(|\<omega\>|)>=<frac|\<omega\>|\<pi\>c<rsup|2>>Im<around*|(|\<cal-G\>|)>
  </equation*>

  We remind that <math|\<cal-G\>> is given by:

  <\equation*>
    \<cal-G\><around*|(|r|)>=-<frac|\<mathe\><rsup|i*k<rsub|0>r>|4\<pi\>r><around*|(|P<around*|(|i*k<rsub|0>r|)>+<frac|<math-bf|r>\<otimes\><math-bf|r>|r<rsup|2>>Q<around*|(|i*k<rsub|0>r|)>|)>
  </equation*>

  So:

  <\equation*>
    Im<around*|(|\<cal-G\><around*|(|r|)>|)>=<frac|-cos<around*|(|k<rsub|0>r|)>|2\<pi\>r<rsup|2>k<rsub|0>>+<frac|sin<around*|(|k<rsub|0>r|)>|2\<pi\>r<around*|(|k<rsub|0>r|)><rsup|2>>
  </equation*>

  Notice that if we take the limit <math|r\<rightarrow\>0>:

  <\equation*>
    lim<rsub|r\<rightarrow\>0><around*|(|Im<around*|(|\<cal-G\><around*|(|r|)>|)>|)>=<frac|k<rsub|0>|6\<pi\>>
  </equation*>

  And thus:

  <\equation*>
    <frac|2i\<pi\>|\<hbar\>>\<cal-D\><around*|(|\<omega\><rsub|0>|)><around*|\||W<rsub|a*b><rsup|>|\|><rsup|2>=<frac|2i\<pi\>|\<hbar\>><frac|\<omega\>|\<pi\>c<rsup|2>><frac|k<rsub|0>|6\<pi\>><around*|\||W<rsub|a*b><rsup|>|\|><rsup|2>
  </equation*>

  We consider that <math|<around*|\||W<rsub|a*b><rsup|>|\|><rsup|2>=\<mu\><rsup|2>\<hbar\>\<omega\>/2\<varepsilon\><rsub|0>>
  (??) so:

  <\equation*>
    \<Gamma\>=<frac|\<omega\><rsup|3>|c<rsup|3>><frac|\<mu\><rsup|2>|6\<pi\>\<varepsilon\><rsub|0>>
  </equation*>

  Erreur ? ne correspond pas avec ??

  <\equation*>
    \<Gamma\><rsub|0>=<frac|2\<omega\><rsup|3>\<mu\><rsup|2>|3\<pi\>\<hbar\>\<varepsilon\><rsub|0>c<rsub|0><rsup|3>>
  </equation*>

  Now we can apply the method of images:

  <\equation*>
    \<cal-G\><around*|(|r|)>=-<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k<rsub|0>r>|4\<pi\>r<rsub|n>><around*|(|P<around*|(|i*k<rsub|0>r<rsub|n>|)>+<frac|<math-bf|r>\<otimes\><math-bf|r>|r<rsup|2>>Q<around*|(|i*k<rsub|0>r<rsub|n>|)>|)>
  </equation*>

  Again we get:

  <\equation*>
    Im<around*|(|\<cal-G\><around*|(|r|)>|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|-cos<around*|(|k<rsub|0>r<rsub|n>|)>|2\<pi\>r<rsub|n><rsup|2>k<rsub|0>>+<frac|sin<around*|(|k<rsub|0>r<rsub|n>|)>|2\<pi\>r<around*|(|k<rsub|0>r<rsub|n>|)><rsup|2>>
  </equation*>

  Which can be expressed as a sum of polylogarithm functions.

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
  </collection>
</references>