Imagine que la lumière c'est comme toi et tes amis qui jouent à
cache-cache dans un grand terrain de jeu.  Dans la plupart des
endroits, la lumière peut aller dans toutes les directions, un peu
comme si toi et amis jouaient au loup touche-touche dans toutes les
directions possibles. Mais dans ma thèse, c'est comme si quelqu'un
avait dessiné des flèches spéciales sur le sol, indiquant une seule
direction dans laquelle vous pouvez vous déplacer.

Ma thèse c'est ça: dessiner des flèches au sol des cours de récré ou
joue la lumière.


--------> Pas ici car enfant de 8 ans mais introduire les termes
          précis.
