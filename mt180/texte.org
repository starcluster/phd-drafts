Supposons que les photons, les petites particules qui constituent la
lumière, soient comme des enfants qui jouent dans une cour de récré,
ils courent à droite à gauche, ils crient et se déplacent librement
dans l'espace disponible comme le ferait les photons dans un
matériau. Arrive alors un maître particulièrement sévère et sadique
qui impose de nouvelles règles. S'en est fini de cette liberté, tous
sont contrains de se déplacer selon des lignes tracées à l'avance, à
la file indienne: ils doivent faire le tour de la cour en longeant le
bord. Cauchemar du gamin, mais rêve du physicien: maîtriser la
lumière, dompter les photons, s'assurer qu'ils ne peuvent se déplacer
que dans une seule direction sans jamais se rencontrer et ainsi perdre
du temps à bavarder.

Pendant que des enfants tournent dans la cour, en classe d'autres
étudient la topologie, la branche des mathématiques qui s'intéresse au
nombre de trous dans un objet. Par exemple, un topologiste ne verra
pas de différence entre un donut et un mug, car pour lui les deux
n'ont qu'un seul trou. La transformation de l'un à l'autre pourra donc
se faire sans à avoir à percer ou déchirer la matière qui les
constitue.

Le voilà notre instituteur tyran: la topologie. C'est lui qui fixe les
règles pour forcer les photons à se déplacer selon des trajectoires
bien définies situés sur les bords de notre milieu. La cour de récré
devient alors un isolant topologique.

En général cela fonctionne très bien si le materiau dans lequel la
lumière se propage est ordonnée, c'est-à-dire que les atomes se
positionnent selon un motif qui se répète. Comme si les enfants
avaient à leurs disposition une cour de récrée propre et nette. Dénuée
d'endroit ou se cacher. Mais qu'en est-il des cours remplies d'arbres,
de préau, de recoins ? Notre maître d'école, aussi rigoriste soit-il
aura bien de la peine à tout surveiller. On parle alors de milieu
désordonné. Dans mes recherches je cherche à savoir à quel niveau de
désordre notre professeur ne peut plus surveiller la cours et laisse
ainsi les enfants de nouveau jouer librement.

Mais ce n'est pas tout. Dans certains cas, de façon complètement
inattendue, c'est le désordre lui-même qui peut forcer les photons à
circuler sur le bord, comme si les enfants choisissaient, plutôt que
de profiter du désordre, de l'éviter. Ces situations sont rares et
n'apparaissent pas facilement, pour les trouver j'ai du faire de
nombreuses simulations avec mon ordinateur et j'ai supposé que les
photons étaient toujours placés dans un champ magnétique. Une astuce
bien connue des physiciens pour faire circuler les photons sur le bord
du matériau. En fait c'est un peu si il pleuvait dans la cours et que
les enfants étaient forcés de marcher le long du préau pour éviter la
pluie.

Mais les champs magnétiques, comme la pluie d'ailleurs, on aimerait
s'en passer, c'est le deuxième objectif de ma thèse: créer des
materiaux topologiques pour les photons mais sans champ magnétique.

En conclusion j'utilise la topologie pour identifier les matériaux qui
possèdent en quelque sorte des trous, cela force alors à la lumière à
se déplacer sur le bord. Ces matériaux je m'amuse ensuite à les
déformer pour voir si leurs propriétés persistent. Enfin j'essaye de
les créer mais sans passer par des champs magnétiques pour faciliter
les applications futures.

