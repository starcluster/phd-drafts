\babel@toc {british}{}\relax 
\contentsline {chapter}{\numberline {1}Topology in physics}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Topology in a nutshell}{4}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Counting holes}{4}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Curvature, from local to global}{6}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}The link with physics}{6}{subsection.1.1.3}%
\contentsline {section}{\numberline {1.2}Topology: from QHE to classical waves}{7}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Quantum Hall effect}{7}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Mechanical waves}{8}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Acoustic waves}{9}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Equatorial waves}{10}{subsection.1.2.4}%
\contentsline {subsubsection}{Aharonov-Bohm effect}{10}{subsection.1.2.4}%
\contentsline {subsubsection}{Coriolis force}{10}{figure.caption.6}%
\contentsline {subsubsection}{Kelvin waves}{12}{equation.1.2.9}%
\contentsline {section}{\numberline {1.3}Topology in Photonic}{13}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Genesis of photonic topology}{13}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}The surprising topological anderson insulator}{15}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Topological photonic fibers}{16}{subsection.1.3.3}%
\contentsline {subsection}{\numberline {1.3.4}Topological laser}{17}{subsection.1.3.4}%
\contentsline {section}{\numberline {1.4}Summary}{17}{section.1.4}%
\contentsline {chapter}{\numberline {2}Introducing tools and models for topological phase study }{19}{chapter.2}%
\contentsline {section}{\numberline {2.1}Graphene}{21}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Analysing Graphene's Brillouin zone}{21}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Graphene nanoribbon}{23}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Haldane's model}{25}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Brillouin zone}{25}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Nanoribbon}{27}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Chern number}{28}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Real space}{29}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Bott index}{31}{subsection.2.2.5}%
\contentsline {subsubsection}{Bott index of two unitary matrices}{31}{subsection.2.2.5}%
\contentsline {subsubsection}{Bott index of two invertible matrix}{32}{equation.2.2.49}%
\contentsline {subsubsection}{Vanishing of Bott index}{32}{equation.2.2.52}%
\contentsline {subsubsection}{Bott index for the physicist}{33}{equation.2.2.54}%
\contentsline {subsubsection}{Bott Index on Haldane's model}{34}{equation.2.2.63}%
\contentsline {section}{\numberline {2.3}Kane-Mele model}{35}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Band structure}{35}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Spin Chern number}{36}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Spin Bott index}{36}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Notation}{37}{subsection.2.3.4}%
\contentsline {chapter}{\numberline {3}Lattice of sub-wavelength resonators}{39}{chapter.3}%
\contentsline {section}{\numberline {3.1}One atom in the vacuum}{39}{section.3.1}%
\contentsline {section}{\numberline {3.2}$N$ atoms in the vacuum}{43}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Maxwell equations}{43}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Wave equation in Fourier space}{44}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Green propagator}{44}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Effective Hamiltonian for $N$ atoms}{45}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Atom in a Fabry-P\'{e}rot cavity}{46}{section.3.3}%
\contentsline {chapter}{\numberline {4}The honeycomb lattice between plates of metal}{49}{chapter.4}%
\contentsline {section}{\numberline {4.1}Introduction}{50}{section.4.1}%
\contentsline {section}{\numberline {4.2}Honeycomb atomic lattice in the free space}{51}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}The model}{51}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Band diagram of a honeycomb lattice in the free space}{52}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Width of the band gap}{54}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Topological properties of the band structure}{55}{subsection.4.2.4}%
\contentsline {section}{\numberline {4.3}Honeycomb atomic lattice in a Fabry-P\'{e}rot cavity}{58}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Green's function in a Fabry-P\'{e}rot cavity}{59}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Band diagram of a honeycomb lattice in a Fabry-P\'{e}rot cavity}{60}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Topological properties of the band structure}{62}{subsection.4.3.3}%
\contentsline {section}{\numberline {4.4}Conclusion}{63}{section.4.4}%
\contentsline {chapter}{\numberline {5}Toward a Photonic Topological Anderson Insulator}{65}{chapter.5}%
\contentsline {section}{\numberline {5.1}Our system: A two-dimensional honeycomb atomic lattice}{66}{section.5.1}%
\contentsline {section}{\numberline {5.2}Density of states and band gaps}{67}{section.5.2}%
\contentsline {section}{\numberline {5.3}Computing the Bott index: a topological marker}{68}{section.5.3}%
\contentsline {section}{\numberline {5.4}Introducing disorder }{70}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Disorder as a topological killer}{71}{subsection.5.4.1}%
\contentsline {section}{\numberline {5.5}The emergence of a topological Anderson insulator}{73}{section.5.5}%
\contentsline {chapter}{\numberline {6}Topology in a tight-binding model without TRS breaking}{79}{chapter.6}%
\contentsline {section}{\numberline {6.1}The kekule texture model}{79}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Description of the model}{79}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Band diagram and density of states}{80}{subsection.6.1.2}%
\contentsline {section}{\numberline {6.2}Hints of topology}{81}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Inversion of bands}{81}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Studying edge states on a nanoribbon}{82}{subsection.6.2.2}%
\contentsline {section}{\numberline {6.3}Computing topological invariants}{85}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}The spin Chern number}{85}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}The spin Bott index}{85}{subsection.6.3.2}%
\contentsline {section}{\numberline {6.4}Introducing disorder ?}{89}{section.6.4}%
\contentsline {section}{\numberline {6.5}Link with experimental work}{89}{section.6.5}%
\contentsline {chapter}{\numberline {7}Light in a deformed honeycomb atomic lattice}{91}{chapter.7}%
\contentsline {section}{\numberline {7.1}Angular-momentum eigenstates in a system of six atoms}{91}{section.7.1}%
\contentsline {section}{\numberline {7.2}Establishing the band diagram}{93}{section.7.2}%
\contentsline {subsection}{\numberline {7.2.1}Fourier Transform of the Hamiltonian}{93}{subsection.7.2.1}%
\contentsline {subsection}{\numberline {7.2.2}Projecting the bands on the spdf basis}{95}{subsection.7.2.2}%
\contentsline {section}{\numberline {7.3}Topological behaviours}{97}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}Spin Chern number for light in a deformed honeycomb lattice}{97}{subsection.7.3.1}%
\contentsline {subsection}{\numberline {7.3.2}Influence of \ $k_0 a$ on Gap Properties}{98}{subsection.7.3.2}%
\contentsline {subsection}{\numberline {7.3.3}Observing the edge states}{98}{subsection.7.3.3}%
\contentsline {section}{\numberline {7.4}Introducing disorder}{99}{section.7.4}%
\contentsline {chapter}{Appendices}{101}{chapter*.80}%
\contentsline {section}{\numberline {A}Vector Bundles}{101}{section.7.1}%
\contentsline {section}{\numberline {B}Derivation of the formula for the width of the spectral gap}{101}{section.7.2}%
