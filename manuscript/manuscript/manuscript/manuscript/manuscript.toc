\babel@toc {british}{}
\contentsline {chapter}{\numberline {1}Topology in physics}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Topology in a nutshell}{4}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Counting holes}{4}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Curvature, from local to global}{6}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}The link with physics}{6}{subsection.1.1.3}%
\contentsline {section}{\numberline {1.2}Topology: from QHE to classical waves}{7}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Quantum Hall effect}{7}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Mechanical waves}{8}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Acoustic waves}{9}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Equatorial waves}{10}{subsection.1.2.4}%
\contentsline {subsubsection}{Aharonov-Bohm effect}{10}{subsection.1.2.4}%
\contentsline {subsubsection}{Coriolis force}{10}{figure.caption.6}%
\contentsline {subsubsection}{Kelvin waves}{12}{equation.1.2.9}%
\contentsline {section}{\numberline {1.3}Topology in Photonic}{13}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Genesis of photonic topology}{13}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}The surprising topological anderson insulator}{15}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Topological photonic fibers}{16}{subsection.1.3.3}%
\contentsline {subsection}{\numberline {1.3.4}Topological laser}{17}{subsection.1.3.4}%
\contentsline {section}{\numberline {1.4}Summary}{17}{section.1.4}%
\contentsline {chapter}{\numberline {2}Introducing tools and models for topological phase study }{19}{chapter.2}%
\contentsline {section}{\numberline {2.1}Graphene}{21}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Analysing Graphene's Brillouin zone}{21}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Graphene nanoribbon}{23}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Haldane's model}{25}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Brillouin zone}{25}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Nanoribbon}{27}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Chern number}{28}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Real space}{29}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Bott index}{31}{subsection.2.2.5}%
\contentsline {subsubsection}{Bott index of two unitary matrices}{31}{subsection.2.2.5}%
\contentsline {subsubsection}{Bott index of two invertible matrix}{32}{equation.2.2.48}%
\contentsline {subsubsection}{Vanishing of Bott index}{32}{equation.2.2.51}%
\contentsline {subsubsection}{Bott index for the physicist}{33}{equation.2.2.53}%
\contentsline {subsubsection}{Bott Index on Haldane's model}{34}{equation.2.2.62}%
\contentsline {section}{\numberline {2.3}Kane-Mele model}{35}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Band structure}{35}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Spin Chern number}{36}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Spin Bott index}{36}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Notation}{37}{subsection.2.3.4}%
\contentsline {chapter}{\numberline {3}The honeycomb lattice between plates of metal}{39}{chapter.3}%
\contentsline {section}{\numberline {3.1}Introduction}{39}{section.3.1}%
\contentsline {section}{\numberline {3.2}Honeycomb atomic lattice in the free space}{40}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}The model}{40}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Band diagram of a honeycomb lattice in the free space}{41}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Width of the band gap}{44}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Topological properties of the band structure}{45}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Honeycomb atomic lattice in a Fabry-P\'{e}rot cavity}{47}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Green's function in a Fabry-P\'{e}rot cavity}{48}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Atom in a Fabry-P\'{e}rot cavity}{49}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Band diagram of a honeycomb lattice in a Fabry-P\'{e}rot cavity}{51}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Topological properties of the band structure}{52}{subsection.3.3.4}%
\contentsline {section}{\numberline {3.4}Conclusion}{53}{section.3.4}%
\contentsline {chapter}{\numberline {4}Toward a Photonic Topological Anderson Insulator}{55}{chapter.4}%
\contentsline {section}{\numberline {4.1}Our system: A two-dimensional honeycomb atomic lattice}{55}{section.4.1}%
\contentsline {section}{\numberline {4.2}Density of states and band gaps}{56}{section.4.2}%
\contentsline {section}{\numberline {4.3}Computing the Bott index: a topological marker}{57}{section.4.3}%
\contentsline {section}{\numberline {4.4}Introducing disorder }{59}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Disorder as a topological killer}{59}{subsection.4.4.1}%
\contentsline {section}{\numberline {4.5}The emergence of a topological Anderson insulator}{61}{section.4.5}%
\contentsline {chapter}{\numberline {5}Topology in a tight-binding model without TRS breaking}{67}{chapter.5}%
\contentsline {section}{\numberline {5.1}The kekule texture model}{67}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Description of the model}{67}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Band diagram and density of states}{68}{subsection.5.1.2}%
\contentsline {section}{\numberline {5.2}Hints of topology}{69}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Inversion of bands}{69}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Studying edge states on a nanoribbon}{70}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Computing topological invariants}{71}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}The spin Chern number}{71}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}The spin Bott index}{72}{subsection.5.3.2}%
\contentsline {section}{\numberline {5.4}Introducing disorder ?}{75}{section.5.4}%
\contentsline {section}{\numberline {5.5}Link with experimental work}{75}{section.5.5}%
\contentsline {chapter}{\numberline {6}Light in a deformed honeycomb atomic lattice}{79}{chapter.6}%
\contentsline {section}{\numberline {6.1}Establishing the band diagram}{79}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Fourier Transform of the Hamiltonian}{79}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Projecting the bands on the spdf basis}{80}{subsection.6.1.2}%
\contentsline {section}{\numberline {6.2}Topological behaviours}{81}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Spin Chern number for light in a deformed honeycomb lattice}{81}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Influence of \ $k_0 a$ on Gap Properties}{82}{subsection.6.2.2}%
\contentsline {subsection}{\numberline {6.2.3}Observing the edge states}{82}{subsection.6.2.3}%
\contentsline {section}{\numberline {6.3}Introducing disorder}{83}{section.6.3}%
\contentsline {chapter}{Appendices}{87}{chapter*.73}%
\contentsline {section}{\numberline {A}Vector Bundles}{87}{section.6.1}%
\contentsline {section}{\numberline {B}Derivation of the formula for the width of the spectral gap}{87}{section.6.2}%
