\BOOKMARK [0][-]{chapter.1}{Topology in physics}{}% 1
\BOOKMARK [1][-]{section.1.1}{Topology in a nutshell}{chapter.1}% 2
\BOOKMARK [2][-]{subsection.1.1.1}{Counting holes}{section.1.1}% 3
\BOOKMARK [2][-]{subsection.1.1.2}{Curvature, from local to global}{section.1.1}% 4
\BOOKMARK [2][-]{subsection.1.1.3}{The link with physics}{section.1.1}% 5
\BOOKMARK [1][-]{section.1.2}{Topology: from QHE to classical waves}{chapter.1}% 6
\BOOKMARK [2][-]{subsection.1.2.1}{Quantum Hall effect}{section.1.2}% 7
\BOOKMARK [2][-]{subsection.1.2.2}{Mechanical waves}{section.1.2}% 8
\BOOKMARK [2][-]{subsection.1.2.3}{Acoustic waves}{section.1.2}% 9
\BOOKMARK [2][-]{subsection.1.2.4}{Equatorial waves}{section.1.2}% 10
\BOOKMARK [1][-]{section.1.3}{Topology in Photonic}{chapter.1}% 11
\BOOKMARK [2][-]{subsection.1.3.1}{Genesis of photonic topology}{section.1.3}% 12
\BOOKMARK [2][-]{subsection.1.3.2}{The surprising topological anderson insulator}{section.1.3}% 13
\BOOKMARK [2][-]{subsection.1.3.3}{Topological photonic fibers}{section.1.3}% 14
\BOOKMARK [2][-]{subsection.1.3.4}{Topological laser}{section.1.3}% 15
\BOOKMARK [1][-]{section.1.4}{Summary}{chapter.1}% 16
\BOOKMARK [0][-]{chapter.2}{Introducing tools and models for topological phase study }{}% 17
\BOOKMARK [1][-]{section.2.1}{Graphene}{chapter.2}% 18
\BOOKMARK [2][-]{subsection.2.1.1}{Analysing Graphene's Brillouin zone}{section.2.1}% 19
\BOOKMARK [2][-]{subsection.2.1.2}{Graphene nanoribbon}{section.2.1}% 20
\BOOKMARK [1][-]{section.2.2}{Haldane's model}{chapter.2}% 21
\BOOKMARK [2][-]{subsection.2.2.1}{Brillouin zone}{section.2.2}% 22
\BOOKMARK [2][-]{subsection.2.2.2}{Nanoribbon}{section.2.2}% 23
\BOOKMARK [2][-]{subsection.2.2.3}{Chern number}{section.2.2}% 24
\BOOKMARK [2][-]{subsection.2.2.4}{Real space}{section.2.2}% 25
\BOOKMARK [2][-]{subsection.2.2.5}{Bott index}{section.2.2}% 26
\BOOKMARK [1][-]{section.2.3}{Kane-Mele model}{chapter.2}% 27
\BOOKMARK [2][-]{subsection.2.3.1}{Band structure}{section.2.3}% 28
\BOOKMARK [2][-]{subsection.2.3.2}{Spin Chern number}{section.2.3}% 29
\BOOKMARK [2][-]{subsection.2.3.3}{Spin Bott index}{section.2.3}% 30
\BOOKMARK [2][-]{subsection.2.3.4}{Notation}{section.2.3}% 31
\BOOKMARK [0][-]{chapter.3}{The honeycomb lattice between plates of metal}{}% 32
\BOOKMARK [1][-]{section.3.1}{Introduction}{chapter.3}% 33
\BOOKMARK [1][-]{section.3.2}{Honeycomb atomic lattice in the free space}{chapter.3}% 34
\BOOKMARK [2][-]{subsection.3.2.1}{The model}{section.3.2}% 35
\BOOKMARK [2][-]{subsection.3.2.2}{Band diagram of a honeycomb lattice in the free space}{section.3.2}% 36
\BOOKMARK [2][-]{subsection.3.2.3}{Width of the band gap}{section.3.2}% 37
\BOOKMARK [2][-]{subsection.3.2.4}{Topological properties of the band structure}{section.3.2}% 38
\BOOKMARK [1][-]{section.3.3}{Honeycomb atomic lattice in a Fabry-P\351rot cavity}{chapter.3}% 39
\BOOKMARK [2][-]{subsection.3.3.1}{Green's function in a Fabry-P\351rot cavity}{section.3.3}% 40
\BOOKMARK [2][-]{subsection.3.3.2}{Atom in a Fabry-P\351rot cavity}{section.3.3}% 41
\BOOKMARK [2][-]{subsection.3.3.3}{Band diagram of a honeycomb lattice in a Fabry-P\351rot cavity}{section.3.3}% 42
\BOOKMARK [2][-]{subsection.3.3.4}{Topological properties of the band structure}{section.3.3}% 43
\BOOKMARK [1][-]{section.3.4}{Conclusion}{chapter.3}% 44
\BOOKMARK [0][-]{chapter.4}{Toward a Photonic Topological Anderson Insulator}{}% 45
\BOOKMARK [1][-]{section.4.1}{Our system: A two-dimensional honeycomb atomic lattice}{chapter.4}% 46
\BOOKMARK [1][-]{section.4.2}{Density of states and band gaps}{chapter.4}% 47
\BOOKMARK [1][-]{section.4.3}{Computing the Bott index: a topological marker}{chapter.4}% 48
\BOOKMARK [1][-]{section.4.4}{Introducing disorder }{chapter.4}% 49
\BOOKMARK [2][-]{subsection.4.4.1}{Disorder as a topological killer}{section.4.4}% 50
\BOOKMARK [1][-]{section.4.5}{The emergence of a topological Anderson insulator}{chapter.4}% 51
\BOOKMARK [0][-]{chapter.5}{Topology in a tight-binding model without TRS breaking}{}% 52
\BOOKMARK [1][-]{section.5.1}{The kekule texture model}{chapter.5}% 53
\BOOKMARK [2][-]{subsection.5.1.1}{Description of the model}{section.5.1}% 54
\BOOKMARK [2][-]{subsection.5.1.2}{Band diagram and density of states}{section.5.1}% 55
\BOOKMARK [1][-]{section.5.2}{Hints of topology}{chapter.5}% 56
\BOOKMARK [2][-]{subsection.5.2.1}{Inversion of bands}{section.5.2}% 57
\BOOKMARK [2][-]{subsection.5.2.2}{Studying edge states on a nanoribbon}{section.5.2}% 58
\BOOKMARK [1][-]{section.5.3}{Computing topological invariants}{chapter.5}% 59
\BOOKMARK [2][-]{subsection.5.3.1}{The spin Chern number}{section.5.3}% 60
\BOOKMARK [2][-]{subsection.5.3.2}{The spin Bott index}{section.5.3}% 61
\BOOKMARK [1][-]{section.5.4}{Introducing disorder ?}{chapter.5}% 62
\BOOKMARK [1][-]{section.5.5}{Link with experimental work}{chapter.5}% 63
\BOOKMARK [0][-]{chapter.6}{Light in a deformed honeycomb atomic lattice}{}% 64
\BOOKMARK [1][-]{section.6.1}{Establishing the band diagram}{chapter.6}% 65
\BOOKMARK [2][-]{subsection.6.1.1}{Fourier Transform of the Hamiltonian}{section.6.1}% 66
\BOOKMARK [2][-]{subsection.6.1.2}{Projecting the bands on the spdf basis}{section.6.1}% 67
\BOOKMARK [1][-]{section.6.2}{Topological behaviours}{chapter.6}% 68
\BOOKMARK [2][-]{subsection.6.2.1}{Spin Chern number for light in a deformed honeycomb lattice}{section.6.2}% 69
\BOOKMARK [2][-]{subsection.6.2.2}{Influence of \040k0 a on Gap Properties}{section.6.2}% 70
\BOOKMARK [2][-]{subsection.6.2.3}{Observing the edge states}{section.6.2}% 71
\BOOKMARK [1][-]{section.6.3}{Introducing disorder}{chapter.6}% 72
\BOOKMARK [0][-]{chapter*.73}{Appendices}{}% 73
\BOOKMARK [1][-]{section.6.1}{Vector Bundles}{chapter*.73}% 74
\BOOKMARK [1][-]{section.6.2}{Derivation of the formula for the width of the spectral gap}{chapter*.73}% 75
