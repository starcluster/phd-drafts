\babel@toc {british}{}\relax 
\contentsline {chapter}{\numberline {1}Topology in physics}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Topology in a nutshell}{4}{section.1.1}%
\contentsline {section}{\numberline {1.2}Topology in classical wave mechanics}{7}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Mechanical waves}{7}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Acoustic waves}{7}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Equatorial waves}{8}{subsection.1.2.3}%
\contentsline {subsubsection}{Aharonov-Bohm effect}{8}{subsection.1.2.3}%
\contentsline {subsubsection}{Coriolis force}{9}{figure.caption.5}%
\contentsline {subsubsection}{Kelvin waves}{10}{equation.1.2.9}%
\contentsline {section}{\numberline {1.3}Topology in Photonic}{11}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Genesis of photonic topology}{11}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}The surprising topological anderson insulator}{11}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Topological photonic fibers}{11}{subsection.1.3.3}%
\contentsline {subsection}{\numberline {1.3.4}Topological laser}{12}{subsection.1.3.4}%
\contentsline {subsection}{\numberline {1.3.5}Summary}{13}{subsection.1.3.5}%
\contentsline {chapter}{\numberline {2}Introducing tools for topological phase study }{15}{chapter.2}%
\contentsline {section}{\numberline {2.1}Graphene}{17}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Analysing Graphene's Brillouin zone}{17}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Graphene nanoribbon}{19}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Haldane's model}{21}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Brillouin zone}{21}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Nanoribbon}{23}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Chern number}{24}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Real space}{25}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Bott index}{27}{subsection.2.2.5}%
\contentsline {subsubsection}{Bott index of two unitary matrices}{27}{subsection.2.2.5}%
\contentsline {subsubsection}{Bott index of two invertible matrix}{28}{equation.2.2.48}%
\contentsline {subsubsection}{Vanishing of Bott index}{28}{equation.2.2.51}%
\contentsline {subsubsection}{Bott index for the physicist}{29}{equation.2.2.53}%
\contentsline {subsubsection}{Bott Index on Haldane's model}{30}{equation.2.2.62}%
\contentsline {section}{\numberline {2.3}Kane-Mele model}{31}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Band structure}{31}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Spin Chern number}{32}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Spin Bott index}{32}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Notation}{33}{subsection.2.3.4}%
\contentsline {chapter}{\numberline {3}The honeycomb lattice between plates of metal}{35}{chapter.3}%
\contentsline {section}{\numberline {3.1}Introduction}{35}{section.3.1}%
\contentsline {section}{\numberline {3.2}Honeycomb atomic lattice in the free space}{36}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}The model}{36}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Band diagram of a honeycomb lattice in the free space}{37}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Width of the band gap}{40}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Topological properties of the band structure}{41}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Honeycomb atomic lattice in a Fabry-P\'{e}rot cavity}{43}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Green's function in a Fabry-P\'{e}rot cavity}{44}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Atom in a Fabry-P\'{e}rot cavity}{45}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Band diagram of a honeycomb lattice in a Fabry-P\'{e}rot cavity}{46}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Topological properties of the band structure}{47}{subsection.3.3.4}%
\contentsline {section}{\numberline {3.4}Conclusion}{48}{section.3.4}%
\contentsline {chapter}{Appendices}{51}{chapter*.39}%
\contentsline {section}{\numberline {A}Fiber Bundles}{51}{section.3.1}%
\contentsline {section}{\numberline {B}Derivation of the formula for the width of the spectral gap}{51}{section.3.2}%
