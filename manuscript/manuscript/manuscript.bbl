\begin{thebibliography}{}

\bibitem[Agarwal, 1995]{agarwal95}
Agarwal, G., editor (1995).
\newblock {\em Selected Papers on Resonant and Collective Phenomena in Quantum
  Optics}.
\newblock SPIE Optical Engineering Press, New York, NY.

\bibitem[Agarwal, 2013]{agarwal13}
Agarwal, G., editor (2013).
\newblock {\em Quantum Optics}.
\newblock Cambridge University Press, New York, NY.

\bibitem[Aharonov and Bohm, 1959]{Aharonov1959Aug}
Aharonov, Y. and Bohm, D. (1959).
\newblock {Significance of Electromagnetic Potentials in the Quantum Theory}.
\newblock {\em Phys. Rev.}, 115(3):485--491.
\newblock \url{https://doi.org/10.1103/PhysRev.115.485}.

\bibitem[Anderson, 1958]{Anderson1958Mar}
Anderson, P.~W. (1958).
\newblock {Absence of Diffusion in Certain Random Lattices}.
\newblock {\em Phys. Rev.}, 109(5):1492--1505.
\newblock \url{https://doi.org/10.1103/PhysRev.109.1492}.

\bibitem[Anderson et~al., 2020]{anderson20}
Anderson, R.~P., Trypogeorgos, D., Vald\'es-Curiel, A., Liang, Q.-Y., Tao, J.,
  Zhao, M., Andrijauskas, T., Juzeli\ifmmode~\bar{u}\else \={u}\fi{}nas, G.,
  and Spielman, I.~B. (2020).
\newblock Realization of a deeply subwavelength adiabatic optical lattice.
\newblock {\em Phys. Rev. Research}, 2:013149.
\newblock \url{https://doi.org/10.1103/PhysRevResearch.2.013149}.

\bibitem[Antezza and Castin, 2009]{antezza09pra}
Antezza, M. and Castin, Y. (2009).
\newblock Fano-{H}opfield model and photonic band gaps for an arbitrary atomic
  lattice.
\newblock {\em Phys. Rev. A}, 80:013816.
\newblock \url{https://doi.org/10.1103/PhysRevA.80.013816}.

\bibitem[Antezza and Castin, 2013]{antezza13}
Antezza, M. and Castin, Y. (2013).
\newblock Photonic band gap in an imperfect atomic diamond lattice: Penetration
  depth and effects of finite size and vacancies.
\newblock {\em Phys. Rev. A}, 88:033844.
\newblock \url{https://doi.org/10.1103/PhysRevA.88.033844}.

\bibitem[Bandres et~al., 2018]{Bandres2018Mar}
Bandres, M.~A., Wittek, S., Harari, G., Parto, M., Ren, J., Segev, M.,
  Christodoulides, D.~N., and Khajavikhan, M. (2018).
\newblock {Topological insulator laser: Experiments}.
\newblock {\em Science}, 359(6381).
\newblock \url{https://doi.org/10.1126/science.aar4005}.

\bibitem[Barbuto et~al., 2020]{Barbuto}
Barbuto, M., Miri, M.~A., Al{\ifmmode\grave{u}\else\`{u}\fi}, A., Bilotti, F.,
  and Toscano, A. (2020).
\newblock {Topological Phenomena in Antenna Systems}.
\newblock In {\em {2020 Fourteenth International Congress on Artificial
  Materials for Novel Wave Phenomena (Metamaterials)}}, pages 2020--03. IEEE.
\newblock \url{https://doi.org/10.1109/Metamaterials49557.2020.9284970}.

\bibitem[Bernevig et~al., 2006]{Bernevig2006Dec}
Bernevig, B.~A., Hughes, T.~L., and Zhang, S.-C. (2006).
\newblock {Quantum Spin Hall Effect and Topological Phase Transition in HgTe
  Quantum Wells}.
\newblock {\em Science}, 314(5806):1757--1761.
\newblock \url{https://doi.org/10.1126/science.1133734}.

\bibitem[Berry et~al., 1980]{Berry1980Jul}
Berry, M.~V., Chambers, R.~G., Large, M.~D., Upstill, C., and Walmsley, J.~C.
  (1980).
\newblock {Wavefront dislocations in the Aharonov-Bohm effect and its water
  wave analogue}.
\newblock {\em Eur. J. Phys.}, 1(3):154.
\newblock \url{https://doi.org/10.1088/0143-0807/1/3/008}.

\bibitem[Bettles et~al., 2017]{bettles17}
Bettles, R.~J., Min\'a\ifmmode~\check{r}\else \v{r}\fi{}, J. c.~v., Adams,
  C.~S., Lesanovsky, I., and Olmos, B. (2017).
\newblock Topological properties of a dense atomic lattice gas.
\newblock {\em Phys. Rev. A}, 96:041603.
\newblock \url{https://doi.org/10.1103/PhysRevA.96.041603}.

\bibitem[Bongiovanni et~al., 2021]{Bongiovanni2021Oct}
Bongiovanni, D., Juki{\ifmmode\acute{c}\else\'{c}\fi}, D., Hu, Z.,
  Luni{\ifmmode\acute{c}\else\'{c}\fi}, F., Hu, Y., Song, D., Morandotti, R.,
  Chen, Z., and Buljan, H. (2021).
\newblock {Dynamically Emerging Topological Phase Transitions in Nonlinear
  Interacting Soliton Lattices}.
\newblock {\em Phys. Rev. Lett.}, 127(18):184101.
\newblock \url{https://doi.org/10.1103/PhysRevLett.127.184101}.

\bibitem[Bradlyn and Iraola, 2022]{Bradlyn2022May}
Bradlyn, B. and Iraola, M. (2022).
\newblock {Lecture notes on Berry phases and topology}.
\newblock {\em SciPost Phys. Lect. Notes}, page 051.
\newblock \url{https://doi.org/10.21468/SciPostPhysLectNotes.51}.

\bibitem[Carusotto and Ciuti, 2013]{Carusotto2013Feb}
Carusotto, I. and Ciuti, C. (2013).
\newblock {Quantum fluids of light}.
\newblock {\em Rev. Mod. Phys.}, 85(1):299--366.
\newblock \url{https://doi.org/10.1103/RevModPhys.85.299}.

\bibitem[Chambers, 1960]{Chambers1960Jul}
Chambers, R.~G. (1960).
\newblock {Shift of an Electron Interference Pattern by Enclosed Magnetic
  Flux}.
\newblock {\em Phys. Rev. Lett.}, 5(1):3--5.
\newblock \url{https://doi.org/10.1103/PhysRevLett.5.3}.

\bibitem[Chang et~al., 2013]{Chang2013Apr}
Chang, C.-Z., Zhang, J., Feng, X., Shen, J., Zhang, Z., Guo, M., Li, K., Ou,
  Y., Wei, P., Wang, L.-L., Ji, Z.-Q., Feng, Y., Ji, S., Chen, X., Jia, J.,
  Dai, X., Fang, Z., Zhang, S.-C., He, K., Wang, Y., Lu, L., Ma, X.-C., and
  Xue, Q.-K. (2013).
\newblock {Experimental Observation of the Quantum Anomalous Hall Effect in a
  Magnetic Topological Insulator}.
\newblock {\em Science}, 340(6129):167--170.
\newblock \url{https://doi.org/10.1126/science.1234414}.

\bibitem[Choi, 2023]{Choi2023Mar}
Choi, C.~Q. (2023).
\newblock {Topological Photonics: What It Is and Why We Need It}.
\newblock {\em IEEE Spectr.}
\newblock
  \url{https://spectrum.ieee.org/topological-photonics-what-it-is-why-we-need-it}.

\bibitem[Cohen-Tannoudji et~al., 1998]{cohen98}
Cohen-Tannoudji, C., Jacques Dupont-Roc, J., and Grynberg, G. (1998).
\newblock {\em Atom-Photon Interactions: Basic Processes and Applications}.
\newblock John Wiley \& Sons, New York, NY.

\bibitem[Dalibard, 2018]{Dalibard2018}
Dalibard, J. (2018).
\newblock {xn--College-266c de France lecture 2018: Topological matter explored
  with quantum xn--gases-dz3b}.
\newblock \url{https://pro.college-de-france.fr/jean.dalibard/index_en.html}.

\bibitem[Delplace and Venaille, 2020]{Delplace2020}
Delplace, P. and Venaille, A. (2020).
\newblock {From the geometry of Foucault pendulum to the topology of planetary
  waves}.
\newblock {\em C. R. Phys.}, 21(2):165--175.
\newblock \url{https://doi.org/10.5802/crphys.28}.

\bibitem[Dowling, 1993]{dowling93}
Dowling, J.~P. (1993).
\newblock Spontaneous emission in cavities: How much more classical can you
  get?
\newblock {\em Found. Phys.}, 23(6):895--905.
\newblock \url{https://doi.org/10.1007/BF01891512}.

\bibitem[Faure, 2023]{Faure2023}
Faure, F. (2023).
\newblock {Manifestation of the topological index formula in quantum waves and
  geophysical waves}.
\newblock {\em Annales Henri Lebesgue}, 6:449--492.
\newblock \url{https://doi.org/10.5802/ahl.169}.

\bibitem[Fleury et~al., 2014]{Fleury2014Jan}
Fleury, R., Sounas, D.~L., Sieck, C.~F., Haberman, M.~R., and
  Al{\ifmmode\grave{u}\else\`{u}\fi}, A. (2014).
\newblock {Sound Isolation and Giant Linear Nonreciprocity in a Compact
  Acoustic Circulator}.
\newblock {\em Science}, 343(6170):516--519.
\newblock \url{https://doi.org/10.1126/science.1246957}.

\bibitem[Fukui et~al., 2005a]{Fukui2005Jun}
Fukui, T., Hatsugai, Y., and Suzuki, H. (2005a).
\newblock {Chern Numbers in Discretized Brillouin Zone: Efficient Method of
  Computing (Spin) Hall Conductances}.
\newblock {\em J. Phys. Soc. Jpn.}, 74(6):1674--1677.
\newblock \url{https://doi.org/10.1143/JPSJ.74.1674}.

\bibitem[Fukui et~al., 2005b]{fukui05}
Fukui, T., Hatsugai, Y., and Suzuki, H. (2005b).
\newblock {Chern Numbers in Discretized Brillouin Zone: Efficient Method of
  Computing (Spin) Hall Conductances}.
\newblock {\em J. Phys. Soc. Jpn.}, 74(6):1674--1677.
\newblock \url{https://doi.org/10.1143/JPSJ.74.1674}.

\bibitem[Gross and Haroche, 1982]{gross82}
Gross, M. and Haroche, S. (1982).
\newblock Superradiance: An essay on the theory of collective spontaneous
  emission.
\newblock {\em Physics Reports}, 93(5):301--396.
\newblock \url{https://doi.org/https://doi.org/10.1016/0370-1573(82)90102-8}.

\bibitem[Groth et~al., 2009]{Groth2009Nov}
Groth, C.~W., Wimmer, M., Akhmerov, A.~R., Tworzyd{\l}o, J., and Beenakker, C.
  W.~J. (2009).
\newblock {Theory of the Topological Anderson Insulator}.
\newblock {\em Phys. Rev. Lett.}, 103(19):196805.
\newblock \url{https://doi.org/10.1103/PhysRevLett.103.196805}.

\bibitem[Haldane, 1988]{Haldane1988Oct}
Haldane, F. D.~M. (1988).
\newblock {Model for a Quantum Hall Effect without Landau Levels:
  Condensed-Matter Realization of the "Parity Anomaly"}.
\newblock {\em Phys. Rev. Lett.}, 61(18):2015--2018.
\newblock \url{https://doi.org/10.1103/PhysRevLett.61.2015}.

\bibitem[Haldane and Raghu, 2008]{Haldane2008Jan}
Haldane, F. D.~M. and Raghu, S. (2008).
\newblock {Possible Realization of Directional Optical Waveguides in Photonic
  Crystals with Broken Time-Reversal Symmetry}.
\newblock {\em Phys. Rev. Lett.}, 100(1):013904.
\newblock \url{https://doi.org/10.1103/PhysRevLett.100.013904}.

\bibitem[Harari et~al., 2018]{Harari2018Mar}
Harari, G., Bandres, M.~A., Lumer, Y., Rechtsman, M.~C., Chong, Y.~D.,
  Khajavikhan, M., Christodoulides, D.~N., and Segev, M. (2018).
\newblock {Topological insulator laser: Theory}.
\newblock {\em Science}, 359(6381).
\newblock \url{https://doi.org/10.1126/science.aar4003}.

\bibitem[Haroche and Kleppner, 1989]{haroche89}
Haroche, S. and Kleppner, D. (1989).
\newblock Cavity quantum electrodynamics.
\newblock {\em Physics Today}, 42(1):24--30.
\newblock \url{https://doi.org/10.1063/1.881201}.

\bibitem[Hatcher, 2017]{VBbook}
Hatcher, A. (2017).
\newblock {Vector Bundles {\&} K-Theory Book}.
\newblock \url{https://pi.math.cornell.edu/~hatcher/VBKT/VBpage.html}.

\bibitem[Hu et~al., 2008]{Hu2008Dec}
Hu, H., Strybulevych, A., Page, J.~H., Skipetrov, S.~E., and van Tiggelen,
  B.~A. (2008).
\newblock {Localization of ultrasound in a three-dimensional elastic network}.
\newblock {\em Nat. Phys.}, 4:945--948.
\newblock \url{https://doi.org/10.1038/nphys1101}.

\bibitem[Huang and Liu, 2018]{Huang2018Sep}
Huang, H. and Liu, F. (2018).
\newblock {Theory of spin Bott index for quantum spin Hall states in
  nonperiodic systems}.
\newblock {\em Phys. Rev. B}, 98(12):125130.
\newblock \url{https://doi.org/10.1103/PhysRevB.98.125130}.

\bibitem[Hulet et~al., 1985]{hulet85}
Hulet, R.~G., Hilfer, E.~S., and Kleppner, D. (1985).
\newblock Inhibited spontaneous emission by a {R}ydberg atom.
\newblock {\em Phys. Rev. Lett.}, 55:2137--2140.
\newblock \url{https://doi.org/10.1103/PhysRevLett.55.2137}.

\bibitem[Jalali~Mehrabad et~al., 2023]{JalaliMehrabad2023Oct}
Jalali~Mehrabad, M., Mittal, S., and Hafezi, M. (2023).
\newblock {Topological photonics: Fundamental concepts, recent developments,
  and future directions}.
\newblock {\em Phys. Rev. A}, 108(4):040101.
\newblock \url{https://doi.org/10.1103/PhysRevA.108.040101}.

\bibitem[Jhe et~al., 1987]{jhe87}
Jhe, W., Anderson, A., Hinds, E.~A., Meschede, D., Moi, L., and Haroche, S.
  (1987).
\newblock Suppression of spontaneous decay at optical frequencies: Test of
  vacuum-field anisotropy in confined space.
\newblock {\em Phys. Rev. Lett.}, 58:666--669.
\newblock \url{https://doi.org/10.1103/PhysRevLett.58.666}.

\bibitem[Joannopoulos et~al., 2008]{joan08}
Joannopoulos, J., Johnson, S., Winn, J., and Meade, R. (2008).
\newblock {\em Photonic Crystals: Molding the Flow of Light}.
\newblock Princeton University Press, 2 edition.

\bibitem[Kane and Mele, 2005]{Kane2005Sep}
Kane, C.~L. and Mele, E.~J. (2005).
\newblock {${Z}_{2}$ Topological Order and the Quantum Spin Hall Effect}.
\newblock {\em Phys. Rev. Lett.}, 95(14):146802.
\newblock \url{https://doi.org/10.1103/PhysRevLett.95.146802}.

\bibitem[Kawabata et~al., 2019]{Kawabata2019Oct}
Kawabata, K., Shiozaki, K., Ueda, M., and Sato, M. (2019).
\newblock {Symmetry and Topology in Non-Hermitian Physics}.
\newblock {\em Phys. Rev. X}, 9(4):041015.
\newblock \url{https://doi.org/10.1103/PhysRevX.9.041015}.

\bibitem[Khanikaev and Shvets, 2017]{khanikaev17}
Khanikaev, A.~B. and Shvets, G. (2017).
\newblock Two-dimensional topological photonics.
\newblock {\em Nature Photonics}, 11:763--773.
\newblock \url{https://doi.org/10.1038/s41566-017-0048-5}.

\bibitem[Kittel, 2004]{Kittel2004Nov}
Kittel, C. (2004).
\newblock {\em {Introduction to Solid State Physics, 8th Edition}}.
\newblock Wiley, Hoboken, NJ, USA.

\bibitem[Klitzing et~al., 1980]{Klitzing1980Aug}
Klitzing, K.~v., Dorda, G., and Pepper, M. (1980).
\newblock {New Method for High-Accuracy Determination of the Fine-Structure
  Constant Based on Quantized Hall Resistance}.
\newblock {\em Phys. Rev. Lett.}, 45(6):494--497.
\newblock \url{https://doi.org/10.1103/PhysRevLett.45.494}.

\bibitem[Koshino et~al., 2002]{Koshino2002Aug}
Koshino, M., Aoki, H., and Halperin, B.~I. (2002).
\newblock {Wrapping current versus bulk integer quantum Hall effect in three
  dimensions}.
\newblock {\em Phys. Rev. B}, 66(8):081301.
\newblock \url{https://doi.org/10.1103/PhysRevB.66.081301}.

\bibitem[Lagendijk et~al., 2009]{Lagendijk2009Aug}
Lagendijk, A., Tiggelen, B.~v., and Wiersma, D.~S. (2009).
\newblock {Fifty years of Anderson localization}.
\newblock {\em Phys. Today}, 62(8):24--29.
\newblock \url{https://doi.org/10.1063/1.3206091}.

\bibitem[Li et~al., 2009]{Li2009Apr}
Li, J., Chu, R.-L., Jain, J.~K., and Shen, S.-Q. (2009).
\newblock {Topological Anderson Insulator}.
\newblock {\em Phys. Rev. Lett.}, 102(13):136806.
\newblock \url{https://doi.org/10.1103/PhysRevLett.102.136806}.

\bibitem[Liu et~al., 2017]{Liu2017Nov}
Liu, C., Gao, W., Yang, B., and Zhang, S. (2017).
\newblock {Disorder-Induced Topological State Transition in Photonic
  Metamaterials}.
\newblock {\em Phys. Rev. Lett.}, 119(18):183901.
\newblock \url{https://doi.org/10.1103/PhysRevLett.119.183901}.

\bibitem[Liu et~al., 2020]{Liu2020Sep}
Liu, G.-G., Yang, Y., Ren, X., Xue, H., Lin, X., Hu, Y.-H., Sun, H.-x., Peng,
  B., Zhou, P., Chong, Y., and Zhang, B. (2020).
\newblock {Topological Anderson Insulator in Disordered Photonic Crystals}.
\newblock {\em Phys. Rev. Lett.}, 125(13):133603.
\newblock \url{https://doi.org/10.1103/PhysRevLett.125.133603}.

\bibitem[Loring, 2019]{Loring2019Jul}
Loring, T.~A. (2019).
\newblock {A Guide to the Bott Index and Localizer Index}.
\newblock {\em arXiv}.
\newblock \url{https://doi.org/10.48550/arXiv.1907.11791}.

\bibitem[Loring and Hastings, 2011]{Loring2011Jan}
Loring, T.~A. and Hastings, M.~B. (2011).
\newblock {Disordered topological insulators via C{$\ast$}-algebras}.
\newblock {\em Europhys. Lett.}, 92(6):67004.
\newblock \url{https://doi.org/10.1209/0295-5075/92/67004}.

\bibitem[Matsuno, 1966]{Matsuno1966}
Matsuno, T. (1966).
\newblock {Quasi-Geostrophic Motions in the Equatorial Area}.
\newblock {\em Journal of the Meteorological Society of Japan. Ser. II},
  44(1):25--43.
\newblock \url{https://doi.org/10.2151/jmsj1965.44.1_25}.

\bibitem[Maxworthy, 1983]{Maxworthy1983Apr}
Maxworthy, T. (1983).
\newblock {Experiments on solitary internal Kelvin waves}.
\newblock {\em J. Fluid Mech.}, 129:365--383.
\newblock \url{https://doi.org/10.1017/S0022112083000816}.

\bibitem[Meier et~al., 2018]{Meier2018Nov}
Meier, E.~J., An, F.~A., Dauphin, A., Maffei, M., Massignan, P., Hughes, T.~L.,
  and Gadway, B. (2018).
\newblock {Observation of the topological Anderson insulator in disordered
  atomic wires}.
\newblock {\em Science}, 362(6417):929--933.
\newblock \url{https://doi.org/10.1126/science.aat3406}.

\bibitem[Mili\ifmmode \acute{c}\else \'{c}\fi{}evi\ifmmode~\acute{c}\else
  \'{c}\fi{} et~al., 2017]{mili17}
Mili\ifmmode \acute{c}\else \'{c}\fi{}evi\ifmmode~\acute{c}\else \'{c}\fi{},
  M., Ozawa, T., Montambaux, G., Carusotto, I., Galopin, E., Lema\^{\i}tre, A.,
  Le~Gratiet, L., Sagnes, I., Bloch, J., and Amo, A. (2017).
\newblock Orbital edge states in a photonic honeycomb lattice.
\newblock {\em Phys. Rev. Lett.}, 118:107403.
\newblock \url{https://doi.org/10.1103/PhysRevLett.118.107403}.

\bibitem[Milonni and Knight, 1973]{milonni73}
Milonni, P. and Knight, P. (1973).
\newblock Spontaneous emission between mirrors.
\newblock {\em Optics Communications}, 9(2):119--122.
\newblock \url{https://doi.org/https://doi.org/10.1016/0030-4018(73)90239-3}.

\bibitem[Nakahara, 2003]{Nakahara2003Jun}
Nakahara, M. (2003).
\newblock {\em {Geometry, Topology and Physics, Second Edition (Graduate
  Student Series in Physics)}}.
\newblock CRC Press, Boca Raton, FL, USA.
\newblock \url{https://doi.org/10.1201/9781315275826}.

\bibitem[Nascimbene et~al., 2015]{nas15}
Nascimbene, S., Goldman, N., Cooper, N.~R., and Dalibard, J. (2015).
\newblock Dynamic optical lattices of subwavelength spacing for ultracold
  atoms.
\newblock {\em Phys. Rev. Lett.}, 115:140401.
\newblock \url{https://doi.org/10.1103/PhysRevLett.115.140401}.

\bibitem[Nash et~al., 2015]{Nash2015Nov}
Nash, L.~M., Kleckner, D., Read, A., Vitelli, V., Turner, A.~M., and Irvine, W.
  T.~M. (2015).
\newblock {Topological mechanics of gyroscopic metamaterials}.
\newblock {\em Proc. Natl. Acad. Sci. U.S.A.}, 112(47):14495--14500.
\newblock \url{https://doi.org/10.1073/pnas.1507413112}.

\bibitem[Ni et~al., 2023]{ni23}
Ni, X., Yves, S., Krasnok, A., and Al\`{u}, A. (2023).
\newblock Topological metamaterials.
\newblock {\em Chemical Reviews}, 123:7585--7654.
\newblock \url{https://doi.org/https://doi.org/10.1021/acs.chemrev.2c00800}.

\bibitem[Novoselov et~al., 2004]{Novoselov2004Oct}
Novoselov, K.~S., Geim, A.~K., Morozov, S.~V., Jiang, D., Zhang, Y., Dubonos,
  S.~V., Grigorieva, I.~V., and Firsov, A.~A. (2004).
\newblock {Electric Field Effect in Atomically Thin Carbon Films}.
\newblock {\em Science}, 306(5696):666--669.
\newblock \url{https://doi.org/10.1126/science.1102896}.

\bibitem[Oh, 2013]{Oh2013Apr}
Oh, S. (2013).
\newblock {The Complete Quantum Hall Trio}.
\newblock {\em Science}, 340(6129):153--154.
\newblock \url{https://doi.org/10.1126/science.1237215}.

\bibitem[Olmos et~al., 2013]{olmos13}
Olmos, B., Yu, D., Singh, Y., Schreck, F., Bongs, K., and Lesanovsky, I.
  (2013).
\newblock Long-range interacting many-body systems with alkaline-earth-metal
  atoms.
\newblock {\em Phys. Rev. Lett.}, 110:143602.
\newblock \url{https://doi.org/10.1103/PhysRevLett.110.143602}.

\bibitem[Ozawa et~al., 2019a]{Ozawa2019Mar}
Ozawa, T., Price, H.~M., Amo, A., Goldman, N., Hafezi, M., Lu, L., Rechtsman,
  M.~C., Schuster, D., Simon, J., Zilberberg, O., and Carusotto, I. (2019a).
\newblock {Topological photonics}.
\newblock {\em Rev. Mod. Phys.}, 91(1):015006.
\newblock \url{https://doi.org/10.1103/RevModPhys.91.015006}.

\bibitem[Ozawa et~al., 2019b]{ozawa19}
Ozawa, T., Price, H.~M., Amo, A., Goldman, N., Hafezi, M., Lu, L., Rechtsman,
  M.~C., Schuster, D., Simon, J., Zilberberg, O., and Carusotto, I. (2019b).
\newblock Topological photonics.
\newblock {\em Rev. Mod. Phys.}, 91:015006.
\newblock \url{https://doi.org/10.1103/RevModPhys.91.015006}.

\bibitem[Parappurath et~al., 2020]{parap20}
Parappurath, N., Alpeggiani, F., Kuipers, L., and Verhagen, E. (2020).
\newblock Direct observation of topological edge states in silicon photonic
  crystals: Spin, dispersion, and chiral routing.
\newblock {\em Science Advances}, 6(10):eaaw4137.
\newblock \url{https://doi.org/10.1126/sciadv.aaw4137}.

\bibitem[Perczel et~al., 2017a]{perczel17pra}
Perczel, J., Borregaard, J., Chang, D.~E., Pichler, H., Yelin, S.~F., Zoller,
  P., and Lukin, M.~D. (2017a).
\newblock Photonic band structure of two-dimensional atomic lattices.
\newblock {\em Phys. Rev. A}, 96:063801.
\newblock \url{https://doi.org/10.1103/PhysRevA.96.063801}.

\bibitem[Perczel et~al., 2017b]{perczel17}
Perczel, J., Borregaard, J., Chang, D.~E., Pichler, H., Yelin, S.~F., Zoller,
  P., and Lukin, M.~D. (2017b).
\newblock Topological quantum optics in two-dimensional atomic arrays.
\newblock {\em Phys. Rev. Lett.}, 119:023603.
\newblock \url{https://doi.org/10.1103/PhysRevLett.119.023603}.

\bibitem[Polini et~al., 2013]{polini13}
Polini, M., Guinea, F., Lewenstein, M., Manoharan, H.~C., and Pellegrini, V.
  (2013).
\newblock Artificial honeycomb lattices for electrons, atoms and photons.
\newblock {\em Nature Nanotechnology}, 9:625--633.
\newblock \url{https://doi.org/10.1038/nnano.2013.161}.

\bibitem[Qi and Zhang, 2010]{Qi2010Jan}
Qi, X.-L. and Zhang, S.-C. (2010).
\newblock {The quantum spin Hall effect and topological insulators}.
\newblock {\em Phys. Today}, 63(1):33--38.
\newblock \url{https://doi.org/10.1063/1.3293411}.

\bibitem[Raghu and Haldane, 2008]{Raghu2008Sep}
Raghu, S. and Haldane, F. D.~M. (2008).
\newblock {Analogs of quantum-Hall-effect edge states in photonic crystals}.
\newblock {\em Phys. Rev. A}, 78(3):033834.
\newblock \url{https://doi.org/10.1103/PhysRevA.78.033834}.

\bibitem[Reisner et~al., 2021a]{reisner21}
Reisner, M., Bellec, M., Kuhl, U., and Mortessagne, F. (2021a).
\newblock Microwave resonator lattices for topological photonics (invited).
\newblock {\em Opt. Mater. Express}, 11(3):629--653.
\newblock \url{https://doi.org/10.1364/OME.416835}.

\bibitem[Reisner et~al., 2021b]{Reisner2021Mar}
Reisner, M., Bellec, M., Kuhl, U., and Mortessagne, F. (2021b).
\newblock {Microwave resonator lattices for topological photonics [Invited]}.
\newblock {\em Opt. Mater. Express, OME}, 11(3):629--653.

\bibitem[Roberts et~al., 2022]{Roberts2022Dec}
Roberts, N., Baardink, G., Nunn, J., Mosley, P.~J., and Souslov, A. (2022).
\newblock {Topological supermodes in photonic crystal fiber}.
\newblock {\em Sci. Adv.}, 8(51).
\newblock \url{https://doi.org/10.1126/sciadv.add3522}.

\bibitem[Röhlsberger et~al., 2010]{rohl10}
Röhlsberger, R., Schlage, K., Sahoo, B., Couet, S., and Rüffer, R. (2010).
\newblock Collective {L}amb shift in single-photon superradiance.
\newblock {\em Science}, 328(5983):1248--1251.
\newblock \url{https://doi.org/10.1126/science.1187770}.

\bibitem[Scully, 2009]{scully09}
Scully, M.~O. (2009).
\newblock Collective {L}amb shift in single photon {D}icke superradiance.
\newblock {\em Phys. Rev. Lett.}, 102:143601.
\newblock \url{https://doi.org/10.1103/PhysRevLett.102.143601}.

\bibitem[Shalaev et~al., 2019]{Shalaev2019Jan}
Shalaev, M.~I., Walasik, W., Tsukernik, A., Xu, Y., and Litchinitser, N.~M.
  (2019).
\newblock {Robust topologically protected transport in photonic crystals at
  telecommunication wavelengths}.
\newblock {\em Nat. Nanotechnol.}, 14:31--34.
\newblock \url{https://doi.org/10.1038/s41565-018-0297-6}.

\bibitem[Shen et~al., 2018]{Shen2018Apr}
Shen, H., Zhen, B., and Fu, L. (2018).
\newblock {Topological Band Theory for Non-Hermitian Hamiltonians}.
\newblock {\em Phys. Rev. Lett.}, 120(14):146402.
\newblock \url{https://doi.org/10.1103/PhysRevLett.120.146402}.

\bibitem[Sheng et~al., 2006]{Sheng2006Jul}
Sheng, D.~N., Weng, Z.~Y., Sheng, L., and Haldane, F. D.~M. (2006).
\newblock {Quantum Spin-Hall Effect and Topologically Invariant Chern Numbers}.
\newblock {\em Phys. Rev. Lett.}, 97(3):036808.
\newblock \url{https://doi.org/10.1103/PhysRevLett.97.036808}.

\bibitem[Skipetrov, 2020a]{skip20epj}
Skipetrov, S.~E. (2020a).
\newblock Finite-size scaling of the density of states inside band gaps of
  ideal and disordered photonic crystals.
\newblock {\em Eur. Phys. J. B}, 93:70.
\newblock \url{https://doi.org/10.1140/epjb/e2020-100473-3}.

\bibitem[Skipetrov, 2020b]{skip20prb}
Skipetrov, S.~E. (2020b).
\newblock Localization of light in a three-dimensional disordered crystal of
  atoms.
\newblock {\em Phys. Rev. B}, 102:134206.
\newblock \url{https://doi.org/10.1103/PhysRevB.102.134206}.

\bibitem[Skipetrov and Sokolov, 2015]{skip15}
Skipetrov, S.~E. and Sokolov, I.~M. (2015).
\newblock Magnetic-field-driven localization of light in a cold-atom gas.
\newblock {\em Phys. Rev. Lett.}, 114:053902.
\newblock \url{https://doi.org/10.1103/PhysRevLett.114.053902}.

\bibitem[Skipetrov and Wulles, 2022a]{nous}
Skipetrov, S.~E. and Wulles, P. (2022a).
\newblock {Topological transitions and {A}nderson localization of light in
  disordered atomic arrays}.
\newblock {\em Phys. Rev. A}, 105(4):043514.
\newblock \url{https://doi.org/10.1103/PhysRevA.105.043514}.

\bibitem[Skipetrov and Wulles, 2022b]{Skipetrov2022Apr}
Skipetrov, S.~E. and Wulles, P. (2022b).
\newblock {Topological transitions and Anderson localization of light in
  disordered atomic arrays}.
\newblock {\em Phys. Rev. A}, 105(4):043514.

\bibitem[Skipetrov and Wulles, 2023a]{skip23}
Skipetrov, S.~E. and Wulles, P. (2023a).
\newblock Photonic topological {A}nderson insulator in a two-dimensional atomic
  lattice.
\newblock {\em Comptes Rendus. Physique}.
\newblock \url{https://doi.org/10.5802/crphys.147}.

\bibitem[Skipetrov and Wulles, 2023b]{Skipetrov2023}
Skipetrov, S.~E. and Wulles, P. (2023b).
\newblock {Photonic topological Anderson insulator in a two-dimensional atomic
  lattice}.
\newblock {\em C. R. Phys.}, 24(S3):1--16.

\bibitem[St{\ifmmode\ddot{u}\else\"{u}\fi}tzer et~al., 2018]{Stutzer2018Aug}
St{\ifmmode\ddot{u}\else\"{u}\fi}tzer, S., Plotnik, Y., Lumer, Y., Titum, P.,
  Lindner, N.~H., Segev, M., Rechtsman, M.~C., and Szameit, A. (2018).
\newblock {Photonic topological Anderson insulators}.
\newblock {\em Nature}, 560:461--465.
\newblock \url{https://doi.org/10.1038/s41586-018-0418-2}.

\bibitem[Suddards et~al., 2012]{Suddards2012Aug}
Suddards, M.~E., Baumgartner, A., Henini, M., and Mellor, C.~J. (2012).
\newblock {Scanning capacitance imaging of compressible and incompressible
  quantum Hall effect edge strips}.
\newblock {\em New J. Phys.}, 14(8):083015.
\newblock \url{https://doi.org/10.1088/1367-2630/14/8/083015}.

\bibitem[S{\ifmmode\ddot{u}\else\"{u}\fi}sstrunk and Huber,
  2015]{Susstrunk2015Jul}
S{\ifmmode\ddot{u}\else\"{u}\fi}sstrunk, R. and Huber, S.~D. (2015).
\newblock {Observation of phononic helical edge states in a mechanical
  topological insulator}.
\newblock {\em Science}, 349(6243):47--50.
\newblock \url{https://doi.org/10.1126/science.aab0239}.

\bibitem[Tan et~al., 2014]{Tan2014Jan}
Tan, W., Sun, Y., Chen, H., and Shen, S.-Q. (2014).
\newblock {Photonic simulation of topological excitations in metamaterials}.
\newblock {\em Sci. Rep.}, 4(3842):1--7.
\newblock \url{https://doi.org/10.1038/srep03842}.

\bibitem[Tang et~al., 2022]{tang22}
Tang, G.-J., He, X.-T., Shi, F.-L., Liu, J.-W., Chen, X.-D., and Dong, J.-W.
  (2022).
\newblock Topological photonic crystals: Physics, designs, and applications.
\newblock {\em Laser \& Photonics Reviews}, 16(4):2100300.
\newblock \url{https://doi.org/https://doi.org/10.1002/lpor.202100300}.

\bibitem[Taylor, 1922]{Taylor1922Jan}
Taylor, G.~I. (1922).
\newblock {Tidal Oscillations in Gulfs and Rectangular Basins}.
\newblock {\em Proc. London Math. Soc.}, s2-20(1):148--181.
\newblock \url{https://doi.org/10.1112/plms/s2-20.1.148}.

\bibitem[Toniolo, 2022]{Toniolo2022Dec}
Toniolo, D. (2022).
\newblock {On the Bott index of unitary matrices on a finite torus}.
\newblock {\em Lett. Math. Phys.}, 112(6):1--24.
\newblock \url{https://doi.org/10.1007/s11005-022-01602-6}.

\bibitem[Tonomura, 2003]{Tonomura2003Jul}
Tonomura, A. (2003).
\newblock {Direct Observation of the Microscopic World by Using Phase Shifts of
  Electron Waves}.
\newblock In {\em {A Garden of Quanta}}, pages 7--21. WORLD SCIENTIFIC,
  Singapore.
\newblock \url{https://doi.org/10.1142/9789812795106_0002}.

\bibitem[Van~Tiggelen, 1999]{VanTiggelen1999}
Van~Tiggelen, B.~A. (1999).
\newblock {Localization of Waves}.
\newblock In {\em {Diffuse Waves in Complex Media}}, pages 1--60. Springer,
  Dordrecht, The Netherlands.
\newblock \url{https://doi.org/10.1007/978-94-011-4572-5_1}.

\bibitem[Wallace, 1947a]{wallace71}
Wallace, P.~R. (1947a).
\newblock The band theory of graphite.
\newblock {\em Phys. Rev.}, 71:622--634.
\newblock \url{https://doi.org/10.1103/PhysRev.71.622}.

\bibitem[Wallace, 1947b]{Wallace1947May}
Wallace, P.~R. (1947b).
\newblock {The Band Theory of Graphite}.
\newblock {\em Phys. Rev.}, 71(9):622--634.
\newblock \url{https://doi.org/10.1103/PhysRev.71.622}.

\bibitem[Wang et~al., 2009]{Wang2009Oct}
Wang, Z., Chong, Y., Joannopoulos, J.~D., and
  Solja{\ifmmode\check{c}\else\v{c}\fi}i{\ifmmode\acute{c}\else\'{c}\fi}, M.
  (2009).
\newblock {Observation of unidirectional backscattering-immune topological
  electromagnetic states}.
\newblock {\em Nature}, 461:772--775.
\newblock \url{https://doi.org/10.1038/nature08293}.

\bibitem[Wang et~al., 2008]{Wang2008Jan}
Wang, Z., Chong, Y.~D., Joannopoulos, J.~D., and
  Solja{\ifmmode\check{c}\else\v{c}\fi}i{\ifmmode\acute{c}\else\'{c}\fi}, M.
  (2008).
\newblock {Reflection-Free One-Way Edge Modes in a Gyromagnetic Photonic
  Crystal}.
\newblock {\em Phys. Rev. Lett.}, 100(1):013905.
\newblock \url{https://doi.org/10.1103/PhysRevLett.100.013905}.

\bibitem[Wu and Hu, 2016]{Wu2016Apr}
Wu, L.-H. and Hu, X. (2016).
\newblock {Topological Properties of Electrons in Honeycomb Lattice with
  Detuned Hopping Energy}.
\newblock {\em Sci. Rep.}, 6(24347):1--9.

\bibitem[Yamilov et~al., 2023]{Yamilov2023Sep}
Yamilov, A., Skipetrov, S.~E., Hughes, T.~W., Minkov, M., Yu, Z., and Cao, H.
  (2023).
\newblock {Anderson localization of electromagnetic waves in three dimensions}.
\newblock {\em Nat. Phys.}, 19:1308--1313.
\newblock \url{https://doi.org/10.1038/s41567-023-02091-7}.

\bibitem[Yves et~al., 2017]{Yves2017Nov}
Yves, S., Lemoult, F., Fink, M., and Lerosey, G. (2017).
\newblock {Crystalline Soda Can Metamaterial exhibiting Graphene-like
  Dispersion at subwavelength scale}.
\newblock {\em Sci. Rep.}, 7(15359):1--7.
\newblock \url{https://doi.org/10.1038/s41598-017-15335-3}.

\bibitem[Zangeneh-Nejad et~al., 2020]{Zangeneh-Nejad2020}
Zangeneh-Nejad, F., Al{\ifmmode\grave{u}\else\`{u}\fi}, A., and Fleury, R.
  (2020).
\newblock {Topological wave insulators: a review}.
\newblock {\em C. R. Phys.}, 21(4-5):467--499.
\newblock \url{https://doi.org/10.5802/crphys.3}.

\bibitem[Zhang et~al., 2018]{Zhang2018Dec}
Zhang, X., Xiao, M., Cheng, Y., Lu, M.-H., and Christensen, J. (2018).
\newblock {Topological sound}.
\newblock {\em Commun. Phys.}, 1(97):1--13.
\newblock \url{https://doi.org/10.1038/s42005-018-0094-4}.

\end{thebibliography}
