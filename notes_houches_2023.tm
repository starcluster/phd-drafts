<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Multiple scattering in resonant media>>

  <section|General concepts>

  <\equation*>
    E<rsub|s>=E-E<rsub|0>
  </equation*>

  HE for the total field minus HE without the particle: HE for the scattered
  field <math|\<Rightarrow\>> with green formalism gives Lippmann Swchinger
  equation:

  <\equation*>
    E<around*|(|r,\<omega\>|)>=E<rsub|0><around*|(|r,\<omega\>|)>+<big|int><rsub|\<delta\>V>G<rsub|0><around*|(|r-r<rprime|'>,\<omega\>|)>k<rsub|0><rsup|2><around*|(|\<varepsilon\><around*|(|\<omega\>|)>-1|)>E<around*|(|r,\<omega\>|)>dr<rprime|'>\<Rightarrow\>E=E<rsub|0>+G<rsub|0>V*E
  </equation*>

  Born series:

  <\equation*>
    E=E<rsub|0>+G<rsub|0>V*E<rsub|0>+G<rsub|0>V*G<rsub|0>V*E<rsub|0>+\<cdots\>
  </equation*>

  Scattering matrix:

  <\equation*>
    T=V+V*G<rsub|0>V+\<cdots\>
  </equation*>

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
  </collection>
</references>