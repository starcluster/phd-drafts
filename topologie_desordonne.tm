<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Indices topologiques dans des milieux d�sordonn�s>>

  <section|Introduction>

  On dit d'un mat�riau qu'il est un isolant topologique si il se comporte
  comme un isolant dans le bulk et comme un conducteur sur les bords. Il
  s'agit d'une propri�t� intrins�que du mat�riau.

  <section|Nombre de Chern>

  <\definition>
    On note <math|P> le projecteur sur les niveaux occup�s:

    <\equation*>
      P=<frac|1|2\<pi\>i><big|oint><rsub|\<Omega\>><around*|(|z-H<rsub|0>|)><rsup|-1>\<mathd\>z
    </equation*>

    ou <math|\<Omega\>> est un contour qui entoure les niveaux occup�s.
  </definition>

  On peut d�composer <math|P> en une somme directe de projecteur (Th�or�me de
  Bloch)

  <\equation*>
    U*P*U<rsup|-1>=<big|oplus><rsub|k\<in\>\<bbb-T\>>P<rsub|k>
  </equation*>

  <\definition>
    Le nombre de Chern est d�fini comme:

    <\equation*>
      C=<frac|1|2\<pi\>i><big|int><rsub|\<bbb-T\>>Tr<around*|(|P<rsub|k><around*|[|\<partial\><rsub|k<rsub|1>>P<rsub|k>,\<partial\><rsub|k<rsub|2>>P<rsub|k>|]>|)>\<mathd\><rsup|2>k
    </equation*>
  </definition>

  <section|Nombre de spin-Chern>

  En introduisant un nombre de Chern associ� � la bande du dessus et � la
  bande du dessous:

  <\equation*>
    C<rsub|\<pm\>>=<frac|1|2\<pi\>i><big|int><rsub|\<bbb-T\>>Tr<around*|(|P<rsub|k><rsup|\<pm\>><around*|[|\<partial\><rsub|k<rsub|1>>P<rsub|k><rsup|\<pm\>>,\<partial\><rsub|k<rsub|2>>P<rsub|k><rsup|<rsub|\<pm\>>>|]>|)>\<mathd\><rsup|2>k
  </equation*>

  <\definition>
    On d�finit le nombre de spin-Chern:

    <\equation*>
      C<rsub|s>=<frac|1|2><around*|(|C<rsub|+>-C<rsub|->|)>
    </equation*>
  </definition>

  <\definition>
    Le noyau d'un op�rateur lin�aire <math|f> est d�fini par:
  </definition>

  <\equation*>
    Ker<around*|(|f|)>=<around*|{|x\<in\>H,f<around*|(|x|)>=0|}>
  </equation*>

  <\definition>
    Alors l'index de l'op�rateur <math|f> est d�fini comme:

    <\equation*>
      Ind<around*|(|f|)>=dim<around*|(|Ker<around*|(|f|)>|)>-dim<around*|(|Ker<around*|(|f*<rsup|\<ast\>>|)>|)>
    </equation*>
  </definition>

  <\definition>
    La classe de Fredholm contient tous les op�rateurs <math|f> born�s, i.e.
    pour lesquels <math|dim<around*|(|Ker<around*|(|f|)>|)>\<less\>+\<infty\>>,
    <math|dim<around*|(|Ker<around*|(|f<rsup|*\<ast\>>|)>|)>\<less\>+\<infty\>>,
    <math|f<around*|(|H|)>> et <math|f<rsup|\<ast\>><around*|(|H|)>> sont des
    espaces ferm�s.
  </definition>

  L'index n'est donc correctement d�fini que pour les op�rateurs appartenant
  � la classe de Fredholm.

  <\definition>
    La classe trace <math|S<rsup|1>> contient tous les op�rateurs compacts
    <math|g> tels que\ 

    <\equation*>
      <around*|\<\|\|\>|A|\<\|\|\>><rsub|S<rsup|1>>=<big|sum><rsub|i>\<mu\><rsub|i>\<less\>+\<infty\>
    </equation*>

    ou les <math|\<mu\><rsub|i>> sont les valeurs propres de
    <math|<sqrt|g*g<rsup|\<ast\>>>>. On d�finit la norme sur <math|S<rsup|1>>
  </definition>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Nombre
      de Chern> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Nombre
      de spin-Chern> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>