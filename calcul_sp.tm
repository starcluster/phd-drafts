<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Calcul de <math|\<Gamma\><rsub|sp>> entre des
  plaques>>

  D'apr�s la m�thode de Gunnar Bjork et Yoshihisa Yamamoto:

  On a:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|2\<pi\>|\<hbar\>><around*|\||<around*|\<langle\>||\|><math-bf|d>\<cdummy\><math-bf|E><rsup|\<dagger\>><rsub|<math-bf|k>,\<sigma\>><around*|\|||\<nobracket\>>|\<rangle\>><around*|\||<rsup|2>|\<nobracket\>>\<rho\><around*|(|\<omega\>|)>
  </equation*>

  O�:

  <\equation*>
    \<rho\><around*|(|\<omega\>|)>=<frac|\<omega\><rsup|2>V|c<rsub|0><rsup|3>\<pi\><rsup|2>>
  </equation*>

  Dans le vide:

  <\equation*>
    <around*|\||<around*|\<langle\>||\|><math-bf|d>\<cdummy\><math-bf|E><rsup|\<dagger\>><rsub|<math-bf|k>,\<sigma\>><around*|\|||\<nobracket\>>|\<rangle\>><around*|\||<rsup|2>|\<nobracket\>>=<frac|d<around*|(|\<omega\>|)><rsup|2>\<hbar\>\<omega\>|2\<varepsilon\><rsub|0>V>
  </equation*>

  Alors l'�mission angulaire pour un <math|y>-dipole peut s'�crire en
  polarisation S:

  <\equation*>
    \<gamma\><rsub|sp><around*|(|\<omega\>,\<theta\>|)>=<frac|2\<pi\>|\<hbar\><rsup|2>><frac|d<around*|(|\<omega\>|)><rsup|2>\<hbar\>\<omega\>|2\<varepsilon\><rsub|0>V><frac|\<omega\><rsup|2>V|4c<rsub|0><rsup|3>\<pi\><rsup|3>>cos<around*|(|\<varphi\>|)><rsup|2>=<frac|3\<Gamma\><rsub|0>cos<around*|(|\<varphi\>|)><rsup|2>|8\<pi\>>
  </equation*>

  En polarisation P:

  <\equation*>
    \<gamma\><rsub|sp><around*|(|\<omega\>,\<theta\>|)>=<frac|3\<Gamma\><rsub|0>cos<around*|(|\<theta\>|)><rsup|2>sin<around*|(|\<varphi\>|)><rsup|2>|8\<pi\>>
  </equation*>

  D'o� pour l'�mission totale dans le vide:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|8\<pi\>><big|int><rsub|0><rsup|2\<pi\>>\<mathd\>\<varphi\><big|int><rsub|0><rsup|\<pi\>>\<mathd\>\<theta\>sin<around*|(|\<theta\>|)><around*|(|cos<around*|(|\<theta\>|)><rsup|2>sin<around*|(|\<varphi\>|)><rsup|2>+cos<around*|(|\<varphi\>|)><rsup|2>|)>=\<Gamma\><rsub|0>
  </equation*>

  Consid�rons maintenant le cas d'une cavit� plane avec des coefficients de
  r�flexions aux plaques <math|R<rsub|1>> et <math|R<rsub|2>> pour les ondes
  S:

  <\equation*>
    E<rsub|\<parallel\>><rsup|2>=<around*|\||E<rsub|+>-E<rsub|->|\|><rsup|2>=<frac|<around*|(|1-R<rsub|2>|)><around*|(|1+R<rsub|1>-2<sqrt|R<rsub|1>>cos<around*|(|2k<rsub|>*z<rsub|0>cos<around*|(|\<theta\>|)>|)>|)>|<around*|(|1-<sqrt|R<rsub|1>R<rsub|2>>|)><rsup|2>+4<sqrt|R<rsub|1>R<rsub|2>>sin<around*|(|k*Lcos<around*|(|\<theta\>|)>|)><rsup|2>><around*|\||E<rsub|0>|\|><rsup|2>
  </equation*>

  Et:

  <\equation*>
    E<rsub|z><rsup|2>=0
  </equation*>

  Pour les ondes P:

  <\equation*>
    E<rsub|\<parallel\>><rsup|2>=<around*|\||E<rsub|+>-E<rsub|->|\|><rsup|2>cos<around*|(|\<theta\>|)><rsup|2>=<frac|<around*|(|1-R<rsub|2>|)><around*|(|1+R<rsub|1>-2<sqrt|R<rsub|1>>cos<around*|(|2k<rsub|>*z<rsub|0>cos<around*|(|\<theta\>|)>|)>|)>|<around*|(|1-<sqrt|R<rsub|1>R<rsub|2>>|)><rsup|2>+4<sqrt|R<rsub|1>R<rsub|2>>sin<around*|(|k*Lcos<around*|(|\<theta\>|)>|)><rsup|2>>cos<around*|(|\<theta\>|)><rsup|2><around*|\||E<rsub|0>|\|><rsup|2>
  </equation*>

  Et:

  <\equation*>
    E<rsub|z><rsup|2>=<around*|\||E<rsub|+>-E<rsub|->|\|><rsup|2>sin<around*|(|\<theta\>|)><rsup|2>=<frac|<around*|(|1-R<rsub|2>|)><around*|(|1+R<rsub|1>+2<sqrt|R<rsub|1>>cos<around*|(|2k<rsub|>*z<rsub|0>cos<around*|(|\<theta\>|)>|)>|)>|<around*|(|1-<sqrt|R<rsub|1>R<rsub|2>>|)><rsup|2>+4<sqrt|R<rsub|1>R<rsub|2>>sin<around*|(|k*Lcos<around*|(|\<theta\>|)>|)><rsup|2>>sin<around*|(|\<theta\>|)><rsup|2><around*|\||E<rsub|0>|\|><rsup|2>
  </equation*>

  On simplifie en supposant <math|R<rsub|1>=R<rsub|2>> et on suppose
  <math|z<rsub|0>=L/2>

  Donc:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|8\<pi\>><big|int><rsub|0><rsup|+\<infty\>>\<mathd\>\<omega\><big|int><rsub|0><rsup|2\<pi\>>\<mathd\>\<varphi\><big|int><rsub|0><rsup|\<pi\>>\<mathd\>\<theta\>sin<around*|(|\<theta\>|)>\<times\>
  </equation*>

  <\equation*>
    <around*|(|cos<around*|(|\<theta\>|)><rsup|2>sin<around*|(|\<varphi\>|)><rsup|2>+cos<around*|(|\<varphi\>|)><rsup|2>|)><frac|<around*|(|1-R<rsub|>|)><around*|(|1+R<rsub|>-2<sqrt|R<rsub|>>cos<around*|(|2k<rsub|>*z<rsub|0>cos<around*|(|\<theta\>|)>|)>|)>|<around*|(|1-R|)><rsup|2>+4R*sin<around*|(|k*L*cos<around*|(|\<theta\>|)>|)><rsup|2>>\<delta\><around*|(|\<omega\>-2\<pi\>c/\<lambda\><rsub|0>|)>
  </equation*>

  Qui vaut:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|4><big|int><rsub|0><rsup|\<pi\>/2>\<mathd\>\<theta\>sin<around*|(|\<theta\>|)><frac|<around*|(|1-R<rsub|>|)><around*|(|1+R<rsub|>-2<sqrt|R<rsub|>>cos<around*|(|k<rsub|><rsub|0>L*cos<around*|(|\<theta\>|)>|)>|)>|<around*|(|1-R|)><rsup|2>+4R*sin<around*|(|k<rsub|0>L*cos<around*|(|\<theta\>|)>|)><rsup|2>><around*|(|1+cos<around*|(|\<theta\>|)><rsup|2>|)>
  </equation*>

  On pose <math|cos<around*|(|\<theta\>|)>=t/k<rsub|0>L>, donc:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|4><big|int><rsub|0><rsup|k<rsub|0>L><frac|<around*|(|1-R<rsub|>|)><around*|(|1+R<rsub|>-2<sqrt|R<rsub|>>cos<around*|(|t|)>|)>|<around*|(|1-R|)><rsup|2>+4R*sin<around*|(|t|)><rsup|2>><around*|(|1+<around*|(|<frac|t|k<rsub|0>L>|)><rsup|2>|)>\<mathd\>t
  </equation*>

  Dans la limite <math|R\<rightarrow\>1:>

  <\equation*>
    <frac|<around*|(|1-R<rsub|>|)><around*|(|1+R<rsub|>-2<sqrt|R<rsub|>>cos<around*|(|t|)>|)>|<around*|(|1-R|)><rsup|2>+4R*sin<around*|(|t|)><rsup|2>>=2\<pi\><big|sum><rsup|+\<infty\>><rsub|m=1>\<delta\><around*|(|t-<around*|(|2m-1|)>\<pi\>|)>
  </equation*>

  Donc:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|4><big|int><rsub|0><rsup|k<rsub|0>L>2\<pi\><big|sum><rsup|+\<infty\>><rsub|m=1>\<delta\><around*|(|t-<around*|(|2m-1|)>\<pi\>|)><around*|(|1+<around*|(|<frac|t|k<rsub|0>L>|)><rsup|2>|)>\<mathd\>t
  </equation*>

  Qui donne:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|2><frac|\<pi\>|k<rsub|0>L><big|sum><rsup|<around*|\<lceil\>|k<rsub|0>L|\<rceil\>>><rsub|m=1><frac|1-<around*|(|-1|)><rsup|m>|2><around*|(|1+<around*|(|<frac|m\<pi\>|k<rsub|0>L>|)><rsup|2>|)>
  </equation*>

  Si on ne suppose pas <math|z<rsub|0>=L/2> alors:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|4><big|int><rsub|0><rsup|\<pi\>/2>\<mathd\>\<theta\>sin<around*|(|\<theta\>|)><frac|<around*|(|1-R<rsub|>|)><around*|(|1+R<rsub|>-2<sqrt|R<rsub|>>cos<around*|(|2k<rsub|>*z<rsub|0>cos<around*|(|\<theta\>|)>|)>|)>|<around*|(|1-R|)><rsup|2>+4R*sin<around*|(|k<rsub|0>L*cos<around*|(|\<theta\>|)>|)><rsup|2>><around*|(|1+cos<around*|(|\<theta\>|)><rsup|2>|)>
  </equation*>

  Donc:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|4><big|int><rsub|0><rsup|k<rsub|0>L>2\<pi\><big|sum><rsup|+\<infty\>><rsub|m=1>\<delta\><around*|(|t-<around*|(|2m-1|)>\<pi\>|)><around*|(|2-2cos<around*|(|*2z<rsub|0>t/L|)>|)><around*|(|1+<around*|(|<frac|t|k<rsub|0>L>|)><rsup|2>|)>\<mathd\>t
  </equation*>

  Donne:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|4><frac|\<pi\>|k<rsub|0>L><big|sum><rsup|<around*|\<lceil\>|k<rsub|0>L|\<rceil\>>><rsub|m=1><around*|(|1-cos<around*|(|2\<pi\>m*z<rsub|0>/L|)>|)><around*|(|1+<around*|(|<frac|m\<pi\>|k<rsub|0>L>|)><rsup|2>|)>\<mathd\>t
  </equation*>

  Dans l'autre cas on a pour la somme des ondes P et S:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|4><big|int><rsub|0><rsup|\<pi\>/2>\<mathd\>\<theta\>sin<around*|(|\<theta\>|)><frac|<around*|(|1-R<rsub|>|)><around*|(|1+R<rsub|>+2<sqrt|R<rsub|>>cos<around*|(|k<rsub|><rsub|>z<rsub|0>cos<around*|(|\<theta\>|)>|)>|)>|<around*|(|1-R|)><rsup|2>+4R*sin<around*|(|k<rsub|0>L*cos<around*|(|\<theta\>|)>|)><rsup|2>>sin<around*|(|\<theta\>|)><rsup|2>
  </equation*>

  M�me proc�dure en remarquant que les seules diff�rences sont sont
  <math|1-cos<around*|(|k<rsub|><rsub|>z<rsub|0>cos<around*|(|\<theta\>|)>|)>>
  qui devient <math|1+cos<around*|(|k<rsub|><rsub|>z<rsub|0>cos<around*|(|\<theta\>|)>|)>>
  d'une part et d'autre part le <math|1+cos<around*|(|\<theta\>|)><rsup|2>>
  qui devient un <math|sin<around*|(|\<theta\>|)><rsup|2>=1-cos<around*|(|\<theta\>|)><rsup|2>>
  on obtiendra donc:

  <\equation*>
    \<Gamma\><rsub|sp>=<frac|3\<Gamma\><rsub|0>|2><frac|\<pi\>|k<rsub|0>L><around*|(|1+<big|sum><rsup|<around*|\<lceil\>|k<rsub|0>L|\<rceil\>>><rsub|m=1><around*|(|1+cos<around*|(|2\<pi\>m*z<rsub|0>/L|)>|)><around*|(|1-<around*|(|<frac|m\<pi\>|k<rsub|0>L>|)><rsup|2>|)>\<mathd\>t|)>
  </equation*>

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>