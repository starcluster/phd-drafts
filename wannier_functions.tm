<TeXmacs|2.1>

<style|<tuple|generic|british>>

<\body>
  <doc-data|<doc-title|Localized Wannier functions and Topology>>

  We consider a Hamiltonian <math|H> describing free fermions on a lattice in
  a tight-binding model on the surface of a sphere. <math|H> is hermitian and
  has a spectral gap from <math|-\<Delta\>E/2> to <math|\<Delta\>E/2>. We
  note <math|P> the projector spanned by the eigenvectors with eigenvalues
  less than <math|-\<Delta\>E/2> and we note <math|X>, <math|Y> and <math|Z>
  the position operators.

  <\definition>
    A set of exponentially localized Wannier functions with localization
    length <math|\<xi\>> is a set of orthonormals vectors,
    <math|<around*|{|v<rsub|a>|}><rsub|1\<leqslant\>a\<leqslant\>m>>,
    spanning the subspace onto wich <math|P> projects such that for each
    vector <math|v<rsub|a>> the following property holds.

    Let <math|x<rsub|a>=<around*|\<langle\>|v<rsub|a><around*|\|||\<nobracket\>>X*v<rsub|a>|\<rangle\>>>,
    <math|y<rsub|a>=<around*|\<langle\>|v<rsub|a><around*|\|||\<nobracket\>>Y*v<rsub|a>|\<rangle\>>>
    and <math|z<rsub|a>=<around*|\<langle\>|v<rsub|a><around*|\|||\<nobracket\>>Z*v<rsub|a>|\<rangle\>>>.

    <\equation>
      \<forall\>a\<in\><around*|{|1,\<ldots\>,m|}>,\<forall\>D\<geqslant\>0,<big|sum><rsub|i,dist<around*|(|i,<around*|(|x<rsub|a>,y<rsub|a>,z<rsub|a>|)>|)>\<geqslant\>D><around*|\||v<rsub|a><rsup|i>|\|>=\<delta\>\<leqslant\>exp<around*|(|-<frac|D|\<xi\>>|)><label|bound>
    </equation>
  </definition>

  Where the localization length <math|\<xi\>> depends of parameter of the
  Hamiltonian <math|H> such as the range of interactions between fermions,
  the probability of hoping or the width of the gap. We can conjecture
  something like <math|\<xi\>\<sim\><frac|t*R|\<Delta\>E>> where <math|t> is
  the hoping stength and <math|R> the average range of interaction.

  Then we can write a theorem:

  <\theorem>
    If an index shows that <math|H<rsub|1>=P*X*P>, <math|H<rsub|2>=P*Y*P> and
    <math|H<rsub|3>=P*Z*P> cannot be approximate by a set of commuting
    matrices then there is no set of exponentially localized Wannier function
    that spans the range of <math|P>. By contrapositive, if there is a set of
    exponentially localized Wannier function that spans the range of <math|P>
    then <math|H<rsub|r>> can be approximate by commuting matrices.
  </theorem>

  In the following I show numerical evidence of this result for a canonical
  model: the Haldane model on a torus. I consider two situations, a first one
  in which we assign an opposite energy to sites <math|A> and <math|B> and a
  second one in which we also add \ imaginary second-nearest neighbor
  hoppings with a precise pattern. In Fig. <reference|localization> it is
  clear that in the trivial case the quantity <math|\<delta\>> is bounded by
  an inverse exponential showing that these functions are indeed localized
  wannier functions. On the opposite in the topological case we do not
  observe theis bounding.

  <\big-figure|<image|/home/these/chapter-2-thesis/figs/wannier_5.pdf|0.5par|||><image|/home/these/chapter-2-thesis/figs/wannier_evolution.pdf|0.5par|||>>
    <label|localization>(Left) The quantity <math|\<delta\>> defined in
    <eqref|bound> is plotted for each eigenvalues below the gap. We observe
    that for a given <math|D>, most of these bound for the trivial case are
    below the ones for the topological case. (Right) Curves show the how
    <math|\<delta\>> is decresing when increasing <math|D>, the black curves
    illustrates the exponential localization of wannier function while the
    red curves show that in the topological case there is no such
    localization.
  </big-figure>

  Note: The Haldane model is on a Torus so we do not need the third matrix
  <math|H<rsub|3>=P*Z*P>.
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|bound|<tuple|1|?>>
    <associate|localization|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>