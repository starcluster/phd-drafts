import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from scipy.integrate import tplquad
from mpmath import polylog
from mpmath import exp

def re_delta_perp(d):
    N = 100
    s = 1/2
    for n in range(1, N):
        if n < d/np.pi:
            s += (1-n**2*np.pi**2/d**2)*np.cos(np.pi*n*1/d-np.pi*n/2)**2

    return 6*np.pi*s/d

def re_delta_parall(d):
    N = 100
    s = 0
    for n in range(1, N):
        if n < d/np.pi:
            s += (1+n**2*np.pi**2/d**2)*np.sin(np.pi*n/2)**2

    return 3*np.pi*s/d/2

def plot_f(f):
    ds = np.linspace(2,100,200)
    y = np.array([f(d) for d in ds])
    plt.plot(ds,y)

def integrand(x,y,z,L):
    k0 = 2*np.pi*0.05
    return (x**2+y**2)/np.sqrt(x**2+y**2+z**2)/(np.sqrt(x**2+y**2+z**2)-k0)*(1+np.exp(2*1j*z*(-1/2*L)))

def delta_sum_perp(L):
    R = 12
    s = 0
    N = 10
    k0 = 2*np.pi*0.05
    for n in range(-N,N+1):
        s += (1+np.exp(2*1j*np.pi*n/L*(0-1/2*L)))*(R**2/2+k0*np.sqrt(R**2+(n*np.pi/L)**2)-k0*n*np.pi/L - ((n*np.pi/L)**2-k0**2)*np.log(np.abs( (k0-n*np.pi/L)/(k0-np.sqrt((n*np.pi/L)**2+R**2)))))

    return np.imag(s)

def delta_sum_parall(L):
    R = 1
    s = 0
    N = 10
    k0 = 2*np.pi*0.05
    for n in range(-N,N+1):
        # print("log", np.log(np.abs((-k0+np.sqrt(R**2+(np.pi*n/L)**2))/(-k0+np.pi*n/L))))
        if n < L/np.pi:
            s += (1-np.exp(2*1j*np.pi*n/L*(0-1/2*L)))*np.pi*n/L*np.log(np.abs((-k0+np.sqrt(R**2+(np.pi*n/L)**2))/(-k0+np.pi*n/L)))

    return s

def delta_theoretical(x):
    return x**3/3+x**2/2+x+np.log(x-1)

def delta_theoretical_parall(L):
    s = 0
    N = 100
    R = 1000 # arbitraire
    b = 0.3
    for n in range(1, N):
        if n < L/np.pi:
            a = np.pi*n/L
            s += integrate.quad(lambda x: x*(x ** 2 + 2 * a ** 2)* np.sqrt(x ** 2 + a ** 2)/ (np.sqrt(x ** 2 + a ** 2) - b)/ (x ** 2 + a ** 2), 0, R)[0]
    # print(s)

    return s/L

def my_formula(d):
    return ((polylog(1, -exp(1j * d)) / d+ 1j * polylog(2, -exp(1j * d)) / d ** 2 - polylog(3, -exp(1j * d)) / d ** 3) + 1j/3)*3

def my_formula_real(d):
    return np.real(my_formula(d))

def my_formula_imag(d):
    return np.imag(my_formula(d))
    
if __name__ == "__main__":
    plot_f(re_delta_parall)
    plt.show()
    plot_f(my_formula_imag)
    plt.show()
    # plot_f(re_delta_parall)
    # plot_f(my_formula_imag)
    # plt.show()
    plot_f(delta_sum_parall)
    # plot_f(my_formula_real)
    plt.show()
    
    Ls = [i/10 for i in range(1,1000)]
    y = []
    y2 = []
    for L in Ls:
    #     y.append(np.real(tplquad(lambda x,y,z: integrand(x,y,z,L), 1, R, 1, R, 1, R)))
        y.append(np.real(delta_sum_parall(L)))
        y2.append(np.real(my_formula(L)))
    # UNITÉS ?    
    print(y)
    plt.plot(Ls,y)
    plt.plot(Ls,y2)
    plt.show()
    
                            
