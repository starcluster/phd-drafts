<TeXmacs|2.1>

<style|generic>

<\body>
  <\equation*>
    i\<hbar\><frac|\<mathd\>\<psi\>|\<mathd\>t>=H\<psi\>
  </equation*>

  On diag <math|H>, on obtient des �tats propres <math|\<varphi\>>, puis on
  suppose un �tat inital qui est situ� sur le bord, cet �tat s'�crit dans la
  base des �tats propres:

  <\equation*>
    \<psi\>=<big|sum><rsub|i>a<rsub|i>\<varphi\><rsub|i>
  </equation*>

  En r�injectant dans Schr�dinger:

  <\equation*>
    i\<hbar\><big|sum><rsub|i><frac|\<mathd\>a<rsub|i>|\<mathd\>t>\<varphi\><rsub|i>=<big|sum><rsub|i>\<lambda\><rsub|i>a<rsub|i>\<varphi\><rsub|i>
  </equation*>

  Donc par identification sur la base des �tats propres:

  <\equation*>
    a<rsub|i><around*|(|t|)>=exp<around*|(|<frac|-i\<lambda\><rsub|i>t|\<hbar\>>|)>a<rsub|i><around*|(|0|)>
  </equation*>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>