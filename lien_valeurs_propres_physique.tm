<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Lien valeurs propres <math|<wide|G|^>>/physique>>

  On a une matrice de la transition de la forme:

  <\equation*>
    \<alpha\><around*|(|\<omega\>|)>=B<frac|-\<Gamma\><rsub|0>/2|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2>
  </equation*>

  L'�quation d'�volution s'�crit:

  <\equation*>
    <around|\||u<around*|(|\<omega\>|)>|\<rangle\>>=<around|\||v<around*|(|\<omega\>|)>|\<rangle\>>+<frac|\<alpha\><around*|(|\<omega\>|)>|B><around*|[|G<around*|(|\<omega\>|)>-i<with|font|Bbb|1>|]><around|\||u<around*|(|\<omega\>|)>|\<rangle\>>
  </equation*>

  On note les �tats propres droits et gauches:

  <\equation*>
    <around|\<langle\>|L<rsub|m><around*|\||R<rsub|n>|\<nobracket\>>|\<rangle\>>=\<delta\><rsub|m*n>
  </equation*>

  On peut d�velopper <math|<around|\||u<around*|(|\<omega\>|)>|\<rangle\>>>
  sur la base des fonctions propres:

  <\equation*>
    <around|\||u<around*|(|\<omega\>|)>|\<rangle\>><long-arrow|\<rubber-equal\>|><big|sum><rsub|n>A<rsub|n><around|\<nobracket\>|<around*|\||R<rsub|n>|\<nobracket\>>|\<rangle\>>
  </equation*>

  Donc en r�injectant dans l'�quation:

  <\equation*>
    <big|sum><rsub|n>A<rsub|n><around|\<nobracket\>|<around*|\||R<rsub|n>|\<nobracket\>>|\<rangle\>>=<around|\||v<around*|(|\<omega\>|)>|\<rangle\>>+<frac|\<alpha\><around*|(|\<omega\>|)>|B><around*|[|G<around*|(|\<omega\>|)>-i<with|font|Bbb|1>|]><big|sum><rsub|n>A<rsub|n><around|\<nobracket\>|<around*|\||R<rsub|n>|\<nobracket\>>|\<rangle\>>
  </equation*>

  On multiplie par <math|<around*|\<langle\>|L<rsub|m>|\|>>:

  <\equation*>
    <big|sum><rsub|n>A<rsub|n><around*|\<langle\>|L<rsub|m>|\<nobracket\>><around|\<nobracket\>|<around*|\||R<rsub|n>|\<nobracket\>>|\<rangle\>>=<around*|\<langle\>|L<rsub|m>|\|><around|\<nobracket\>|v<around*|(|\<omega\>|)>|\<rangle\>>+<frac|\<alpha\><around*|(|\<omega\>|)>|B><around*|[|<around*|\<langle\>|L<rsub|m>|\|>G<around*|(|\<omega\>|)>-i<around*|\<langle\>|L<rsub|m>|\|><with|font|Bbb|>|]><big|sum><rsub|n>A<rsub|n><around|\||R<rsub|n>|\<rangle\>>
  </equation*>

  Donc on d�roule les calculs:

  <\equation*>
    A<rsub|m>=<around*|\<langle\>|L<rsub|m>|\|><around|\<nobracket\>|v<around*|(|\<omega\>|)>|\<rangle\>>+<frac|\<alpha\><around*|(|\<omega\>|)>|B><around*|[|<around*|\<langle\>|L<rsub|m>|\|>\<Lambda\><rsub|m>-i<around*|\<langle\>|L<rsub|m>|\|><with|font|Bbb|>|]><big|sum><rsub|n>A<rsub|n><around|\||R<rsub|n>|\<rangle\>>
  </equation*>

  <\equation*>
    \<Rightarrow\>A<rsub|m>=<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>+<frac|\<alpha\><around*|(|\<omega\>|)>|B><big|sum><rsub|n>A<rsub|n><around*|(|\<Lambda\><rsub|m>-i|)><around*|\<langle\>|L<rsub|m><around*|\||R<rsub|n>|\<rangle\>>|\<nobracket\>>
  </equation*>

  <\equation*>
    \<Rightarrow\>A<rsub|m>=<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>+<frac|\<alpha\><around*|(|\<omega\>|)>|B>A<rsub|m><around*|(|\<Lambda\><rsub|m>-i|)>
  </equation*>

  On factorise pour enfin obtenir:

  <\equation*>
    A<rsub|m><around*|(|1-<frac|-\<Gamma\><rsub|0>/2|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2><around*|(|\<Lambda\><rsub|m>-i|)>|)>=<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>
  </equation*>

  <\equation*>
    \<Rightarrow\>A<rsub|m><around*|(|1-<frac|-\<Gamma\><rsub|0>/2|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2><around*|(|<around*|(|\<omega\><rsub|0>-\<omega\><rsub|n>|)><frac|2|\<Gamma\><rsub|0>>+i<around*|(|<frac|\<Gamma\><rsub|n>|\<Gamma\><rsub|0>>|)>-i|)>|)>=<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>
  </equation*>

  <\equation*>
    \<Rightarrow\>A<rsub|m><around*|(|<frac|1|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2><around*|(|\<omega\>-<neg|\<omega\><rsub|0>>+<neg|i\<Gamma\><rsub|0>/2>+<around*|(|<neg|\<omega\><rsub|0>>-\<omega\><rsub|n>|)>+i<around*|(|<frac|\<Gamma\><rsub|n>|2>|)>-<neg|i\<Gamma\><rsub|0>/2>|)>|)>=<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>
  </equation*>

  <\equation*>
    \<Rightarrow\>A<rsub|m><around*|(|<frac|1|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2><around*|(|\<omega\>-\<omega\><rsub|n>+i<around*|(|<frac|\<Gamma\><rsub|n>|2>|)>|)>|)>=<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>
  </equation*>

  <\equation*>
    \<Rightarrow\>A<rsub|m>=<frac|<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>|\<omega\>-\<omega\><rsub|n>+i<around*|(|<frac|\<Gamma\><rsub|n>|2>|)>><around*|(|\<omega\>-\<omega\><rsub|0>+i\<Gamma\><rsub|0>/2|)>=<frac|-B\<Gamma\><rsub|0>/2|\<alpha\><around*|(|\<omega\>|)>><frac|<around*|\<langle\>|L<rsub|m>|\|>v<around*|(|\<omega\>|)>\<rangle\>|\<omega\>-\<omega\><rsub|n>+i<around*|(|<frac|\<Gamma\><rsub|n>|2>|)>>
  </equation*>

  \;

  <\padded-right-aligned>
    <math|<qed>>
  </padded-right-aligned>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>