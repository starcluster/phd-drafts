<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <section|Calcul de <math|\<Delta\><rsup|\<perp\>><around*|(|E|)>>>

  <subsection|Partie r�elle>

  On utilise la m�thode des images:

  <\equation*>
    G<around*|(|r|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>G<around*|(|r<rsub|n>|)>
  </equation*>

  Puis dans l'espace de Fourier:

  <\equation*>
    G<around*|(|r|)>=<big|int><wide|G|~><around*|(|q|)>\<mathe\><rsup|i*q*r>\<mathd\><rsup|3>q
  </equation*>

  Donc:

  <\equation*>
    G<around*|(|r|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><big|int><wide|G|~><around*|(|q|)>\<mathe\><rsup|i*q*r<rsub|n>>\<mathd\><rsup|3>q
  </equation*>

  On calcule la propagation entre <math|r> et <math|r<rprime|'>>:

  <\equation*>
    G<around*|(|r,r<rprime|'>|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><big|int><wide|G|~><around*|(|q|)>\<mathe\><rsup|i*q<rsub|z>*<around*|(|z<rsub|n>-z<rprime|'>|)>>\<mathd\><rsup|3>q
  </equation*>

  En substituant <math|z<rsub|n>=n*L+<around*|(|-1|)><rsup|n>z<rsub|>>:

  <\equation*>
    G<around*|(|r,r<rprime|'>|)>=<big|int><wide|G|~><around*|(|q|)>\<mathe\><rsup|-i*k<rsub|z>*z<rsub|>><around*|(|1+\<mathe\><rsup|2i*q<rsub|z>*<around*|(|z<rsub|>-<frac|L|2>|)>>|)><big|sum><rsub|n\<in\>\<bbb-Z\>>\<mathe\><rsup|i*q<rsub|z>2n*L>\<mathd\><rsup|3>q
  </equation*>

  En remettat les bons coefficient on retrouve la formule de Milloni:

  \;

  <\equation*>
    \<Delta\><rsup|\<perp\>><around*|(|E|)>=-i<big|sum><rsub|<math-bf|k>,\<lambda\>><frac|<around*|\||g<rsub|<math-bf|k>,\<lambda\>>|\|><rsup|2>|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>><around*|(|1+exp<around*|(|2i*k<rsub|z><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>exp<around*|(|2i*n*k<rsub|z>L|)>
  </equation*>

  On a: <math|<wide|\<mu\>|^>\<cdummy\>\<varepsilon\><rsub|1>=cos<around*|(|\<theta\>+<frac|\<pi\>|2>|)>=-sin<around*|(|\<theta\>|)>>
  et <math|<wide|\<mu\>|^>\<cdummy\>\<varepsilon\><rsub|2>=0> et
  <math|\<omega\><rsub|<math-bf|k>>=k\<cdummy\>c>

  Formule de Poisson:

  <\equation*>
    <big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>exp<around*|(|2i*n*k<rsub|z>L|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>\<delta\><around*|(|<frac|k<rsub|z>L|\<pi\>>-n|)>
  </equation*>

  Et �galement:

  <\equation*>
    <around*|\||g<rsub|<math-bf|k>,\<lambda\>>|\|><rsup|2>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|2>><around*|\||<wide|\<mu\>|^>\<cdummy\>\<varepsilon\><rsub|\<lambda\>>|\|><rsup|2><around*|(|2\<pi\>\<hbar\>\<omega\><rsub|<math-bf|k>>/V|)>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|2>>sin<around*|(|\<theta\>|)><rsup|2><around*|(|2\<pi\>\<hbar\>\<omega\><rsub|<math-bf|k>>/V|)>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|2>><around*|(|<frac|k<rsub|\<parallel\>>|k>|)><rsup|2><around*|(|2\<pi\>\<hbar\>\<omega\><rsub|<math-bf|k>>/V|)>
  </equation*>

  Le coefficient d'Einstein s'�crit:

  <\equation*>
    A=<frac|1|3\<pi\>\<varepsilon\><rsub|0>><frac|\<mu\><rsup|2>k<rsub|0><rsup|3>|\<hbar\>>
  </equation*>

  Donc:

  <\equation*>
    <around*|\||g<rsub|<math-bf|k>,\<lambda\>>|\|><rsup|2>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|>><around*|(|<frac|k<rsub|\<parallel\>>|k>|)><rsup|2><around*|(|2\<pi\>\<omega\><rsub|<math-bf|k>>/V|)>=A
    <frac|3\<pi\>\<varepsilon\><rsub|0>|k<rsub|0><rsup|3>><around*|(|<frac|k<rsub|\<parallel\>>|k>|)><rsup|2><around*|(|2\<pi\>\<omega\><rsub|<math-bf|k>>/V|)>=A
    <frac|6\<pi\>\<varepsilon\><rsub|0>|k<rsub|0><rsup|3>><frac|k<rsub|\<parallel\>><rsup|2>|k><around*|(|\<pi\>c/V|)>
  </equation*>

  On substitue:

  <\equation*>
    \<Delta\><rsup|\<perp\>><around*|(|E|)>=-i<big|sum><rsub|<math-bf|k>,\<lambda\>><frac|<around*|\||g<rsub|<math-bf|k>,\<lambda\>>|\|><rsup|2>|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>><around*|(|1+exp<around*|(|2i*k<rsub|z><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>\<delta\><around*|(|<frac|k<rsub|z>L|\<pi\>>-n|)>
  </equation*>

  On somme sur les polarisations et on transforme la forme sur
  <math|<math-bf|k>> en int�grale:

  <\equation*>
    \<Delta\><rsup|\<perp\>><around*|(|E|)>=-i<big|int><frac|<around*|\||g<rsub|<math-bf|k>,\<lambda\>>|\|><rsup|2>|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>><around*|(|1+exp<around*|(|2i*k<rsub|z><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>\<delta\><around*|(|<frac|k<rsub|z>L|\<pi\>>-n|)><frac|V\<mathd\><rsup|3><math-bf|k>|<around*|(|2\<pi\>|)><rsup|3>>
  </equation*>

  Donne:

  <\equation*>
    \<Delta\><rsup|\<perp\>><around*|(|E|)>=-i<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><big|int><frac|A
    |\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>>
    <frac|6\<pi\>\<varepsilon\><rsub|0>|k<rsub|0><rsup|3>><frac|k<rsub|\<parallel\>><rsup|2>|k><around*|(|\<pi\>c|)><around*|(|1+exp<around*|(|2i*k<rsub|z><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)>\<delta\><around*|(|<frac|k<rsub|z>L|\<pi\>>-n|)><frac|\<mathd\><rsup|3><math-bf|k>|<around*|(|2\<pi\>|)><rsup|3>>
  </equation*>

  On peut �crire:

  <\equation*>
    <frac|1|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>>=<frac|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>+i\<varepsilon\>|<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><rsup|2>+\<varepsilon\><rsup|2>>
  </equation*>

  Et donc :

  <\equation*>
    Im<around*|(|<frac|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>+i\<varepsilon\>|<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><rsup|2>+\<varepsilon\><rsup|2>>|)>=\<pi\>\<delta\><around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>
  </equation*>

  Donc:du

  <\equation*>
    \<Delta\><rsup|\<perp\>><around*|(|E|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><big|int><frac|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>+\<varepsilon\>|<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><rsup|2>+\<varepsilon\><rsup|2>><frac|A6\<pi\><rsup|2>\<varepsilon\><rsub|0>c|k<rsub|0><rsup|3>><frac|k<rsub|\<parallel\>><rsup|2>|k><around*|(|1+exp<around*|(|2i*k<rsub|z><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)>\<delta\><around*|(|<frac|k<rsub|z>L|\<pi\>>-n|)><frac|\<mathd\><rsup|3><math-bf|k>|<around*|(|2\<pi\>|)><rsup|3>>
  </equation*>

  On int�gre premi�rement <math|k<rsub|z>>:<math|\<delta\><around*|(|<frac|k<rsub|z>L|\<pi\>>-n|)>=\<delta\><around*|(|k<rsub|z>-<frac|\<pi\>n|L>|)>\<cdummy\><frac|\<pi\>|L>>:

  et:

  <\equation*>
    \<omega\><rsub|<math-bf|k>>=k\<cdummy\>c=<sqrt|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>
  </equation*>

  <\equation*>
    \<Delta\><rsup|\<perp\>><around*|(|E|)>=<frac|6A\<pi\><rsup|3>\<varepsilon\><rsub|0>c|L*k<rsub|0><rsup|3><around*|(|2\<pi\>|)><rsup|3>><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|1+exp<around*|(|*<frac|2i\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><big|int><frac|-i<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>+\<varepsilon\>|<around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)><rsup|2>+\<varepsilon\><rsup|2>><frac|k<rsub|\<parallel\>><rsup|2>|<sqrt|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>>\<mathd\><rsup|2><math-bf|k>
  </equation*>

  \;

  On se concentre sur le calcul de l'int�grale:

  <\equation*>
    <big|int>\<pi\>\<delta\><around*|(|c<sqrt|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>-\<omega\><rsub|0>|)><frac|k<rsub|\<parallel\>><rsup|2>|<sqrt|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>>\<mathd\><rsup|2><math-bf|k>
  </equation*>

  Changement de variable polaire:

  <\equation*>
    <big|int>\<pi\>\<delta\><around*|(|c<sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>-\<omega\><rsub|0>|)><frac|r<rsup|2>r|<sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>>\<mathd\>r*\<mathd\>\<theta\>
  </equation*>

  On pose: <math|f<around*|(|r|)>=c<sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>-\<omega\><rsub|0>>
  qui s'annule en:

  <\equation*>
    r<rsub|0>=<sqrt|<around*|(|<frac|\<omega\><rsub|0>|c<rsup|>>|)><rsup|2>-<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>
  </equation*>

  On exploite la relation:

  <\equation*>
    \<delta\><around*|(|f<around*|(|x|)>|)>=<frac|\<delta\><around*|(|x-x<rsub|0>|)>|<around*|\||f<rprime|'><around*|(|x<rsub|0>|)>|\|>>
  </equation*>

  Premi�rement:

  <\equation*>
    f<rprime|'><around*|(|r|)>=<frac|c*r|<sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>>
  </equation*>

  Donc:

  <\equation*>
    f<rprime|'><around*|(|r<rsub|0>|)>=<frac|c*<sqrt|<around*|(|<frac|\<omega\><rsub|0>|c<rsup|>>|)><rsup|2>-<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>|<frac|\<omega\><rsub|0>|c<rsup|>>>
  </equation*>

  <\equation*>
    \;
  </equation*>

  Donc:

  <\equation*>
    <frac|\<omega\><rsub|0>|c<rsup|>><big|int><frac|\<pi\>\<delta\><around*|(|r-r<rsub|0>|)>|c*<sqrt|<around*|(|<frac|\<omega\><rsub|0>|c<rsup|>>|)><rsup|2>-<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>><frac|r<rsup|2>r|<sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>>\<mathd\>r*\<mathd\>\<theta\>
  </equation*>

  Donne au final:

  <\equation*>
    2\<pi\><neg|<frac|\<omega\><rsub|0>|c<rsup|>>><frac|\<pi\>|c*<sqrt|<around*|(|<frac|\<omega\><rsub|0>|c<rsup|>>|)><rsup|2>-<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>><frac|<sqrt|<around*|(|<frac|\<omega\><rsub|0>|c<rsup|>>|)><rsup|2>-<around*|(|<frac|\<pi\>n|L>|)><rsup|2>><rsup|3>|<neg|<frac|\<omega\><rsub|0>|c>>>=<frac|2\<pi\><rsup|2>k<rsub|0><rsup|2>|c><around*|(|1-<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)>
  </equation*>

  On remet dans la somme:

  <\equation*>
    <frac|6A\<pi\><rsup|3>\<varepsilon\><rsub|0>c|L*k<rsub|0><rsup|3><around*|(|2\<pi\>|)><rsup|3>><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|1+exp<around*|(|*<frac|2i\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><frac|2\<pi\><rsup|2>k<rsub|0><rsup|2>|c><around*|(|1-<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)>
  </equation*>

  Simplifions un peu:

  <\equation*>
    <frac|6A\<varepsilon\><rsub|0>\<pi\><rsup|2>|L*k<rsub|0><rsup|>2<rsup|2>><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|1+exp<around*|(|*<frac|2i\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><around*|(|1-<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)>
  </equation*>

  R�organisation de la somme:

  <\equation*>
    <frac|6A\<varepsilon\><rsub|0>\<pi\><rsup|2>|L*k<rsub|0><rsup|>2<rsup|2>><around*|(|2+<big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|2+exp<around*|(|*<frac|2i\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>+exp<around*|(|*<frac|-2i\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><around*|(|1-<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)>|)>
  </equation*>

  <\equation*>
    <frac|6A\<varepsilon\><rsub|0>\<pi\><rsup|2>|L*k<rsub|0><rsup|>2<rsup|>><around*|(|1+<big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|1+cos<around*|(|*<frac|2i\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><around*|(|1-<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)>|)>
  </equation*>

  Et on remarque que:

  <\equation*>
    1+cos<around*|(|*<frac|2\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>=2cos<rsup|2><around*|(|<frac|\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)><rsup|>
  </equation*>

  On obtient finalement:

  <\equation*>
    <with|color|red|2\<pi\>\<varepsilon\><rsub|0>><frac|6A\<pi\><rsup|>|L*k<rsub|0><rsup|>2<rsup|>><around*|(|<frac|1|2>+<big|sum><rsub|n=1><rsup|+\<infty\>>cos<rsup|2><around*|(|<frac|\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)><around*|(|1-<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)>|)>
  </equation*>

  Qui est correct au syst�me d'unit� pr�s.

  Et le cut-off d'ou ca vient ???:

  <\equation*>
    <with|color|red|2\<pi\>\<varepsilon\><rsub|0>><frac|6A\<pi\><rsup|>|L*k<rsub|0><rsup|>2<rsup|>><around*|(|<frac|1|2>+<big|sum><rsub|n=1><rsup|<with|color|red|<around*|\<lceil\>|k<rsub|0>L/\<pi\>|\<rceil\>>>>cos<rsup|2><around*|(|<frac|\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)><around*|(|1-<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)>|)>
  </equation*>

  <section|Calcul de <math|\<Delta\><rsup|\<parallel\>><around*|(|E|)>>>

  <subsection|Partie r�elle>

  <\equation*>
    \<Delta\><rsup|\<parallel\>><around*|(|E|)>=-i<big|sum><rsub|<math-bf|k>,\<lambda\>><frac|<around*|\||g<rsub|<math-bf|k>,\<lambda\>>|\|><rsup|2>|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>><around*|(|1-exp<around*|(|2i*k<rsub|z><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>exp<around*|(|2i*n*k<rsub|z>L|)>
  </equation*>

  Le calcul fait pr�c�demment est similaire sauf bien s�r dans
  <math|g<rsub|<math-bf|k>,\<lambda\>>>:

  <\equation*>
    \<Delta\><rsup|\<parallel\>><around*|(|E|)>=-i<frac|\<mu\><rsup|2>|\<hbar\><rsup|2>><frac|2\<pi\>\<hbar\>|V><big|sum><rsub|<math-bf|k>,\<lambda\>><frac|\<omega\><rsub|<math-bf|k>><around*|(|1+<frac|k<rsub|z><rsup|2>|k<rsup|2>>|)>|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>><around*|(|1-exp<around*|(|2i*k<rsub|z><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>exp<around*|(|2i*n*k<rsub|z>L|)>
  </equation*>

  Et on passe au continuum:

  <\equation*>
    \<Delta\><rsup|\<parallel\>><around*|(|E|)>=-i<frac|\<mu\><rsup|2>|\<hbar\><rsup|2>><frac|2\<pi\>\<hbar\>|V><frac|\<pi\>|L><big|int><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|\<omega\><rsub|<math-bf|k>><around*|(|1+<frac|k<rsub|z><rsup|2>|k<rsup|2>>|)>|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>><around*|(|1-exp<around*|(|2i*<frac|\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>|)><frac|V\<mathd\><rsup|2><math-bf|k>|<around*|(|2\<pi\>|)><rsup|3>>
  </equation*>

  On r�exprime la somme plus int�gration sur <math|k<rsub|z>>

  <\equation*>
    \<Delta\><rsup|\<parallel\>><around*|(|E|)>=-i<frac|\<mu\><rsup|2>|\<hbar\><rsup|>><frac|8\<pi\><rsup|2>|L><big|int><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|\<omega\><rsub|<math-bf|k>><around*|(|1+<frac|<around*|(|<frac|\<pi\>n|L>|)><rsup|2>|k<rsup|2>>|)>|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>-i\<varepsilon\>>sin<rsup|2><around*|(|*<frac|\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)><frac|\<mathd\><rsup|2><math-bf|k>|<around*|(|2\<pi\>|)><rsup|3>>
  </equation*>

  Pour la partie r�elle:

  <\equation*>
    Re<around*|(|\<Delta\><rsup|\<parallel\>><around*|(|E|)>|)>=-i<frac|\<mu\><rsup|2>|\<hbar\><rsup|>><frac|8\<pi\><rsup|2>|L><big|int><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|\<pi\>\<delta\><around*|(|\<omega\><rsub|<math-bf|k>>-\<omega\><rsub|0>|)>k*c|k-k<rsub|0>-i\<varepsilon\>>sin<rsup|2><around*|(|*<frac|\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)><frac|\<mathd\><rsup|2><math-bf|k>|<around*|(|2\<pi\>|)><rsup|3>>
  </equation*>

  On souhaite maintenant calculer:

  <\equation*>
    <big|int>\<pi\>\<delta\><around*|(|c<sqrt|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>-\<omega\><rsub|0>|)><around*|(|1+<frac|<around*|(|<frac|\<pi\>n|L>|)><rsup|2>|k<rsup|2>>|)>k\<mathd\><rsup|2><math-bf|k>
  </equation*>

  M�thode similaire � la partie pr�c�dente:
  <math|r<rsub|0><rsup|2>=<around*|(|<frac|\<omega\><rsub|0>|c<rsup|>>|)><rsup|2>-<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>

  <\equation*>
    <frac|\<omega\><rsub|0>|c<rsup|>><big|int><frac|\<pi\>\<delta\><around*|(|r-r<rsub|0>|)>|c*r<rsub|0>><around*|(|1+<frac|<around*|(|<frac|\<pi\>n|L>|)><rsup|2>|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>|)>r<rsup|><sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>\<mathd\>r*\<mathd\>\<theta\>=<frac|2\<pi\><rsup|2>\<omega\><rsub|0><rsup|2>|c<rsup|3>><around*|(|1+<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)><rsup|>
  </equation*>

  \;

  <\equation*>
    Re<around*|(|\<Delta\><rsup|\<parallel\>><around*|(|E|)>|)>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|>><frac|\<pi\><rsup|>|L><frac|2\<omega\><rsub|0><rsup|2>|c<rsup|3>><big|sum><rsub|n=1><rsup|<with|color|red|<around*|\<lceil\>|k<rsub|0>L/\<pi\>|\<rceil\>>>><around*|(|1+<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>|)><rsup|>sin<rsup|2><around*|(|*<frac|\<pi\>n|L><around*|(|Z<rsub|0>-<frac|1|2>L|)>|)>
  </equation*>

  <subsection|Partie imaginaire>

  <subsubsection|Cas <math|L\<rightarrow\>+\<infty\>>>

  Dans ce cas le peigne de dirac est remplac� par une constante
  <math|L/\<pi\>>:

  \;

  <\equation*>
    \<Delta\><rsup|\<parallel\>><around*|(|E|)>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|2>><frac|\<hbar\>|<around*|(|2\<pi\>|)><rsup|2>><big|int><rsub|-k<rsub|max>><rsup|+k<rsub|max>><frac|k<around*|(|1+<frac|k<rsub|z><rsup|2>|k<rsup|2>>|)>|k-k<rsub|0>>\<mathd\><rsup|3><math-bf|k>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|2>><frac|\<hbar\>|<around*|(|2\<pi\>|)><rsup|2>><frac|8|3>2\<pi\><big|int><rsub|0><rsup|R><frac|r<rsup|3>|r-k<rsub|0>>\<mathd\>r
  </equation*>

  Et on a en p<math|>osant <math|x=R/k<rsub|0>>

  <\equation*>
    <big|int><rsub|0><rsup|R><frac|r<rsup|3>|r-k<rsub|0>>\<mathd\>r=k<rsub|0><rsup|3><around*|(|<frac|x<rsup|3>|3>+<frac|x<rsup|2>|2>+x+log<around*|(|x-1|)>|)>
  </equation*>

  <subsubsection|Cas <math|L> fini>

  Dans ce cas on calcule:

  <\equation*>
    \<Delta\><rsup|\<parallel\>><around*|(|E|)>=-i<frac|\<mu\><rsup|2>|\<hbar\><rsup|>><frac|8\<pi\><rsup|2>|L><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><big|int><frac|k<around*|(|1+<frac|<around*|(|<frac|\<pi\>n|L>|)><rsup|2>|k<rsup|2>>|)>|k-k<rsub|0>><frac|\<mathd\>k<rsub|x>\<mathd\>k<rsub|y>|<around*|(|2\<pi\>|)><rsup|3>><space|2em>k=<sqrt|k<rsub|x><rsup|2>+k<rsub|y><rsup|2>+<around*|(|\<pi\>n/L|)><rsup|2>>
  </equation*>

  On calcule l'int�grale en posant <math|x=R/k<rsub|0>>:

  <\equation*>
    <big|int><frac|<sqrt|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>><frac|k<rsub|\<parallel\>><rsup|2>+2<around*|(|<frac|\<pi\>n|L>|)><rsup|2>|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>|<sqrt|k<rsub|\<parallel\>><rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>-k<rsub|0>><frac|\<mathd\><rsup|2><math-bf|k>|<around*|(|2\<pi\>|)><rsup|3>>=<big|int><frac|<sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>><around*|(|r<rsup|2>+2<around*|(|<frac|\<pi\>n|L>|)><rsup|2>|)>|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2><around*|(|<sqrt|r<rsup|2>+<around*|(|<frac|\<pi\>n|L>|)><rsup|2>>-k<rsub|0>|)>><frac|r\<mathd\>r|<around*|(|2\<pi\>|)><rsup|2>>
  </equation*>

  Donc:

  <\equation*>
    Im<around*|(|\<Delta\><rsup|\<parallel\>><around*|(|E|)>|)>=<frac|\<mu\><rsup|2>|\<hbar\><rsup|>><frac|2<rsup|>|L>k<rsub|0><rsup|2><big|sum><rsub|n=1><rsup|<with|color|red|<around*|\<lceil\>|k<rsub|0>L/\<pi\>|\<rceil\>>>><around*|(|<frac|x<rsup|2>|2>+x+<around*|(|<around*|(|<frac|\<pi\>n|L*k<rsub|0>>|)><rsup|2>+1|)>log<around*|(|x-1|)>|)>
  </equation*>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|1.1|1>>
    <associate|auto-3|<tuple|2|4>>
    <associate|auto-4|<tuple|2.1|4>>
    <associate|auto-5|<tuple|2.2|5>>
    <associate|auto-6|<tuple|2.2.1|5>>
    <associate|auto-7|<tuple|2.2.2|5>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Calcul
      de <with|mode|<quote|math>|\<Delta\><rsup|\<perp\>><around*|(|E|)>>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Partie r�elle
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Calcul
      de <with|mode|<quote|math>|\<Delta\><rsup|\<parallel\>><around*|(|E|)>>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Partie r�elle
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>Partie imaginaire
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|2tab>|2.2.1<space|2spc>Cas
      <with|mode|<quote|math>|L\<rightarrow\>+\<infty\>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|2tab>|2.2.2<space|2spc>Cas
      <with|mode|<quote|math>|L> fini <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>
    </associate>
  </collection>
</auxiliary>