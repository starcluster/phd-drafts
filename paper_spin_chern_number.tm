<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Spin chern number analysis of topological edge states
  in all-dielectric photonic crystals>>

  <\padded-center>
    <strong|Abstract:> We investigate the photonic properties of an
    all-dielectric photonic crystal structure that supports two-dimensionally
    confined helical topological edge states. Using a tight-binding model and
    a novel method for computing the spin chern number, we demonstrate the
    existence of two distinct regimes: a trivial regime, where the spin chern
    number is zero, and a topological regime, where the spin chern number is
    non-zero and exhibits topologically protected edge states. Our findings
    provide insight into the fundamental photonic properties of
    all-dielectric photonic crystals and suggest potential applications in
    the design of novel photonic devices <with|color|red|(?)>. We show that
    the relative strength of the crystal parameters strongly influences the
    topological properties of the band structure, providing a deeper
    understanding of the electronic and photonic properties of all-dielectric
    photonic crystals and related materials.
  </padded-center>

  <section|Introduction>

  In recent years, there has been a growing interest in the study of
  topological materials, which exhibit unique electronic and photonic
  properties due to their non-trivial topology [refs]. Among these materials,
  all-dielectric photonic crystal structures have attracted particular
  attention as they offer the possibility of realizing robust and efficient
  photonic devices [refs]. In this paper, we investigate the photonic
  properties of an all-dielectric photonic crystal structure that supports
  two-dimensionally confined helical topological edge states
  <with|color|red|(repetition ?)>. Our analysis is based on the tight-binding
  model, which provides a simple yet powerful approach for describing the
  electronic and photonic structure of solids. Specifically, we focus on the
  spin chern number, a topological invariant that characterizes the
  non-trivial topology of the band structure of a material. Our method for
  computing the spin chern number is novel in that we introduce a spin
  operator to select the bands even though they are degenerate. By computing
  the spin chern number for the tight-binding model of the all-dielectric
  photonic crystal structure, we demonstrate the existence of two distinct
  regimes: a trivial regime, where the spin chern number is zero, and a
  topological regime <with|color|red|(quote barik ?)>, where the spin chern
  number is non-zero and exhibits topologically protected edge states. Our
  findings provide insight into the fundamental electronic and photonic
  properties of all-dielectric photonic crystals and suggest potential
  applications in the design of novel photonic devices. It is worth noting
  that our analysis shows that the trivial regime occurs when the crystal
  parameter <math|t<rsub|2>><math|> is smaller than the nearest-neighbor
  parameter <math|t<rsub|1>>, whereas the topological regime is observed when
  the crystal parameter <math|t<rsub|2>> is larger than <math|t<rsub|1>>.
  This result is consistent with previous studies on other topological
  materials [refs], which have shown that the relative strength of the
  parameters can strongly influence the topological properties of the band
  structure. Our analysis sheds further light on the relationship between the
  crystal parameters and the topology of the band structure, providing a
  deeper understanding of the electronic and photonic properties of
  all-dielectric photonic crystals and related materials.

  <section|Our model>

  <\big-figure|<image|lattice_htb.pdf|0.7par|||>>
    \;
  </big-figure>

  Our analysis is based on a tight-binding model of a triangular lattice with
  a six-site basis spanned by :

  <\eqnarray*>
    <tformat|<table|<row|<cell|<math-bf|a><rsub|1>>|<cell|=>|<cell|a<around*|(|3,0|)>>>|<row|<cell|<math-bf|a><rsub|2>>|<cell|=>|<cell|a<around*|(|3/2,3<sqrt|3>/2|)>>>>>
  </eqnarray*>

  \ The Hamiltonian of the system, <math|H>, can be written as the sum of two
  terms, <math|H<rsub|1>> and <math|H<rsub|2>>, representing the
  intra-cluster and inter-cluster couplings, respectively:

  <\equation*>
    H=H<rsub|1>+H<rsub|2>
  </equation*>

  where <math|H<rsub|1>> is characterized by a parameter <math|t<rsub|1>>,
  representing the strength of the intra-cluster coupling, and
  <math|H<rsub|2>> is characterized by a parameter <math|t<rsub|2>>,
  representing the strength of the inter-cluster coupling.

  The <math|H<rsub|1>> term describes the coupling between the sites within
  each cluster, which are arranged in a hexagonal pattern. Specifically,
  <math|H<rsub|1>> and <math|H<rsub|2 >>can be written as:

  <\equation*>
    H<rsub|1>=-t<rsub|1><big|sum><rsub|i,j>c<rsub|i><rsup|\<dagger\>>c<rsub|j><space|1em>H<rsub|2>=-t<rsub|2><big|sum><rsub|m,n>c<rsub|m><rsup|\<dagger\>>c<rsub|n>
  </equation*>

  with <math|i,j> (<math|m,n>) running over NN-sites inside the hexagonal
  unit (NN-sites between hexagonal unit cell) and
  \ <math|c<rsub|i><rsup|\<dagger\>>> (<math|c<rsub|i>>) is the creation
  (annihilation) operator for the <math|i>-th site in the cluster. This
  hamiltonian takes the effective form:

  <\equation*>
    H<around*|(|<math-bf|k>|)>=-t<rsub|1><matrix|<tformat|<table|<row|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|1>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|1>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|1>>|<row|<cell|1>|<cell|0>|<cell|0>|<cell|0>|<cell|1>|<cell|0>>>>>-t<rsub|2><matrix|<tformat|<table|<row|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i<math-bf|k>\<cdot\><math-bf|a><rsub|1>>>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i<math-bf|k>\<cdot\><math-bf|a><rsub|2>>>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|i<math-bf|k>\<cdot\><around*|(|<math-bf|a><rsub|2>-<math-bf|a><rsub|1>|)>>>>|<row|<cell|\<mathe\><rsup|-i<math-bf|k>\<cdot\><math-bf|a><rsub|1>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|\<mathe\><rsup|-i<math-bf|k>\<cdot\><math-bf|a><rsub|2>>>|<cell|0>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|\<mathe\><rsup|-i<math-bf|k>\<cdot\><around*|(|<math-bf|a><rsub|2>-<math-bf|a><rsub|1>|)>>>|<cell|0>|<cell|0>|<cell|0>>>>>
  </equation*>

  For the rest of this paper, we will keep <math|t<rsub|1>> and <math|a>
  fixed and only vary <math|t<rsub|2>.><space|1em>In order to better
  understand the photonic properties of our system, we first present a band
  diagram that shows the energy levels of the system as a function of
  momentum in Fourier space. This band diagram is calculated for three
  interesting values of <math|t<rsub|2>>. The topological properties of these
  bands can be characterized by a topological invariant known as the spin
  Chern number, which we will compute for both cases in the following
  sections.

  <\big-figure|<image|BZ_tb.pdf|0.5par|||><image|/home/these/localization-of-light-in-2d/Correlation
  on band diagramt_2 = 1.pdf|0.5par|||>>
    On the left we present the Brillouin zone of lattice, where we consider
    the momentum vector <math|<math-bf|k>=<around*|(|k<rsub|x>,k<rsub|y>|)>>
    This zone is bounded by high symmetry points and lines, which are
    important for analyzing the band structure. On the right side of the
    figure, we show the band diagram of our Hamiltonian <math|H>, computed
    for the special case where <math|t<rsub|1>=t<rsub|2>=1>. The energy
    levels of the bands are plotted as a function of the momentum vector
    <math|<math-bf|k>> following the path descibed on the left.
  </big-figure>

  <\big-figure|<image|/home/these/localization-of-light-in-2d/Correlation on
  band diagramt_2 = 0.5.pdf|0.5par|||> <image|/home/these/localization-of-light-in-2d/Correlation
  on band diagramt_2 = 1.5.pdf|0.5par|||>>
    Band diagrams of our Hamiltonian <math|H>, computed for the case
    <math|t<rsub|2>\<gtr\>t<rsub|1>> which be believe to be the topological
    case, and the case <math|t<rsub|2>\<less\>t<rsub|1>> which be believe to
    be the trivial case.\ 
  </big-figure>

  <subsection|Topological properties of semi-finite nanoribbons>

  In this section, we consider a semi-finite system where the system is
  finite in the y-direction but infinite in the x-direction. Specifically, we
  consider a nanoribbon of width N, where N is the number of unit cells in
  the y-direction. In the topological case where
  <math|t<rsub|2>\<gtr\>t<rsub|1>>, the system exhibits edge states within
  the bulk gap, while in the trivial case where
  <math|t<rsub|2>\<less\>t<rsub|1>>, no edge states are present within the
  bulk gap. This allows for a clear distinction between the two cases and
  highlights the topological protection of edge states in our system.

  <section|Computing the Spin Chern Number with Projected Spin Operators>

  To characterize the topological properties of the energy bands shown in the
  band diagram, we need to compute a topological invariant known as the Chern
  number. However, the problem we face is that the energy bands are
  degenerate, making it impossible to isolate the eigenstates that we need
  for this calculation. To overcome this difficulty, we introduce a new
  Hamiltonian <math|H<rprime|'>>, given by <math|P\<sigma\>P>, where
  <math|\<sigma\>> is a spin operator:

  <\equation*>
    \<sigma\>=diag<around*|(|0,1,-1,1,-1,0|)>
  </equation*>

  and <math|P> is the projection operator onto the occupied bands. This
  Hamiltonian allows us to lift the degeneracy of the energy bands and select
  only the states we need for our calculation.

  By diagonalizing <math|H<rprime|'>>, we obtain three bands, one at energy
  <math|-1>, one at energy <math|0>, and one at energy <math|1>. The band at
  energy 0 is ignored for the computation of the Spin Chern Number, as it
  does not contribute to the topological properties of the system. Here we
  present the band diagram of <math|H<rprime|'>> for two different parameter
  regimes, one where <math|t<rsub|2>\<less\>t<rsub|1>> and one where
  <math|t<rsub|2>\<gtr\>t<rsub|1>>, highlighting that it is now easy to
  separate bands.

  <\big-figure|<image|/home/these/localization-of-light-in-2d/Band diagram of
  \ P\<sigma\> P \ \ \ t_2 = 1.5.pdf|0.5par|||><image|/home/these/localization-of-light-in-2d/Band
  diagram of \ P\<sigma\> P \ \ \ t_2 = 0.5.pdf|0.5par|||>>
    \;
  </big-figure>

  <\big-figure|<image|/home/these/localization-of-light-in-2d/Spectrum of
  \ P\<sigma\> P \ on Brillouin zone - \ \ t_2 = 1.5.pdf|0.5par|||>
  <image|/home/these/localization-of-light-in-2d/Spectrum of \ P\<sigma\> P
  \ on Brillouin zone - \ \ t_2 = 0.5.pdf|0.5par|||>>
    \ The Hamiltonian exhibits a total of six eigenvalues for each <strong|k>
    with the BZ. Approximately one-sixth of the eigenvalues have an energy
    close to -1, while one-third of the eigenvalues have an energy close to
    0, and one-sixth of the eigenvalues have an energy close to 1.
  </big-figure>

  \;

  Using <math|H<rprime|'>>, we can compute the spin Chern number for each
  band by discretizing the Brillouin zone and calculating the Berry curvature
  for each point in the zone [ref fukui]. Once we have computed the Berry
  curvature, we can integrate it over the Brillouin zone to obtain the spin
  Chern number for band of energy <math|>1 and <math|-1>:

  <\equation*>
    C<rsub|\<pm\>>=<frac|1|2\<pi\>i><big|iint><rsub|BZ>F<rsub|12><rsup|\<pm\>><around*|(|k|)>
  </equation*>

  We have <math|C<rsub|+>+C<rsub|->=0> and we define the spin chern number
  as:

  <\equation*>
    C<rsub|s>=<frac|1|2><around*|(|C<rsub|+>-C<rsub|->|)>
  </equation*>

  This approach has been successfully applied to other systems with
  degenerate bands, but to the best of our knowledge, it has not yet been
  used to study topological properties in the context of all-dielectric
  photonic crystals. We believe that this approach will pave the way for
  further studies of topological photonic systems with degenerate energy
  bands.

  <\big-figure|<image|/home/these/localization-of-light-in-2d/spin_chern_number_tight_binding.pdf|0.55par|||>>
    Spin Chern number as a function of <math|t<rsub|2>>, for fixed
    <math|t<rsub|1>>. The spin Chern number is computed for the entire system
    using the introduced spin operator to select the degenerate bands. The
    critical point where the transition between the topological and trivial
    regimes occurs is at <math|t<rsub|2>=t<rsub|1>>.
  </big-figure>

  <section|Derivation of analytical expressions for the gap width and Chern
  number>

  In this section, we present a theoretical analysis of the honeycomb lattice
  Hamiltonian in the vicinity of the <math|\<Gamma\>> point. Specifically, we
  derive analytical expressions for the width of the gap and the Chern number
  in terms of the lattice parameters <math|t<rsub|1>> and <math|t<rsub|2>>.
  The analytical expressions we obtain will provide valuable insight into the
  behavior of the honeycomb lattice system and may be useful in guiding the
  design of future experiments. \ This method is justified by the fact that
  only the value of the lattice field around the \<Gamma\> point plays a
  significant role in the value of the Chern number.

  <subsection|Width of the gap>

  We noticed that the gap opens at the Gamma point in the Brillouin zone.
  Hence, we can compute the eigenvalues of the Hamiltonian at this point, and
  order them according to their energy. The gap width can then be obtained by
  taking the distance between the third and fourth eigenvalue, since the gap
  is defined as the energy difference between these two states.

  <\equation*>
    Sp<around*|(|H<around*|(|\<Gamma\>|)>|)>=<around*|{|-2t<rsub|1>-t<rsub|2>,-t<rsub|1>+t<rsub|2>,t<rsub|1>-t<rsub|2>,2t<rsub|1>+t<rsub|2>|}>
  </equation*>

  Thus:

  <\equation*>
    W<rsub|gap>=2<around*|\||t<rsub|2>-t<rsub|1>|\|>
  </equation*>

  <subsection|Spin Chern number>

  <\equation*>
    H<around*|(|k|)>=<matrix|<tformat|<table|<row|<cell|H<rsub|sl><around*|(|k|)>>|<cell|H<rsub|sr><around*|(|k|)>>>|<row|<cell|H<rsub|ll><around*|(|k|)>>|<cell|H<rsub|lr><around*|(|k|)>>>>>>
  </equation*>

  <\equation*>
    H<rsub|sr><around*|(|k|)>=-<matrix|<tformat|<table|<row|<cell|t<rsub|2><around*|(|i<sqrt|3>k<rsub|x>+1|)>>|<cell|0>|<cell|t<rsub|1>>>|<row|<cell|0>|<cell|t<rsub|2><around*|(|<frac|1|2><around*|(|i<sqrt|3>k<rsub|x>+3i*k<rsub|y>|)>+1|)>>|<cell|0>>|<row|<cell|t<rsub|1>>|<cell|0>|<cell|t<rsub|2><around*|(|<frac|1|2><around*|(|-i<sqrt|3>k<rsub|x>+3i*k<rsub|y>|)>+1|)>>>>>>+O<around*|(|k<rsub|x><rsup|2>,k<rsub|y><rsup|2>|)>
  </equation*>

  <\equation*>
    H<rsub|sl><around*|(|k|)>=-<matrix|<tformat|<table|<row|<cell|0>|<cell|t<rsub|1>>|<cell|0>>|<row|<cell|t<rsub|1>>|<cell|0>|<cell|t<rsub|1>>>|<row|<cell|0>|<cell|t<rsub|1>>|<cell|0>>>>>=H<rsub|lr><around*|(|k|)>
  </equation*>

  And <math|H<rsub|sr><around*|(|k|)>=H<rsub|ll><around*|(|k|)><rsup|\<ast\>>>.

  <with|color|red|TO BE CONTINUED.>

  <section|Conclusion>

  In this paper, we have studied the tight-binding model on a triangular
  lattice with a six-site basis, characterized by intra-cluster and
  inter-cluster couplings with parameters <math|t<rsub|1>> and
  <math|t<rsub|2>>, respectively. We have shown that for certain values of
  <math|t<rsub|1>> and <math|t<rsub|2>>, the system exhibits a topological
  phase with non-trivial spin Chern number.

  To compute the Chern number, we introduced a spin operator to select the
  relevant bands and compute the spin Chern number. We have presented band
  diagrams and computed the Chern number for different values of
  <math|t<rsub|2>>, highlighting the topological and trivial regimes.

  In the third part of this paper, we have derived analytical expressions for
  the gap width and the Chern number using a Taylor expansion around the
  point Gamma. Our results provide insight into the physical properties of
  the system and may serve as a guide for experimental studies.
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|6|6>>
    <associate|auto-11|<tuple|4|6>>
    <associate|auto-12|<tuple|4.1|6>>
    <associate|auto-13|<tuple|4.2|7>>
    <associate|auto-14|<tuple|5|7>>
    <associate|auto-2|<tuple|2|2>>
    <associate|auto-3|<tuple|1|2>>
    <associate|auto-4|<tuple|2|3>>
    <associate|auto-5|<tuple|3|4>>
    <associate|auto-6|<tuple|2.1|4>>
    <associate|auto-7|<tuple|3|4>>
    <associate|auto-8|<tuple|4|5>>
    <associate|auto-9|<tuple|5|5>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        On the left we present the Brillouin zone of lattice, where we
        consider the momentum vector <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|k>>>=<around*|(|k<rsub|x>,k<rsub|y>|)>>
        This zone is bounded by high symmetry points and lines, which are
        important for analyzing the band structure. On the right side of the
        figure, we show the band diagram of our Hamiltonian
        <with|mode|<quote|math>|H>, computed for the special case where
        <with|mode|<quote|math>|t<rsub|1>=t<rsub|2>=1>. The energy levels of
        the bands are plotted as a function of the momentum vector
        <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|k>>>>
        following the path descibed on the left.
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Band diagrams of our Hamiltonian <with|mode|<quote|math>|H>, computed
        for the case <with|mode|<quote|math>|t<rsub|2>\<gtr\>t<rsub|1>> which
        be believe to be the topological case, and the case
        <with|mode|<quote|math>|t<rsub|2>\<less\>t<rsub|1>> which be believe
        to be the trivial case.\ 
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-8>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \ The Hamiltonian exhibits a total of six eigenvalues for each
        <with|font-series|<quote|bold>|math-font-series|<quote|bold>|k> with
        the BZ. Approximately one-sixth of the eigenvalues have an energy
        close to -1, while one-third of the eigenvalues have an energy close
        to 0, and one-sixth of the eigenvalues have an energy close to 1.
      </surround>|<pageref|auto-9>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        Spin Chern number as a function of
        <with|mode|<quote|math>|t<rsub|2>>, for fixed
        <with|mode|<quote|math>|t<rsub|1>>. The spin Chern number is computed
        for the entire system using the introduced spin operator to select
        the degenerate bands. The critical point where the transition between
        the topological and trivial regimes occurs is at
        <with|mode|<quote|math>|t<rsub|2>=t<rsub|1>>.
      </surround>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Our
      model> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Topological properties of
      semi-finite nanoribbons <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Computing
      the Spin Chern Number with Projected Spin Operators>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Derivation
      of analytical expressions for the gap width and Chern number>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11><vspace|0.5fn>

      <with|par-left|<quote|1tab>|4.1<space|2spc>Width of the gap
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|1tab>|4.2<space|2spc>Spin Chern number
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|5<space|2spc>Conclusion>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>