import os

s1 = """#+SETUPFILE: page.org
#+INCLUDE: begin.org
"""

s2 = """
#+INCLUDE: end.org
"""

page = s1
blog = os.listdir("blog")
for entry in blog:
    if entry != 'end.org' and entry != 'begin.org' and entry != 'page.org' and entry[-5:] != '.html':
        page += '[[file:blog/' + entry + '][' + entry.strip('.org') + ']]\n'
page += s2
# emacs accueil.org --batch -f org-html-export-to-html --kil
print(page)
