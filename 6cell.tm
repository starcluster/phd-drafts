<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Triangular photonic crystal of artificial atoms
  composed by six punctuals scatterers>>

  We start from:

  <\equation*>
    G<math-bf|\<Psi\>>=\<Lambda\><math-bf|\<Psi\>>
  </equation*>

  A unit cell is composed of six atoms:

  <\big-figure|<image|/home/these/phd-drafts/6cell.png|0.3par|||>>
    \;
  </big-figure>

  Where:

  <\equation*>
    <math-bf|a><rsub|1>=<matrix|<tformat|<table|<row|<cell|a>>|<row|<cell|0>>>>>,<math-bf|a><rsub|2>=<matrix|<tformat|<table|<row|<cell|a/2>>|<row|<cell|<sqrt|3>a/2>>>>>,<math-bf|a><rsub|3>=<matrix|<tformat|<table|<row|<cell|-a/2>>|<row|<cell|<sqrt|3>a/2>>>>>,<math-bf|a><rsub|4>=<matrix|<tformat|<table|<row|<cell|-a>>|<row|<cell|0>>>>>,<math-bf|a><rsub|5>=<matrix|<tformat|<table|<row|<cell|-a/2>>|<row|<cell|-<sqrt|3>a/2>>>>>,<math-bf|a><rsub|6>=<matrix|<tformat|<table|<row|<cell|a/2>>|<row|<cell|-<sqrt|3>a/2>>>>>
  </equation*>

  Restricting ourselves to the TE-modes, the wave function inside the unit
  cell <math|n> is a 12 component vector:

  <\equation*>
    <math-bf|\<Psi\>><rsub|n>=\<psi\><around*|(|<math-bf|k>|)>\<mathe\><rsup|i*<math-bf|k>\<cdummy\><math-bf|r><rsub|n>>
  </equation*>

  where:

  <\equation*>
    \<psi\><around*|(|<math-bf|k>|)>=<rsup|t><around*|(|A<rsub|j\<alpha\>>|)><rsub|<tabular*|<tformat|<table|<row|<cell|<tabular*|<tformat|<table|<row|<cell|1\<leqslant\>j\<leqslant\>6>>>>>>>|<row|<cell|<tabular*|<tformat|<table|<row|<cell|\<alpha\>=+,->>>>>>>>>>>
  </equation*>

  We obtain the 12 equations for <math|1\<leqslant\>j\<leqslant\>6> and
  <math|\<alpha\>=+,->:

  <\equation*>
    \<Lambda\>A<rsub|j\<alpha\>><around*|(|<math-bf|k>|)>\<mathe\><rsup|i*<math-bf|k>\<cdummy\><math-bf|r><rsub|n>>=<big|sum><rsub|<tabular*|<tformat|<table|<row|<cell|<math-bf|r><rsub|m>\<in\>C>>|<row|<cell|\<beta\>=+,->>>>>><big|sum><rsup|6><rsub|i=1>A<rsub|\<beta\>i><around*|(|<math-bf|k>|)>\<mathe\><rsup|i*<math-bf|k>\<cdummy\><math-bf|r><rsub|m>>G<rsup|\<alpha\>\<beta\>*><rsub|div><around*|(|<math-bf|r><rsub|m>-<math-bf|r><rsub|n>+<math-bf|a><rsub|i>-<math-bf|a><rsub|j>|)>
  </equation*>

  Additional subscript div remind that the diagonal term
  <math|<math-bf|r><rsub|m>-<math-bf|r><rsub|n>+<math-bf|a><rsub|i>-<math-bf|a><rsub|j>=0>.
  <math|C> is the triangular lattice that generates the hexagonal lattice.

  Assuming all sites are equivalent, which is valid for an infinite atomic
  lattice, we put <math|><math|<math-bf|r><rsub|n>=0> and we can rewrite the
  problem in a compact form:

  <\equation*>
    M<around*|(|<math-bf|k>|)>\<psi\><around*|(|<math-bf|k>|)>=\<Lambda\><around*|(|<math-bf|k>|)>\<psi\><around*|(|<math-bf|k>|)>
  </equation*>

  Where <math|M<around*|(|<math-bf|k>|)>> is a <math|12\<times\>12> matrix:

  <\equation*>
    <around*|(|M<rsup|\<alpha\>\<beta\>><rsub|i*j><around*|(|<math-bf|k>|)>|)><rsub|<tabular*|<tformat|<table|<row|<cell|1\<leqslant\>i,j\<leqslant\>6>>|<row|<cell|\<alpha\>,\<beta\>=+,->>>>>>=<big|sum><rsub|<math-bf|r><rsub|m>\<in\>C>\<mathe\><rsup|i*<math-bf|k>\<cdummy\><math-bf|r><rsub|m>>G<rsup|\<alpha\>\<beta\>*><rsub|div><around*|(|<math-bf|r><rsub|m>+<math-bf|a><rsub|i>-<math-bf|a><rsub|j>|)>
  </equation*>

  Performing Poisson summation, for the diagonal terms:

  <\equation*>
    <big|sum><rsub|<tabular*|<tformat|<table|<row|<cell|<math-bf|r><rsub|m>\<in\>C>>|<row|<cell|<math-bf|r><rsub|m>\<neq\><math-bf|a><rsub|i>-<math-bf|a><rsub|j>
    >>>>>>\<mathe\><rsup|i*<math-bf|k>\<cdummy\><math-bf|r><rsub|m>>G<rsup|\<alpha\>\<beta\>*><rsub|div><around*|(|<math-bf|r><rsub|m>+<math-bf|a><rsub|i>-<math-bf|a><rsub|j>|)>=<frac|1|\<cal-A\>><big|sum><rsub|<tabular*|<tformat|<table|<row|<cell|<math-bf|q><rsub|m>\<in\>\<frak-F\>>>>>>>g<rsup|\<alpha\>\<beta\>*><rsub|><around*|(|<math-bf|q><rsub|m>-<math-bf|k>|)>\<mathe\><rsup|i*<around*|(|<math-bf|q><rsub|m>-<math-bf|k>|)>\<cdummy\><around*|(|<math-bf|r><rsub|m>+<math-bf|a><rsub|i>-<math-bf|a><rsub|j>|)>>-G<rsup|\<alpha\>\<beta\>><around*|(|0|)>
  </equation*>

  And for the non diagonal terms:

  <\equation*>
    <big|sum><rsub|<tabular*|<tformat|<table|<row|<cell|<math-bf|r><rsub|m>\<in\>C>>>>>>\<mathe\><rsup|i*<math-bf|k>\<cdummy\><math-bf|r><rsub|m>>G<rsup|\<alpha\>\<beta\>*><rsub|div><around*|(|<math-bf|r><rsub|m>+<math-bf|a><rsub|i>-<math-bf|a><rsub|j>|)>=<frac|1|\<cal-A\>><big|sum><rsub|<tabular*|<tformat|<table|<row|<cell|<math-bf|q><rsub|m>\<in\>\<frak-F\>>>>>>>g<rsup|\<alpha\>\<beta\>*><rsub|><around*|(|<math-bf|q><rsub|m>-<math-bf|k>|)>\<mathe\><rsup|i*<around*|(|<math-bf|q><rsub|m>-<math-bf|k>|)>\<cdummy\><around*|(|<math-bf|r><rsub|m>+<math-bf|a><rsub|i>-<math-bf|a><rsub|j>|)>>
  </equation*>

  In real space:

  <\equation*>
    S<rsub|i\<nocomma\>j><rsup|\<alpha\>\<nocomma\>\<beta\>><around*|(|<math-bf|k>|)>=<big|sum><rsub|m=-N><rsup|N><big|sum><rsub|n=-N><rsup|N>g<rsup|\<alpha\>\<nocomma\>\<beta\>><around*|(|x,y|)>\<mathe\><rsup|i*k\<cdummy\>r>
  </equation*>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>