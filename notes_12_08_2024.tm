<TeXmacs|2.1>

<style|generic>

<\body>
  <section|Lolitop>

  <\itemize>
    <item>V�rifier la valeur de la vitesse de propagation du bord, utiliser

    <\equation*>
      <frac|\<partial\>\<omega\>|\<partial\>k>=<frac|\<partial\><around*|(|\<omega\>/t<rsub|1>|)>|\<partial\><around*|(|k*a|)>>a*t<rsub|1>
    </equation*>

    <item>�tendre le diagramme de phase pour se retrouver dans la situation
    ni�oise

    <item>MPB/Meep pour comparer avec le tight-binding.

    <item>Voir PRB 88 pour les coefficients de couplage
  </itemize>

  <section|Light in deformed lattice>

  <\itemize>
    <item>Fig 1. erreur au niveau des num�rotation des sites

    <item>Ajouter pour la base spdf_pol les composantes 2 et 6

    <item>(46) <math|n\<rightarrow\>n-1>

    <item>Int�grer le suppl dans le texte en int�grant dans des appendices A
    et B\ 

    <item>(13) probl�me d'unit� ? <math|<sqrt|s>/m*>

    <item>Fig 3. ajouter les axes en <math|a> et <math|b>
  </itemize>

  <section|Bott index package>

  <\itemize>
    <item>Voir prodan pour un nombre d'obital arbitraire (Math theory 44)
  </itemize>

  <section|Defense>

  <\itemize>
    <item>Ne pas parler de tight-binding ?\ 

    <item>Commencer les transparents

    <item>Intro: Topologie, Bott, Chern, Trouver comment introduire ? Parler
    un peu de tight-binding ?
  </itemize>

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-2|<tuple|2|?|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-3|<tuple|3|?|../.TeXmacs/texts/scratch/no_name_2.tm>>
    <associate|auto-4|<tuple|4|?|../.TeXmacs/texts/scratch/no_name_2.tm>>
  </collection>
</references>