import meep as mp
import numpy as np
import matplotlib.pyplot as plt
import os
import imageio

def plot_hz(simulation, sx, sy, frequency):
    """
    Plot the Hz field for a given simulation at a specific frequency.
    
    Parameters:
    - simulation: The Meep simulation object.
    - sx: Size of the simulation cell in the x-direction.
    - sy: Size of the simulation cell in the y-direction.
    - frequency: Frequency at which to plot the Hz field.
    """
    # Extraire le champ Hz à la fréquence donnée
    hz_data = simulation.get_array(center=mp.Vector3(), size=mp.Vector3(sx, sy), component=mp.Hz)
    
    # Reshape des données pour correspondre à la grille de simulation
    num_x = int(sx * simulation.resolution)
    num_y = int(sy * simulation.resolution)
    hz_data = hz_data.reshape((num_x, num_y))
    
    # Traçage du champ Hz
    plt.figure(figsize=(10, 5))
    plt.imshow(hz_data.T, cmap='RdBu', extent=(-sx/2, sx/2, -sy/2, sy/2))
    plt.colorbar(label='Hz field')
    plt.xlabel('x (um)')
    plt.ylabel('y (um)')
    plt.title(f'Hz field distribution at frequency {frequency:.2f} (um^-1)')
    plt.show()

def save_hz_snapshot(simulation, sx, sy, step, output_dir):
    """
    Save a snapshot of the Hz field at a specific simulation step.

    Parameters:
    - simulation: The Meep simulation object.
    - sx: Size of the simulation cell in the x-direction.
    - sy: Size of the simulation cell in the y-direction.
    - step: The current time step of the simulation.
    - output_dir: The directory where the image will be saved.
    """
    hz_data = simulation.get_array(center=mp.Vector3(), size=mp.Vector3(sx, sy), component=mp.Hz)
    num_x = int(sx * simulation.resolution)
    num_y = int(sy * simulation.resolution)
    hz_data = hz_data.reshape((num_x, num_y))

    plt.figure(figsize=(10, 5))
    plt.imshow(hz_data.T, cmap='RdBu', extent=(-sx/2, sx/2, -sy/2, sy/2))
    plt.colorbar(label='Hz field')
    plt.xlabel('x (um)')
    plt.ylabel('y (um)')
    plt.title(f'Hz field distribution at step {step}')
    plt.savefig(f"{output_dir}/hz_field_{step:04d}.png")
    plt.close()

def create_animation_from_images(output_dir, output_filename):
    """
    Create an animation from a series of PNG images.

    Parameters:
    - output_dir: The directory containing the PNG images.
    - output_filename: The filename of the output animation (e.g., 'animation.gif').
    """
    images = []
    filenames = sorted([f for f in os.listdir(output_dir) if f.endswith('.png')])
    for filename in filenames:
        images.append(imageio.imread(os.path.join(output_dir, filename)))
    imageio.mimsave(output_filename, images, fps=10)

if __name__ == '__main__':
    # Paramètres de simulation
    resolution = 20   # pixels/um
    eps = 13          # constante diélectrique du guide d'onde
    w = 1.2           # largeur du guide d'onde
    r = 0.36          # rayon des trous
    d = 1.4           # espacement du défaut
    N = 3             # nombre de trous de chaque côté du défaut
    sy = 6            # taille de la cellule dans la direction y (perpendiculaire au guide d'onde)
    pad = 2           # padding entre le dernier trou et le bord PML
    dpml = 1          # épaisseur de la couche PML

    sx = 2 * (pad + dpml + N) + d - 1  # taille de la cellule dans la direction x

    cell = mp.Vector3(sx, sy, 0)

    # Paramètres de la source
    fcen = 0.3   # fréquence centrale de l'impulsion
    df = 0.2      # largeur de bande de fréquence de l'impulsion

    src = [mp.Source(mp.GaussianSource(fcen, fwidth=df),
                     component=mp.Ey,
                     center=mp.Vector3(-0.5 * sx + dpml),
                     size=mp.Vector3(0, w))]

    sym = [mp.Mirror(mp.Y, phase=-1)]

    # Créer une simulation sans trous pour normaliser le flux
    geometry_no_holes = [mp.Block(size=mp.Vector3(mp.inf, w, mp.inf), material=mp.Medium(epsilon=eps))]

    sim_no_holes = mp.Simulation(cell_size=cell,
                                 geometry=geometry_no_holes,
                                 boundary_layers=[mp.PML(dpml)],
                                 sources=src,
                                 symmetries=sym,
                                 resolution=resolution)

    freg = mp.FluxRegion(center=mp.Vector3(0.5 * sx - dpml - 0.5),
                         size=mp.Vector3(0, 2 * w))
    nfreq = 500  # nombre de fréquences à calculer

    # Ajouter le détecteur de flux pour la simulation de normalisation
    trans_no_holes = sim_no_holes.add_flux(fcen, df, nfreq, freg)

    sim_no_holes.run(mp.at_beginning(mp.output_epsilon),
                     until_after_sources=mp.stop_when_fields_decayed(50, mp.Ey, mp.Vector3(0.5 * sx - dpml - 0.5), 1e-3))

    # Récupérer le spectre de flux sans trous
    flux_data_no_holes = mp.get_fluxes(trans_no_holes)

    # Simulation avec trous
    geometry_with_holes = [mp.Block(size=mp.Vector3(mp.inf, w, mp.inf), material=mp.Medium(epsilon=eps))]

    for i in range(N):
        geometry_with_holes.append(mp.Cylinder(r, center=mp.Vector3(d / 2 + i)))
        geometry_with_holes.append(mp.Cylinder(r, center=mp.Vector3(-(d / 2 + i))))

    sim_with_holes = mp.Simulation(cell_size=cell,
                                   geometry=geometry_with_holes,
                                   boundary_layers=[mp.PML(dpml)],
                                   sources=src,
                                   symmetries=sym,
                                   resolution=resolution)

    # Ajouter le détecteur de flux pour la simulation avec trous
    trans_with_holes = sim_with_holes.add_flux(fcen, df, nfreq, freg)

    sim_with_holes.run(mp.at_beginning(mp.output_epsilon),
                       until_after_sources=mp.stop_when_fields_decayed(50, mp.Ey, mp.Vector3(0.5 * sx - dpml - 0.5), 1e-3))

    # Récupérer le spectre de flux avec trous
    flux_data_with_holes = mp.get_fluxes(trans_with_holes)

    # Calculer la transmission
    frequencies = np.array(mp.get_flux_freqs(trans_with_holes))
    transmission = np.array(flux_data_with_holes) / np.array(flux_data_no_holes)

    # Tracer la transmission en fonction de la fréquence
    plt.figure(figsize=(8, 6))
    plt.plot(frequencies, transmission, label='Transmission avec trous',color="black")
    plt.xlabel('Fréquence (um^-1)')
    plt.ylabel('Transmission')
    plt.title('Transmission du guide d\'onde avec trous')
    plt.legend()
    plt.grid(True)
    plt.show()

    target_frequency = fcen
    plot_hz(sim_with_holes, sx, sy, target_frequency)

    # Création du répertoire pour les images si nécessaire
    output_dir = "hz_snapshots"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Fonction de callback pour sauver les images à chaque étape de temps
    def save_images(sim):
        step = int(sim.meep_time() * 1)
        save_hz_snapshot(sim, sx, sy, step, output_dir)

    # Exécuter la simulation avec l'animation
    sim_with_holes.run(mp.at_beginning(mp.output_epsilon),
                       mp.to_appended("hz_field", mp.at_every(1, save_images)),
                       until_after_sources=mp.stop_when_fields_decayed(50, mp.Ey, mp.Vector3(0.5 * sx - dpml - 0.5), 1e-3))

    # Créer l'animation à partir des images
    create_animation_from_images(output_dir, 'hz_animation.gif')


